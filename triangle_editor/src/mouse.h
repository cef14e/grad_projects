/**
 * Name  : Camill Folsom
 * Class : CAP 5726 Computer Graphics
 * Proj2 : Rasterization
 * Date  : October 21, 2018
 */

#ifndef MOUSE_H
#define MOUSE_H

class mouse
{
	public:
		mouse();

		void incrCount();
		
		int getCount();

        //Sets the whether a user is holding the left mouse button
		void setLhold(bool b);

        //Returns true is the user is holding the left mouse button
		bool lhold();

        //Get the initial x and y positions of the mouse
		double get_initial_x();
		double get_initial_y();

        //Set the initial x and y positions of the mouse
		void set_initial_x(double x);
		void set_initial_y(double y);

        //Sets the number of for the triangle the cursor is inside
		void set_triangle_num(int index);

		int get_triangle_num();

        //Set the vertex number the cursor is closest to
		void set_vertice(int vertice);

		int get_vertice();

        //Sets whether or not we should return to the original callback
		void set_called(bool b);

		bool was_called();

	private:
		//Counts the number of mouse click, the triangle number the cursor is
		//inside, sets the vertex number the cursor is closest to 
		int count, triangle_num, nearest_vertice;

		//Whether or not the user is holding the left mouse button and whether
		//we should return to the original key callback
		bool lbutton_hold, called;

		//Stores an x and y position for the mouse
		double initial_xpos, initial_ypos;
};

#endif