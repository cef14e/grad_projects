/**
 * Name  : Camill Folsom
 * Class : CAP 5726 Computer Graphics
 * Proj2 : Rasterization
 * Date  : October 21, 2018
 */

#include "Helpers.h"
#include <GLFW/glfw3.h>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <chrono>
#include <iostream>
#include <vector>
#include "mouse.h"
#include "triangle.h"

//Store the triangle in the scene
std::vector<triangle, Eigen::aligned_allocator<Eigen::Vector3f>> triangles;
mouse m1;

//The view matrix used in the vertex shader
Eigen::Matrix4f view(4,4);

//Whether the user is holding shift or not
bool hold_shift = false;

//Whether or not the animation should play
bool to_animate = false;

//The amount the view matrix should be zoomed
float zoom = 1;

//The amount the view matrix should be move the view left or right
float move_xy = 0;

//The amount the view matrix should move the view up or down
float move_up_down = 0;

//The time given to a keyframe
double kf_time = 0;

/**
 * @name : cursor_pos_callback
 * @brief: Used to stick a vertex to the mouse cursor
 */
void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
	double xworld, yworld;
	int width, height;
	Eigen::Vector4f p_canonical, p_world;
	Eigen::Vector2d a, b, c, pos;

	glfwGetWindowSize(window, &width, &height);

    //Find the world coordinates
	xworld = (2 * (xpos / double(width)) - 1);
	yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

    //Convert screen position to world coordinates
    p_canonical << float(xworld), float(yworld), 0, 1;
    p_world = view.inverse() * p_canonical;

    //Find the triangle we are drawing 
	for(int i = 0; i < triangles.size(); ++i)
	{
		//Make sure this is not the first left mouse button click and that
		//the triangle has not been drawn yet
		if (m1.getCount()%3 != 0 && triangles[i].getCount() < 3)
		{
			triangles[i].stickCol(p_world(0), p_world(1));
			triangles[i].VBO_update();
		}
	}
}

/**
 * @name : mouse_button_callback
 * @brief: Sets all the vertexes of the triangle to the same point on the first
 *         left mouse button click and calls cursor pos callback to stick a
 *         vertex to the mouse cursor. On the second click it changes another
 *         vertex's coordinates to the mouses. On the third click it sets the
 *         vertex that was stuck to the mouses position.
 */
void mouse_button_callback(GLFWwindow* window, int button, int action, int mode)
{
   double xpos, ypos, xworld, yworld;
   int width, height;
   Eigen::Vector4f p_canonical, p_world;

   //Used to push a new triangle object back into the scene so it can be drawn
   triangle t1;
   
   glfwGetWindowSize(window, &width, &height);
   glfwGetCursorPos(window, &xpos, &ypos);

   //convert to world coordinates
   xworld = (2 * (xpos / double(width)) - 1);  
   yworld = (2 * ((height - ypos - 1) / double(height))) - 1;
   
   //Convert screen position to world coordinates
   p_canonical << float(xworld), float(yworld), 0, 1;
   p_world = view.inverse() * p_canonical;

   //Draws a triangle for each three click of the left mouse button
   if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
   {
   	   //If this is the first click for a triangle push a new triangle back
   	   //into the scene and initialize its VBO.
	   if (m1.getCount() % 3 == 0)
	   {
		   triangles.push_back(t1);
		   triangles[triangles.size()-1].init();
	   }
	   
	   //Loop through each triangle and find the one that need to have its
	   //vertex positions set.
	   for (int i = 0; i < triangles.size(); ++i)
	   {
	   	   //Make sure a triangle has not already had its vertexes set
		   if (triangles[i].getCount() < 3)
		   {
		   	   //Sets a vertex to where the mouse has clicked 
			   triangles[i].setCol(p_world(0), p_world(1));

			   //Sticks a vertex to the cursor
			   glfwSetCursorPosCallback(window, cursor_pos_callback);

			   //Increment the vertex that has been set so it is not set twice
			   triangles[i].incrCount();

			   triangles[i].VBO_update();
		   }
	   }
	   //Tracks the number of left mouse button clicks.   
	   m1.incrCount();
   }
}

/**
 * @name : trans_pos_callback (Translation Cursor Position Callback)
 * @brief: Used to move the selected triangle with the cursor.
 */
void trans_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
	double xworld, yworld, xmov, ymov;
	int width, height;
	Eigen::Vector4f p_canonical, p_world;

	glfwGetWindowSize(window, &width, &height);

    //convert to world coordinates
	xworld = (2 * (xpos / double(width)) - 1);
	yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

    //Convert screen position to world coordinates
    p_canonical << float(xworld), float(yworld), 0, 1;
    p_world = view.inverse() * p_canonical;

    //Calculate how far the mouse has move from the initial position
	xmov = xpos - m1.get_initial_x();
	ymov = ypos - m1.get_initial_y();

    //Update the initial cursor position
	m1.set_initial_x(xpos);
	m1.set_initial_y(ypos);

    //Make sure the user is holding down the left mouse button and the cursor
    //is inside the triangle the user clicked on
	if(m1.lhold() && triangles[m1.get_triangle_num()].inTriangle(p_world(0), p_world(1)))
	{
		triangles[m1.get_triangle_num()].move(window, xmov, ymov, view);
		triangles[m1.get_triangle_num()].VBO_update();
	}
	else
		return;
}

/**
 * @name : trans_key_callback (Translation Key Callback)
 * @brief: In translation mode scale the size of the triangle down by 25% when
 *         the L key is pressed. Scale its size up by 25% when the K key is
 *         pressed. Rotate the triangle 10 degrees clockwise and 10 degrees
 *         counter-clock wise when h and j are pressed. Capture a keyframe when
 *         the M key is pressed.
 */
void trans_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//Makes sure we start calling the original key callback.
	m1.set_called(false);
	switch(key)
	{
		//Shrink triangle by 25%
		case GLFW_KEY_L:
			if(action == GLFW_PRESS)
			   triangles[m1.get_triangle_num()].scale(0.75f);
			break;

		//Grow triangle by 25%
		case GLFW_KEY_K:
			if(action == GLFW_PRESS)
			   triangles[m1.get_triangle_num()].scale(1.25f);
			break;

		//Rotate triangle counterclockwise by 10 degrees
		case GLFW_KEY_H:
			if(action == GLFW_PRESS)
			   triangles[m1.get_triangle_num()].rotate(-10);
			break;

		//Rotate triangle clockwise by 10 degrees
		case GLFW_KEY_J:
			if(action == GLFW_PRESS)
			   triangles[m1.get_triangle_num()].rotate(10);
			break;

		//Capture a keyframe
		case GLFW_KEY_M:
            if(action == GLFW_PRESS)
            {
               std::cout << "Triangle: " << m1.get_triangle_num() << " Key Frame Made\n";

               //Make a keyframe for the triangle being edited
               triangles[m1.get_triangle_num()].make_keyframe(kf_time);

               //Make it so each keyframe time differ by 3.
               kf_time += 3;
            }
			break;
		default:
		    //Used to start calling the original key callback
			m1.set_called(true);

			//Restores the triangles original color when done
			triangles[m1.get_triangle_num()].original_color();
			break;
	}
	triangles[m1.get_triangle_num()].VBO_update();
}

/**
 * @name : trans_button_callback (Translation Mouse Button Callback)
 * @brief: Finds the triangle to be put into translation mode
 */
void trans_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	double xworld, yworld, xpos, ypos;
	int width, height;
	Eigen::Vector4f p_canonical, p_world;

	glfwGetWindowSize(window, &width, &height);
	glfwGetCursorPos(window, &xpos, &ypos);

    //convert to world coordinates
	xworld = (2 * (xpos / double(width)) - 1);
	yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

    //Convert screen position to world coordinates
    p_canonical << float(xworld), float(yworld), 0, 1;
    p_world = view.inverse() * p_canonical;

	//Check to see if the cursor is inside the triangle
	if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{	
		//User is holding left mouse button until release is called
		m1.setLhold(true);

        //Loop through each triangle in the scene
		for(int i = 0; i < triangles.size(); ++i)
		{
            //Check to see if the cursor is inside this triangle
			if(triangles[i].inTriangle(p_world(0), p_world(1)))
			{
				//Makes sure the last triangles number is not out of range.
				if(m1.get_triangle_num() < triangles.size())
				{
				   triangles[m1.get_triangle_num()].original_color();
				   triangles[m1.get_triangle_num()].VBO_update();
			    }

                //Change the triangle color to blue while it is in translation
                //mode
				triangles[i].change_color(0, 0, 1);
				triangles[i].VBO_update();

				//Set the number of the triangle the cursor is inside
				m1.set_triangle_num(i);

				//Set the initial x and y position of the cursor
				m1.set_initial_x(xpos);
				m1.set_initial_y(ypos);

				//Stick the triangle to the cursor while the user holds button
				glfwSetCursorPosCallback(window, trans_pos_callback);
				break;
			}
		}
	}
	
	//Always check to see if user released mouse button
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		//Track whether the user has let go of the mouse button
		m1.setLhold(false);

		//Start calling the key callback to allow user to modify triangle
		glfwSetKeyCallback(window, trans_key_callback);

	}
	
}

/**
 * @name : del_button_callback (Delete Mouse Button Callback)
 * @brief: Deletes a triangle from the scene when the mouse clicks it.
 */
void del_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	double xworld, yworld, xpos, ypos;
	int width, height;
	Eigen::Vector4f p_canonical, p_world;

	glfwGetWindowSize(window, &width, &height);
	glfwGetCursorPos(window, &xpos, &ypos);

    //convert to world coordinates
	xworld = (2 * (xpos / double(width)) - 1);
	yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

    //Convert screen position to world coordinates
    p_canonical << float(xworld), float(yworld), 0, 1;
    p_world = view.inverse() * p_canonical;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		for (int i = 0; i < triangles.size(); ++i)
		{
			//Check to see if the cursor is inside the triangle
			if (triangles[i].inTriangle(p_world(0), p_world(1)))
			{
				triangles[i].VBO_free();
				triangles.erase(triangles.begin() + i);
				break;
			}
		}
	}
}

/**
 * @name : color_key_callback
 * @brief: Color a vertex based on the number keys 1-9. 
 */
void color_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	m1.set_called(false);
	switch (key)
	{
		//Color the specified vertex green
		case GLFW_KEY_1:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 0, 1, 0);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex blue
		case GLFW_KEY_2:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 0, 0, 1);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex red
		case GLFW_KEY_3:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 1, 0, 0);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex white
		case GLFW_KEY_4:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 1, 1, 1);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex cyan
		case GLFW_KEY_5:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 0, 1, 1);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex magenta
		case GLFW_KEY_6:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 1, 0, 1);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex yellow
		case GLFW_KEY_7:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 1, 1, 0);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex black
		case GLFW_KEY_8:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 0, 0, 0);
			triangles[m1.get_triangle_num()].VBO_update();
			break;

		//Color the specified vertex
		case GLFW_KEY_9:
			triangles[m1.get_triangle_num()].color_vertice(m1.get_vertice(), 1.4f, .7f, .4f);
			triangles[m1.get_triangle_num()].VBO_update();
			break;
		default:
			m1.set_called(true);
			break;
	}
	
}

/**
 * @name : color_button_callback (Color Mouse Button Callback)
 * @brief: 
 */
void color_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	double xworld, yworld, xpos, ypos;
	int width, height;
	Eigen::Vector4f p_canonical, p_world;

	glfwGetWindowSize(window, &width, &height);
	glfwGetCursorPos(window, &xpos, &ypos);

    //Convert to world coordinates
	xworld = (2 * (xpos / double(width)) - 1);
	yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

    //Convert screen position to world coordinates
	p_canonical << float(xworld), float(yworld), 0, 1;
	p_world = view.inverse() * p_canonical;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		//Loop through each triangle in the scene
		for (int i = 0; i < triangles.size(); ++i)
		{
			//Check if the cursor is inside the triangle
			if (triangles[i].inTriangle(p_world(0), p_world(1)))
			{
				//Set the vertex that is closet to the mouses position
				m1.set_vertice(triangles[i].nearest_vertice(p_world(0), p_world(1)));

				//Set the triangle number that the mouse is inside
				m1.set_triangle_num(i);

				//Call color key callback to color selected vertex
				glfwSetKeyCallback(window, color_key_callback);
				break;
			}
		}
	}
}

/**
 * @name       : key_callback
 * @brief      : This is the main key callback for the program. 
 * @Description: Key 'I' enters insertion mode. In insertion mode the user sets
 *               the triangles vertexes with left mouse button clicks.
 *
 *               Key 'O' enters into translation mode. Which allows the user to
 *               move, scale, rotate, and capture keyframes for the triangle
 *
 *               Key 'P' enter deletion mode. This allow the user to delete a
 *               triangle.
 *
 *               Key 'C' enter into color mode. This mode allows the user to
 *               color a vertex by clicking on the vertex they wish to color.
 *
 *               Keys '+' and '-' are used to zoom in and out.
 *
 *               Keys w, a, s, and d are used to move the view up, left, down,
 *               and right respectively.
 *
 *               Key 'Z' is used to start the keyframe animation.
 */
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//Make sure the animation does not start until key 'Z' is pressed
	to_animate = false;

	switch (key)
	{
		//Enters into insertion mode
		case GLFW_KEY_I:
			glfwSetMouseButtonCallback(window, mouse_button_callback);
			break;

		//Enters into translation mode
		case GLFW_KEY_O:			
			glfwSetMouseButtonCallback(window, trans_button_callback);
			break;

		//Enters into deletion mode
		case GLFW_KEY_P:
			glfwSetMouseButtonCallback(window, del_button_callback);
			break;

		//Enters into vertex coloring mode
		case GLFW_KEY_C:
			glfwSetMouseButtonCallback(window, color_button_callback);
			break;

		//Check to see if the user is holding left or right shift
		case GLFW_KEY_RIGHT_SHIFT:
		case GLFW_KEY_LEFT_SHIFT:
		    if(action == GLFW_PRESS)
		    	hold_shift = true;

		    if(action == GLFW_RELEASE)
		    	hold_shift = false;
		    break;

		//Increases the views zoom by 20%
		case GLFW_KEY_EQUAL:
		    if(hold_shift && action == GLFW_PRESS)
		    	zoom = zoom + 0.2f;
		    break;

		//Decreases the views zoom by 20%
		case GLFW_KEY_MINUS:
		    if(action == GLFW_PRESS)
		    	zoom -= 0.2f;
		    break;

        //Shift the view to the left by 20%
        case GLFW_KEY_A:
            if(action == GLFW_PRESS)
            	move_xy -= .2f;
            break;

        //Shift the view to the right by 20%
        case GLFW_KEY_D:
            if(action == GLFW_PRESS)
            	move_xy += .2f;
            break;

        //Shift the view to the up by 20%
        case GLFW_KEY_W:
            if(action == GLFW_PRESS)
            	move_up_down += .2f;
            break;

        //Shift the view to the down by 20%
        case GLFW_KEY_S:
            if(action == GLFW_PRESS)
            	move_up_down -= .2f;
            break;

        //Starts the animation
        case GLFW_KEY_Z:
            to_animate = true;
            break;

		default:
			break;
	}
}


int main()
{
   GLFWwindow* window;
   int major, minor, rev, width, height;
   float aspect_ratio;
   int num_triangles = 1;

   //Initialize the library
   if(!glfwInit())
      return -1;

   //Activate supersampling
   glfwWindowHint(GLFW_SAMPLES, 8);

   //Ensure that we get at least a 3.2 context
   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

   //On apple we have to load a core profile with forward compatibility
   #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
   #endif

   //Create a windowed mode window and its OpenGL context
   window = glfwCreateWindow(640, 480, "Triangle Soup", NULL, NULL);
   
   //If failed to create window return error
   if(!window)
   {
      glfwTerminate();
      return -1;
   }

   //Make the window's context current
   glfwMakeContextCurrent(window);

  #ifndef __APPLE__
      glewExperimental = true;
      GLenum err = glewInit();
      
      //Problem: glewInit failed.
      if(GLEW_OK != err)
         fprintf(stderr, "Error: %s\n", glewGetString(GLEW_VERSION));

      //pull and ignore unhandled errors like GL_INVALID_ENUM
      glGetError();
      fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
  #endif

   major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
   minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);
   rev = glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION); 
   
   printf("OpenGL version received: %d.%d.%d\n", major, minor, rev);
   printf("Supported OpenGL is %s\n", (const char*)glGetString(GL_VERSION));
   printf("Supported GLSL is %s\n", (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

   //Initialize the VAO
   VertexArrayObject VAO;
   VAO.init();
   VAO.bind();

   Program program;
 
   //Initialize the OpenGL Program
   //A program controls the OpenGL pipeline and it must contains
   //at least a vertex shader and a fragment shader to be valid
   const GLchar* vertex_shader =
	     "#version 150 core\n"

	     "in vec2 position;"
	     "in vec3 color;"

	     "out vec3 Color;"

         "uniform mat4 view;"

         "void main()"
         "{" 
	         "Color = color;"
	         "gl_Position = view * vec4(position, 0.0, 1.0); "
	     "}";

   const GLchar* fragment_shader = 
        "#version 150 core\n"
         
	     "in vec3 Color;"
         "out vec4 outColor;"

         "void main()"
         "{ outColor = vec4(Color, 1.0);}";


   //Compile the two shaders and upload the binary to the GPU
   //Note that we have to explicitly specify that the output "slot" called outColor
   //is the one that we want in the fragment buffer (and thus on screen)
   program.init(vertex_shader, fragment_shader, "outColor");
   program.bind();


   //Register the keyboard callback
   glfwSetKeyCallback(window, key_callback);

   //Enable sticky keys
   glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);

   //Loop until the user closes the window
   while(!glfwWindowShouldClose(window))
   {
   	  //Get size of the window
   	  glfwGetWindowSize(window, &width, &height);
      aspect_ratio = float(height)/float(width);

      //Set up the view matrix
      view << aspect_ratio * zoom, 0, 0, move_xy,
              0, zoom, 0, move_up_down,
              0, 0, zoom, 0,
              0, 0, 0, 1;
      
      glUniformMatrix4fv(program.uniform("view"), 1, GL_FALSE, view.data());

      //Clear the frame buffer
	  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	  glClear(GL_COLOR_BUFFER_BIT);

      //If a different key callback has finished call the main key callback
	  if(m1.was_called())
	  {
		  glfwSetKeyCallback(window, key_callback);
		  m1.set_called(false);
	  }

      //Draw each triangle and animate it
	  for (int i = 0; i < triangles.size(); ++i)
	  {
	  	  //If the user has hit the key 'Z' then animate any triangle with
	  	  //keyframes
	  	  if(to_animate)
	  	  {
	  	     triangles[i].animate();
	  	     triangles[i].VBO_update();
	  	  } 
	  	  //Draw triangle i   
	  	  triangles[i].draw(program);
	  }

      //Swap front and back buffers
	  glfwSwapBuffers(window);

	  //Poll for and process events
      glfwPollEvents();
   }

   //Deallocate opengl memory
   program.free();
   VAO.free();

   for (int i = 0; i < triangles.size(); ++i)
	   triangles[i].VBO_free();
   
   //Deallocate glfw internals
   glfwTerminate();

   return 0;
}
