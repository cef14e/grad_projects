/**
 * Name  : Camill Folsom
 * Class : CAP 5726 Computer Graphics
 * Proj2 : Rasterization
 * Date  : October 21, 2018
 */

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <Eigen/Core>
#include "Helpers.h"
#include <GLFW/glfw3.h>
#include <vector>


class keyframe
{
    public:
    	//Default Constructor
    	keyframe();

        //Sets the vert_pos that corresponds to vert_num
    	void set_vert_pos(const Eigen::Vector2d&, int vert_num);
        void set_time(double);

        double get_time() const;

        //Get the vert_pos that corresponds to vert_num
        Eigen::Vector2d get_vert_pos(int vert_num) const;

        keyframe& operator=(const keyframe&);  

    private:
       //vertex position for the a triangle in the frame
	   Eigen::Vector2d vert_pos[3];

	   //The time in the animation for the keyframe
	   double time;

	   //Number of vertex the keyframe holds
	   int vertices;
};

class triangle
{
	public:
		triangle(float r = 1.0, float g = 0.0, float b = 0.0);

        //Initialize the triangles VBO
		void init();

        //Sets the vertex positions of the triangle
		void setCol(float xworld, float yworld);

        //Moves the triangle, overloaded to work with or without a view matrix
        void move(GLFWwindow*, double, double);
		void move(GLFWwindow*, double, double, Eigen::Matrix4f&);

        //Scales the size of the triangle by amount
		void scale(float amount);

        //Rotate the triangle by the degrees passed in
		void rotate(float degrees);

        //Changes the whole triangles color
		void change_color(float r, float g, float b);

        // Sets the vert_color variables
		void set_color(float r, float g, float b);

        //Restores the triangle to the values stored in vert_color
		void original_color();

        //Used to stick vertex 0 to the x and y positions passed in
		void stickCol(double x, double y);

		int getCount();

		void incrCount();

		void VBO_bind();

		void VBO_update();

        //Used to test whether xworld and yworld are inside the triangle
		bool inTriangle(double xworld, double yworld);

        //Find the vertex nearest xworld and yworld
		int nearest_vertice(double xworld, double yworld);

        //Color the vertex passed in with the values r, g, b. 
		void color_vertice(int vertice, float r, float g, float b);

        //Sets the bool that stores if the cursor is inside the triangle
		void set_inside(bool b);

        //Return the stored bool for if the cursor is inside the triangle
		bool is_inside();

        //Uses keyframes to animate the triangle
		void animate();

        //Makes a keyframe from the passed in time and current triangle
        //position then adds the keyframe to keyframes
		void make_keyframe(double t);

        //Draw the triangle 
		void draw(Program& program);

		void VBO_free();

	private:
		//Count the number of mouse click that are used to make the triangle.
		int count, num_vertices;
		VertexBufferObject VBO;

		//Store the x and y positions of the triangles vertexes and the rgb
		//color values for each vertex
		Eigen::MatrixXf V;

		//Whether the cursor is inside this triangle
		bool curs_inside;

		//The color values for each vertex
		Eigen::Vector3f vert1_color, vert2_color, vert3_color;
		Eigen::Vector2d center;
		std::vector<keyframe> keyframes;

		//Used to track how long the animation has played for
		double animation_time;

		void find_center();
};

#endif