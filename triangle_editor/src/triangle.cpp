/**
 * Name  : Camill Folsom
 * Class : CAP 5726 Computer Graphics
 * Proj 2: Rasterization
 * Date  : October 21, 2018
 */

#include <math.h>
#include <iostream>
#include <Eigen/Dense>
#include "triangle.h"

//------------ Start of keyframe class ---------------//
/**
 * @name : Default Constructor keyframe
 * @brief: Sets the number of vertexes to three and the vertex position to
 *         zero.
 */
keyframe::keyframe() : time(0)
{
   vertices = 3;
   for(int i = 0; i < vertices; ++i)
      vert_pos[i] << 0, 0;
}

/**
 * @name : set_vert_pos (Set Vertex Position)
 * @brief: Sets the vertex that corresponds to vert_num.
 */
void keyframe::set_vert_pos(const Eigen::Vector2d& vert, int vert_num)
{  vert_pos[vert_num] = vert;}

/**
 * @name : set_time
 * @brief: Sets the keyframes time to the value passed in.
 */
void keyframe::set_time(double t)
{  time = t;}

/**
 * @name : get_time
 * @brief: Returns the time for this keyframe.
 */
double keyframe::get_time() const
{  return time;}

/**
 * @name : get_vert_pos (Get Vertex Position)
 * @brief: Sets the vertex that corresponds to the vert_num passed in.
 */
Eigen::Vector2d keyframe::get_vert_pos(int vert_num) const
{  return vert_pos[vert_num];}

/**
 * @name : operator=
 * @brief: Overloading the equals operation.
 */
keyframe& keyframe::operator=(const keyframe& rhs)
{
   // Make sure the objects are not the same.
   if(this == &rhs)
      return *this;

   // Copy rhs.
   time = rhs.time;

   for(int i = 0; i < vertices; ++i)
      vert_pos[i] = rhs.vert_pos[i];

   return *this;
}

//------------ End of keyframe class -----------------//


//------------ Start of triangle class --------------//

/**
 * @name : Default Constructor triangle
 * @brief: Sets all the triangle values to zero, except color which is set to
 *         the values passed in.
 */
triangle::triangle(float r, float g, float b) : count(0), curs_inside(false) 
{
	vert1_color << r, g, b;
	vert2_color = vert1_color;
	vert3_color = vert1_color;
	num_vertices = 3;
	center << 0, 0;
	V.resize(5, 3);
  animation_time = 0;
}

/**
 * @name : init (Initialize VBO)
 * @brief: Initializes the VBO 
 */
void triangle::init()
{ VBO.init();}

// Set the vertexes for the triangle
/**
 * @name : setCol (Set Column)
 * @brief: Sets each vertex based of the number of set vertexes.
 */
void triangle::setCol(float xworld, float yworld)
{
  //If no vertexes have been set, set all three to the same point
	if (count == 0)
	{
      for(int i = 0; i < num_vertices; ++i)
		      V.col(i) << xworld, yworld, vert1_color(0), vert1_color(1), vert1_color(2);
	}
  //If one vertex has been set then set the next vertex
	else if (count == 1)
	    V.col(1) << xworld, yworld, vert2_color(0), vert2_color(1), vert2_color(2);

  //If two vertexes have been set, change the vertex that was stuck to the 
  //pointer to the final vertex, then find and set the center of the triangle
	else if (count == 2)
	{
	    V.col(0) << xworld, yworld, vert1_color(0), vert1_color(1), vert1_color(2);
	    find_center();
	}
}

/**
 * @name : move (Without a View Matrix)
 * @brief: Move the triangle by xmov, ymov. Not using a view matrix. This is 
 *         done by converting the stored world coordinates to the original x
 *         and y positions then we add the amount moved to these positions. 
 *         Finally we convert the new positions back to the world space.
 */
void triangle::move(GLFWwindow* window, double xmov, double ymov)
{
    double xpos, ypos, xworld, yworld;
    int width, height;

    glfwGetWindowSize(window, &width, &height);

    for(int i = 0; i < num_vertices; i++)
    {
       //Convert the world coordinates to their x and y positions and add the 
       //amount moved.
       xpos = (((V(0,i)+1)/2)*double(width)) + xmov;
       ypos = (-(((V(1,i)+1)/2)*double(height)+1-height)) + ymov;

       //Convert x and y positions to their world coordinates.
       xworld = (2 * (xpos / double(width)) - 1);
       yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

       //Update the triangles position and keep the color set to blue
       V.col(i) << float(xworld), float(yworld), 0, 0, 1;
    }

    //Find and set the new center of the triangle
    find_center();
}

/**
 * @name : move (With a View Matrix)
 * @brief: Move the triangle by xmov, ymov. Using a view matrix. This is 
 *         done by converting the stored world coordinates to the original x
 *         and y positions then we add the amount moved to these positions. 
 *         Finally we convert the new positions back to the world space.
 */
void triangle::move(GLFWwindow* window, double xmov, double ymov, Eigen::Matrix4f& view)
{
   double xpos, ypos, xworld, yworld;
   int width, height;
   Eigen::Vector4f p_canonical, p_world;

   glfwGetWindowSize(window, &width, &height);

   for(int i = 0; i < num_vertices; i++)
   {
      //Get the canonical coordinates;
      p_world << V(0,i), V(1,i), 0, 1;
      p_canonical = view * p_world;

      //Use canonical coordinates to find to get x and y position and add the
      //amount moved to find the new positions
      xpos = (((p_canonical(0)+1)/2)*double(width)) + xmov;
      ypos = (-(((p_canonical(1)+1)/2)*double(height)+1-height)) + ymov;

      //Convert the new positions to world coordinates.
      xworld = (2 * (xpos / double(width)) - 1);
      yworld = (2 * ((height - ypos - 1) / double(height))) - 1;

      //Use the world coordinates and the view matrix to find the real world
      //coordinates
      p_canonical << float(xworld), float(yworld), 0, 1;
      p_world = view.inverse() * p_canonical;

      //Update the triangles positions and keep the color set to blue
      V.col(i) << p_world(0), p_world(1), 0, 0, 1;
   }
   //Find and set the new center of the triangle
   find_center();
}

/**
 * @name : scale
 * @brief: Scale the size of the triangle by amount this is done relative to
 *         the triangles center.
 */
void triangle::scale(float amount)
{
   double x,y;

   //Calculates the new vertice coordinates relative to the center
   for(int i = 0; i < num_vertices; i++)   	
   {  
      x = ((V(0,i) - center(0)) * amount) + center(0);
      y = ((V(1,i) - center(1)) * amount) + center(1);
   	  V.col(i) << float(x), float(y), 0, 0, 1;
   }
}

/**
 * @name : rotate
 * @brief: Rotate the triangle by degrees this is done relative to the center
 */
void triangle::rotate(float degrees)
{
   double x, y;
	 double pi = 3.14159265;
   //Convert degrees to radians
	 double rads = degrees * pi/180;

   for(int i = 0; i < num_vertices; i++)
   {
   	  //find the new x and y vertexes relative to the center
      x = (V(0,i)-center(0)) * cos(rads) - (V(1,i)-center(1)) * sin(rads)
          + center(0);
      y = (V(0,i)-center(0)) * sin(rads) + (V(1,i)-center(1)) * cos(rads)
          + center(1);

      //Update the triangles positions and keep the color set to blue
   	  V.col(i) << float(x), float(y), 0, 0, 1;
   }
}

/**
 * @name : change_color (triangle)
 * @brief: Changes the color of all vertexes of the triangle.
 */
void triangle::change_color(float r, float g, float b)
{
   for(int i = 0; i < num_vertices; ++i)
	    V.col(i) << V(0,i), V(1,i), r, g, b;
}

/**
 * @name : set_color (vertexes)
 * @brief: Sets the stored vertex colors to the values passed in
 */
void triangle::set_color(float r, float g, float b)
{
	vert1_color << r, g, b;
	vert2_color << r, g, b;
	vert3_color << r, g, b;
}

/**
 * @name : original_color (triangle)
 * @brief: Sets the triangle to the stored vert_color values.
 */
void triangle::original_color()
{
    V.col(0) << V(0, 0), V(1, 0), vert1_color(0), vert1_color(1), vert1_color(2);
    V.col(1) << V(0, 1), V(1, 1), vert2_color(0), vert2_color(1), vert2_color(2);
    V.col(2) << V(0, 2), V(1, 2), vert3_color(0), vert3_color(1), vert3_color(2);
}

/**
 * @name : stickCol (Stick Vertex)
 * @brief: Sets vertex 0 to the values passed in.
 */
void triangle::stickCol(double x, double y)
{  V.col(0) << float(x), float(y), vert1_color(0), vert1_color(1), vert1_color(2);}

/**
 * @name : getCount (Get Count)
 * @brief: Get the counter for this triangle.
 */
int triangle::getCount()
{  return count;}

/**
 * @name : incrCount (Increment Count)
 * @brief: Increments the triangles counter
 */
void triangle::incrCount()
{  ++count;}

/**
 * @name : VBO_bind 
 * @brief: Used to bind the triangles VBO
 */
void triangle::VBO_bind()
{  VBO.bind();}

/**
 * @name : VBO_update
 * @brief: Used to update the triangles VBO
 */
void triangle::VBO_update()
{  VBO.update(V);}

/**
 * @name : in_plane
 * @brief: Used to find if a point is inside a triangle. Returns negative 
 *         if point is inside triangle. 
 */
double in_plane(Eigen::Vector2d& point, Eigen::Vector2d& b, Eigen::Vector2d& c)
{  return ((point(0) - c(0)) * (b(1) - c(1)) - (b(0) - c(0)) * (point(1) - c(1)));}

/**
 * @name : inTriangle
 * @brief: Used to check is the world coordinates are inside the triangle
 */
bool triangle::inTriangle(double xworld, double yworld)
{
   Eigen::Vector2d a, b, c, pos;
   bool b1, b2, b3;

   //Get the triangles vertexes
   a << V(0, 0), V(1, 0);
   b << V(0, 1), V(1, 1);
   c << V(0, 2), V(1, 2);
   pos << xworld, yworld;

   //Test whether or not pos is inside the vertexes a, b, and c.
   b1 = in_plane(pos, a, b) < 0;
   b2 = in_plane(pos, b, c) < 0;
   b3 = in_plane(pos, c, a) < 0;

   //If b1, b2, and b3 are less than zero then the pos is inside the vertexes
   if ((b1 == b2) && (b2 == b3))
      return true;
   else
      return false;
}

/**
 * @name : nearest_vertice
 * @brief: Find the vertex that is closet to the parameters passed in
 */
int triangle::nearest_vertice(double xworld, double yworld)
{
   Eigen::Vector2d a, b, c, pos;
   double b1, b2, b3;

   //Get the triangles vertexes
   a << V(0, 0), V(1, 0);
   b << V(0, 1), V(1, 1);
   c << V(0, 2), V(1, 2);
   pos << xworld, yworld;

   //Find the distance between pos and the vertexes a, b, and c
   b1 = (pos - a).norm();
   b2 = (pos - b).norm(); 
   b3 = (pos - c).norm();

   //Returns the vertex the position is closest to, returns 3 if not inside
   //the triangle
   if(b1 < b2 && b1 < b3)
      return 0;
   else if(b2 < b1 && b2 < b3)
      return 1;
   else if(b3 < b1 && b3 < b2)
      return 2;
   else
      return 3; 
}

/**
 * @name : color_vertice
 * @brief: Change the passed in vertex's color to the passed in color
 */
void triangle::color_vertice(int vertice, float r, float g, float b)
{
   //Color the vertex passed in with the passed in values. Make sure the
   //the vertex is in a valid range.
   if(vertice < 3)
      V.col(vertice) << V(0, vertice), V(1, vertice), r, g, b;

   //Change vertex 1's stored color values
   if(vertice == 0)
      vert1_color << r, g, b;

   //Change vertex 2's stored color values
   if(vertice == 1)
      vert2_color << r, g, b;

   //Change vertex 3's stored color values
   if(vertice == 2)
      vert3_color << r, g, b;
}

/**
 * @name : set_inside
 * @brief: Set the value used to determine whether or not the cursor is inside
 *         the triangle
 */
void triangle::set_inside(bool b)
{  curs_inside = b;}

/**
 * @name : is_inside
 * @brief: Returns the stored value for whether or not the cursor is inside
 *         the triangle
 */
bool triangle::is_inside()
{  return curs_inside;}

/**
 * @name : interpolate (Linear Interpolation)
 * @brief: Used to do linear interpolation between the two keyframes passed in.
 *         The the new frame is returned.
 */
keyframe interpolate(const keyframe& prev_kf, const keyframe& cur_kf, double t)
{
   Eigen::Vector2d result, v0, v1;
   keyframe new_kf;
   double t0, t1, dt;
   
   //Get the time from each keyframe
   t0 = prev_kf.get_time();
   t1 = cur_kf.get_time();

   //Calculate the change in time
   dt = (t - t0)/(t1 - t0);

   //Calculate the new position for each vertex of the triangle
   for(int i = 0; i < 3; ++i)
   {
      //Get a vertex from the two keyframe
      v0 = prev_kf.get_vert_pos(i);
      v1 = cur_kf.get_vert_pos(i);

      //Find the new vertex position
      result = v0 + dt * (v1 - v0);

      //Set the new keyframes vertex position.
      new_kf.set_vert_pos(result, i);
   }
   return new_kf;
}

/**
 * @name : animate
 * @brief: Animates the the triangle using keyframes
 */
void triangle::animate()
{
   keyframe new_kf;
   Eigen::Vector2d vert_pos;
   Eigen::Vector3f color[] = {vert1_color, vert2_color, vert3_color};
   float x, y;
   double end_time;
   double dt = 0.016666666666666666666;

   //No frames or not enough frames to animate for this triangle.
   if(keyframes.size() <= 1)
      return;

   end_time = keyframes[keyframes.size()-1].get_time();

   //If the animation time is greater than the last frames time the animation
   //has finished reset time to the initial keyframes time to restart animation
   if(end_time < animation_time)
      animation_time = keyframes[0].get_time();

   //Makes sure the first keyframe is drawn.
   if(animation_time == keyframes[0].get_time())
   {
      //Loop through each vertice of the keyframe
      for(int i = 0; i < num_vertices; ++i)
      {
         //Get a vertice from the keyframe
         vert_pos = keyframes[0].get_vert_pos(i);

         x = float(vert_pos(0));
         y = float(vert_pos(1));

         //Update the triangles positions with the keyframes position
         V.col(i) << x, y, color[i](0), color[i](1), color[i](2);
      }
      
   }

   //Loop through each keyframe and find the new points
   for(int i = 0; i < keyframes.size()-1; ++i)
   {
      //Make we only interpolate for the two keyframe that time is in between
      if(keyframes[i].get_time() < animation_time && 
         keyframes[i+1].get_time() >= animation_time)
      {
         //Use linear interpolation to find the new vertex positions
         new_kf = interpolate(keyframes[i], keyframes[i+1], animation_time);

         //Loop through each of the keyframes vertices
         for(int i = 0; i < num_vertices; ++i)
         {
             //Get a vertex from the interpolated keyframe
             vert_pos = new_kf.get_vert_pos(i);

             x = float(vert_pos(0));
             y = float(vert_pos(1));

             //Update the triangles positions with the new keyframes position
             V.col(i) << x, y, color[i](0), color[i](1), color[i](2);
         }
      }
   }
   //Advance time by an amount dt.
   animation_time += dt;
}

/**
 * @name : make_keyframe
 * @brief: Make a keyframe from the current triangles vertex positions and
 *         the time passed in
 */
void triangle::make_keyframe(double t)
{
   keyframe kf;
   Eigen::Vector2d vert_pos;

   kf.set_time(t);

   //Set the keyframes vertexes to the triangles vertexes
   for(int i = 0; i < num_vertices; ++i)
   {
       vert_pos << V(0,i), V(1,i);
       kf.set_vert_pos(vert_pos, i);
   }

   keyframes.push_back(kf);
}

/**
 * @name : draw
 * @brief: Used to draw the triangle. It draws a line until count is equal to
 *         three then it draws a triangle instead
 */
void triangle::draw(Program& program)
{
   VBO_bind();

   //Specify the layout of the vertex data's position
   GLint posAttrib = program.attrib("position");
   glEnableVertexAttribArray(posAttrib);
   glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

   //Specify the layout of the vertex data's color
   GLint colAttrib = program.attrib("color");
   glEnableVertexAttribArray(colAttrib);
   glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

   //Draws a line until count is equal to 3
   if (count < 3)
      glDrawArrays(GL_LINE_LOOP, 0, 3);
   else
      glDrawArrays(GL_TRIANGLES, 0, 3);
}

/**
 * @name : VBO_free
 * @brief: Free the VBO for this triangle
 */
void triangle::VBO_free()
{ VBO.free();}

/**
 * @name : find_center
 * @brief: Finds the center of the triangle. 
 */
void triangle::find_center()
{
  //Makes sure center is set to zero before we start calculating
	center << 0, 0;

  //Sum up the x and y values of the triangle vertexes
	for(int i = 0; i  < num_vertices; i++)
       center << center(0) + V(0,i), center(1) + V(1,i);
	
  //Divide the summed up center positions by the number of vertexes. 
  center = center/num_vertices;
}

//---------------- End of Triangle Class ------------------------------//