/**
 * Name  : Camill Folsom
 * Class : CAP 5726 Computer Graphics
 * Proj2 : Rasterization
 * Date  : October 21, 2018
 */

#include "mouse.h"

/**
 * @name : Default Constructor
 * @brief: Initializes all values to zero or false
 */
mouse::mouse() : count(0), lbutton_hold(false), initial_xpos(0), 
                 initial_ypos(0), triangle_num(0), nearest_vertice(0), 
                 called(false) {}

/**
 * @name : incrCount (Increment Count)
 * @brief: Add one to the variable used to track number of mouse clicks
 */
void mouse::incrCount()
{ ++count;}

/**
 * @name : getCount
 * @brief: Return the number of mouse clicks
 */
int mouse::getCount()
{ return count;}

/**
 * @name : setLhold (Set Left Mouse Button Hold)
 * @brief: Sets lbutton_hold to the passed in value. Used to see if the left
 *         mouse button is being held down by the user
 */
void mouse::setLhold(bool b)
{ lbutton_hold = b;}

/**
 * @name : lhold (Left Mouse Button Hold)
 * @brief: Used to test whether the left mouse button is being held down
 */
bool mouse::lhold()
{ return lbutton_hold;}

/**
 * @name : get_initial_x (Get Initial X Position)
 * @brief: Used to get the initial x position of the mouse
 */
double mouse::get_initial_x()
{ return initial_xpos;}

/**
 * @name : get_initial_y (Get Initial Y Position)
 * @brief: Used to get the initial y position of the mouse
 */
double mouse::get_initial_y()
{ return initial_ypos;}

/**
 * @name : set_initial_x (Set Initial X Position)
 * @brief: Used to set the initial x position of the mouse
 */
void mouse::set_initial_x(double x)
{ initial_xpos = x;}

/**
 * @name : set_initial_y (Get Initial y Position)
 * @brief: Used to set the initial y position of the mouse
 */
void mouse::set_initial_y(double y)
{ initial_ypos = y;}

/**
 * @name : set_triangle_num
 * @brief: Sets the number of the triangle the cursor is inside
 */
void mouse::set_triangle_num(int index)
{ triangle_num = index;}

/**
 * @name : get_triangle_num
 * @brief: Gets the number of the triangle the cursor is inside
 */
int mouse::get_triangle_num()
{ return triangle_num;}

/**
 * @name : set_vertice
 * @brief: Sets the vertex number the cursor is closest to
 */
void mouse::set_vertice(int vertice)
{ nearest_vertice = vertice;}

/**
 * @name : get_vertice
 * @brief: Gets the vertex number the cursor is closest to
 */
int mouse::get_vertice()
{ return nearest_vertice;}

/**
 * @name : set_called
 * @brief: Used to set the GLFW key callback to the original callback
 */
void mouse::set_called(bool b)
{ called = b;}

/**
 * @name : was_called
 * @brief: Returns the variable used to track if we should switch back to the
 *         original key callback
 */
bool mouse::was_called()
{ return called;}