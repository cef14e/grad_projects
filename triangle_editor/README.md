2D Triangle Soup Editor
=======================
This program is a interactive application that allows the user to add, edit, and
delete any number of triangles. This program allows for the insertion, movement,
rotation, scaling, and color change of each triangle. The scene's view
can also be modified allowing for the camera to be moved left, right, up, or down
and zoomed in or out. The programs last feature allows keyframing to be added to
each triangle individually.

Compilation
-----------
This program was compiled using cmake and Visual Studio 14 2015 to generate a
solution file. To compile, use the repo's root directory as the source and create
a empty build folder to hold the created files. The `ext` directory contains the
libraries for Eigen, GLFW, and GLEW. This directory is required for compilation.

Usage
-----
The file `triangle_editor.exe` is a working Windows bin executable and can be run
on a Windows machine. To create the executable from the build directory open
the solution file and build the solution.

Insertion Mode
--------------
To insert a triangle press the `i` key. When this mode is enabled, every three 
mouse clicks will create a triangle connecting the three locations where the mouse
has been pressed. The first click will create the starting point of the segment, 
which will be immediately visualized. As the mouse is moved, a preview of a segment
will appear. After the second mouse click, a preview of the triangle will appear. 
After the third click, the current preview will transform into the final triangle.

![insertion mode](https://bitbucket.org/cef14e/grad_projects/raw/a00bd41792f8017cd3aafe93b4065d7543e76a3b/comp_graphics/triangle_editor/images/insertion_mode.png "Insertion mode example")

Translation Mode
----------------
To move any triangle press the `o` key. In this mode Each mouse click will selected 
the triangle below the cursor (which will be highlighted), and every movement of 
the mouse (while keeping the button pressed) will result in a corresponding translation 
of the triangle.

![translation mode](https://bitbucket.org/cef14e/grad_projects/raw/68ea5517fc5febf7a220f6ac1a3782453915c6f8/comp_graphics/triangle_editor/images/translation.png "Translation mode example")

Deletion Mode
-------------
To delete a triangle press the `p` key to enter deletion mode. In this mode clicking
on a triangle will delete the triangle under the cursor.

Rotation/Scaling
----------------
When triangle translation mode is enabled, after selecting a triangle and releasing
the mouse cursor the triangle will stay selected. When the a triangle is selected
pressing the key `h` or `j` will rotate by 10 degrees clockwise or counter-clockwise,
respectively. These rotation are done around the triangle barycenter. When pressing
the `k` or `l` keys the triangle will be scaled up or down by 25%.

Color Mode
----------
To enable color mode press the `c` key. In this mode each triangles vertex's color
can be changed. Each mouse click selects the triangles vertex that is closets to the
cursor. After a vertex is selected pressing the keys 1-9 will change the color of the
vertex, with each key representing a different color. The color is interpolated 
linearly inside each triangle.

View Control
------------
To zoom the view press the `+` key to zoom in and the `-` key to zoom
out. Each key press will increase or decrease the zoom by 20%. Pressing the keys
`w`, `a`, `s`, or `d` will move the view up, left, down, or right by 20% of the 
window size, respectively.

Keyframing
----------
Keyframing is used to animate each triangle using linear interpolation to interpolate
in between each frame. Each triangle can be individually keyframed. To capture keyframes
you must be in translation mode. In translation mode a triangle can be selected.
Each press of the `m` key will capture a keyframe for the selected triangle. When 
capturing the keyframes the user can move, rotate, or scale the triangle. If each
action is captured as a keyframe the motion is preserved in the animation. Mixing
of moving, rotating, and scaling can cause the animation to have these properties
mixed into its animation. Any number of keyframes can be captured. To play the 
animation press the `z` key. The animation will play in a continuous loop until
another key, that is not `z`, is pressed.

Control Legend
--------------

* **'i'** : Insertion mode
* **'o'** : Translation mode
* **'p'** : Deletion mode
* **'h'** : Rotate triangle 10 degrees clockwise
* **'j'** : Rotate triangle 10 degrees counter-clockwise
* **'k'** : Scale up 25%
* **'l'** : Scale down 25%
* **'c'** : Color mode
* **'1'** : Green
* **'2'** : Blue
* **'3'** : Red
* **'4'** : White
* **'5'** : Cyan/Aqua
* **'6'** : Magenta / Fuchsia
* **'7'** : Yellow
* **'8'** : Black
* **'9'** : #FFB266
* **'+'** : Zoom in 20%
* **'-'** : Zoom out 20%
* **'w'** : Move camera up 20% of the window size
* **'a'** : Move camera left 20% of the window size
* **'s'** : Move camera down 20% of the window size
* **'d'** : Move camera right 20% of the window size
* **'m'** : Capture a keyframe for the selected triangle
* **'z'** : Plays the animation, any key except 'z' stops animation.
