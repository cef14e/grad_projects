/**
 * @name : Camill Folsom
 * @class: Concurent, Parellel, and Distributed Programming
 * @brieg: This program is the messenger server.
 */

#include <iostream>
#include <string>
#include <fstream>
#include <utility>
#include <vector>
#include <unordered_map>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

/**
 * Maps the user's name to a vector where the first element is the users's
 * password and the other elements define the users friends.
 */
std::unordered_map<std::string, std::vector<std::string>> usr_info;

/* Makes the online user to a pair that contains their ip and port number */
std::unordered_map<std::string, std::pair<std::string, std::string>> online;

/* Maps the user's name to the socket file descriptor they bind to */
std::unordered_map<std::string, int> usr_sockfds;
std::unordered_map<std::string, int>::const_iterator sock_it;

/* Holds all the socket file descriptors the program has open */
std::vector<int> sock_fds;

/* Makes a copy of the name of the user_info file passed in */
std::string arg_cpy;

/**
 * @name : parse_user_info (Parse User Info)
 * @brief: Parses the user info file into the map usr_info. It maps the users
 *         name to a vector that contians the user's password at position 0
 *         and the rest of the vector contains the user's friends.
 */
void parse_usr_info(std::string line)
{
   std::vector<std::string> friends;
   std::string token;
   std::string del = "|";
   std::string del2 = ";";
   std::string usr;
   size_t pos = 0;

   /* Parse on the | character, gets the username */
   pos = line.find(del);
   usr = line.substr(0, pos);
   line.erase(0, pos + del.length());

   /* Parse on the second | char, get the password */
   pos = line.find(del);
   friends.push_back(line.substr(0, pos));
   line.erase(0, pos + del.length());

   /* Parse on ; char, gets the users friends */
   while((pos = line.find(del2)) != std::string::npos)
   {
      friends.push_back(line.substr(0, pos));
      line.erase(0, pos + del2.length());
   }
   friends.push_back(line);

   /* Map the user's name to the vector with their password and friends */
   usr_info.emplace(usr, friends);
}

/**
 * @name : get_port (Get Port Number)
 * @brief: Parses the line passed in on a colon to get the port number from the
 *         config file
 */
int get_port(std::string line)
{
   std::string del = ": ";
   size_t pos = 0;

   /* Delete everything before : */
   pos = line.find(del);
   line.erase(0, pos + del.length());

   return stoi(line);
}

/**
 * @name : write_info (Write User Info)
 * @brief: Writes the usr_info map to the user_info file.
 */
void write_info()
{
   std::fstream ofile(arg_cpy);
   std::string l1;

   if(!ofile.is_open())
   {
      std::cout << "Faile to open: " << arg_cpy << "\n";
      return;
   }

   /* Loop through all entries in the usr_info map */
   for(auto &x: usr_info)
   {
      /* For each entry in the makes vecotr make the line to be writen to file*/
      for(unsigned i = 0; i < x.second.size(); ++i)
      {
         if(i == 0)
            l1 = x.first + "|" + x.second[i] + "|";
         else if(i == x.second.size()-1)
            l1 += x.second[i];
         else
            l1 += x.second[i] + ";";
      }
      l1 += "\n";
      std::cout << "l1: " << l1 << std::endl;

      /* Write the line to the file */
      ofile << l1;
      ofile.flush();
   }
   ofile.close();

}

/**
 * @name : proc_connect (Process Connect)
 * @brief: This is a thread that will handle each clients connection to the
 *         server.
 */
void *proc_connect(void *arg)
{
   int sockfd;
   ssize_t n;
   size_t pos = 0;
   const unsigned MAXBUFLEN = 512;
   char buf[MAXBUFLEN];
   std::string line, usr, cli_ip, update_line, usr2;
   std::vector<std::string> pass;
   std::pair<std::string, std::string> ip_port;
   int reg = 0;
   int login = 0;
   int to_concat = 0;
   std::string del = " ";

   /* Get the sockect file descriptor passed in */
   sockfd = *((int *)arg);
   free(arg);

   pthread_detach(pthread_self());

   /* Push a space back so pass[0] exists */
   pass.push_back(" ");

   /* Read what the client writes into buf */
   while((n = read(sockfd, buf, MAXBUFLEN)) > 0)
   {
      buf[n] = '\0';

      /* Check if the user is trying to register */
      if(strcmp(buf, "r") == 0 && reg == 0)
      {
         line = "Username: ";
         reg = 1;
      }
      else if(reg == 1)
      {
         /* Test if the username the user entered already exists */
         if(usr_info.find(buf) == usr_info.end())
         {
            reg = 2;
            line = "200";
            usr = buf;
         }
         else
         {
            line = "500";
            reg = 0;
         }
      }
      else if(reg == 2)
      {
         /* Sets first element of the vector to the password the user entered*/
         pass[0] = buf;
         usr_info.emplace(usr, pass);

         /* Write the update usr_info to the user_info file */
         write_info();

         line = "Registration Success";
         reg = 0;
      }
      /* If the user is trying to login prompt them with username */
      else if(strcmp(buf, "l") == 0 && login == 0)
      {
         line = "Username: ";
         login = 1;
      }
      else if(login == 1)
      {
         /* Check to see if the username enter does not exist */
         if(usr_info.find(buf) == usr_info.end())
         {
            line = "500";
            login = 0;
         }
         else
         {
            line = "Password: ";
 
            /* Save the username entered for this thread */
            usr = buf;
            login = 2;
         }
      
      }
      else if(login == 2)
      {
         /* Get the password the user entered */
         line = buf;

         /* Make sure the password the user entered matches there password */
         if(line == usr_info[usr][0])
         {
            line = "200";
            login = 3;
            
            /* Map this user to this sockets file descriptor */
            for(unsigned i = 0; i < sock_fds.size(); ++i)
               if(sock_fds[i] == sockfd)
                  usr_sockfds.emplace(usr, sock_fds[i]);
         }
         else
         {
            line = "500";
            login = 0;
         }
      }
      else if(login == 3)
      {
         /* Get the ip address the client sent to the server */
         cli_ip = buf;
         login = 4;
      }
      else if(login == 4)
      {
         /* Get the port number the client sent to the server */
         line = buf;
         ip_port = std::make_pair(cli_ip, line);
         online.emplace(usr, ip_port);

         /* The line to be sent to the user who just logged in */
         line = " ";

         /* The line to send to all the usr on this user's friends list */
         update_line = "u " + usr + "," + ip_port.first + "," + ip_port.second;

         /* Checks if we need to concat line and used to count friends online */
         to_concat = 0;

         /* Loop through all friend, start at one since 0 is user's password */
         for(unsigned i = 1; i < usr_info[usr].size(); ++i)
         {
            for(auto &x: online)
            {
               /** 
                * Check whether one of the users online is a friend of this 
                * user 
                */
               if(x.first == usr_info[usr][i])
               {
                  /** 
                   * Make the line that contians the online user and that users 
                   * location info 
                   */
                  if(to_concat == 0)
                  {
                     line = x.first + "," + x.second.first + "," + x.second.second;
                     ++to_concat;
                  }
                  else
                  {
                     line = line + ";" + x.first + "," + x.second.first + "," 
                            + x.second.second;
                     ++to_concat;
                  }
                  std::cout << "Online : " << x.first << std::endl;
               }
            }

            /* Loop through all online users active sockets */
            for(auto &x: usr_sockfds)
            {
               /* If the user is a friend of this user write to that user */
               if(x.first == usr_info[usr][i] && x.second != sockfd)
               {
                  write(x.second, update_line.c_str(), update_line.length());
               }
            }
         }
         /* Write the location info to this user */
         write(sockfd, line.c_str(), line.length());

         /* Set line to the number of friends online */
         line = std::to_string(to_concat);
         std::cout << "users online: " << line << std::endl;

         login = 0;
      }
      else if(buf[0] == 'm')
      {
         line = buf;

         /** 
          * Erase "m " from the beginning of line, make line equal to user who
          * is recieving message
          */
         line.erase(0,2);

         /* Let the user recieving the message know it can accept connection */
         if((sock_it = usr_sockfds.find(line)) != usr_sockfds.end())
         {
            write(sock_it->second, buf, sizeof(buf));
         }
         continue;
      }
      else if(buf[0] == 'i' && buf[1] == 'a')
      {
         line = buf;

         /* Erase "ia " from start of line */
         line.erase(0,3);

         /* Parse line on space, get user whom we are accepting the invite */
         pos = line.find(del);
         usr2 = line.substr(0, pos);

         /* Makes line equal the message the user entered */
         line.erase(0, pos + del.length()); 

         /* Send to user who sent the invite */
         update_line = "iam " + usr + " " + line + "," + usr + "," + 
                       ip_port.first + "," + ip_port.second;
           
         /* Send to user who accepted invite */
         line = "ia " + usr2 + "," + online[usr2].first + "," + 
                online[usr2].second;

         /* Update both usr info with the new friends */
         usr_info[usr].push_back(usr2);
         usr_info[usr2].push_back(usr);
      
         /* Update user_info file */
         write_info();

         /* Write to the user whi sent the invite */
         if((sock_it = usr_sockfds.find(usr2)) != usr_sockfds.end())
            write(sock_it->second, update_line.c_str(), update_line.length());
         
      }
      else if(buf[0] == 'i')
      {
         line = buf;
  
         /* Erase "i " from start of line */
         line.erase(0,2);

         /* Parse on space to get user we are sending invite to */
         pos = line.find(del);
         usr2 = line.substr(0, pos);

         /* Makes line the message the user sent */
         line.erase(0, pos + del.length());

         /* Create the line to be sent to the user recieving invite */
         line = "i " + usr + " " + line;

         /* Write to the user who is recieving the invite */
         if((sock_it = usr_sockfds.find(usr2)) != usr_sockfds.end())
            write(sock_it->second, line.c_str(), line.length());

         continue;
      }
      else if(strcmp(buf, "logout") == 0)
      {
         /* Used to count number of friend online */
         to_concat = 0;

         /* Set the line to tell the clients remove this user's location info */
         line = "ud " + usr;

         online.erase(usr);

         /* Wrutes the line to all of this user's friend who are online */
         for(unsigned i = 1; i < usr_info[usr].size(); ++i)
         {
            for(auto &x: online)
            {
               if(x.first == usr_info[usr][i])
               { 
                 if((sock_it = usr_sockfds.find(x.first)) != usr_sockfds.end()) 
                    write(sock_it->second, line.c_str(), line.length());
               }
            }
         }

         continue;
      }
      else
         line = "Invalid Input";

      /* Write the line to the client */
      write(sockfd, line.c_str(), line.length());
   }

   /* Check to see if the client crashed */
   if(n == 0)
      std::cout << "client closed\n";
   else
      std::cout << "A problem occured.\n";

   close(sockfd);
   return NULL;
}

/**
 * @name: exit_sig
 * @brief: Handles the SIGINT signal, makes sure all sockets are closed
 */
void exit_sig(int signo)
{
   std::cout << std::endl;

   /* Close all open sockets */
   for(unsigned i = 0; i < sock_fds.size(); ++i)
       close(sock_fds[i]);

   exit(signo);
}

int main(int args, char *argv[])
{
   std::string line;
   unsigned port;
   int serv_sockfd, cli_sockfd, *sock_ptr;
   struct sockaddr_in serv_addr, cli_addr;
   socklen_t sock_len;
   pthread_t tid;

   /* Catch SIGINT */
   signal(SIGINT, exit_sig);

   if(args != 3)
   {
      std::cout << "usage : messenger_server user_info_file configration_file\n";
      return 1;
   }

   /* Copy the name of the user_info file */
   arg_cpy = argv[1];

   /* Open the user info file */
   std::ifstream file(argv[1]);
   std::ifstream conf_file(argv[2]);

   /* Test if user info file is open */
   if(file.is_open())
   {
      while(getline(file, line))
      {
         /* Set the usr_info map */
         parse_usr_info(line);
      }
      file.close();
   }
   else
      std::cout << "Unable to open file : " << argv[1] << std::endl;

   /* Test if config file is open */
   if(conf_file.is_open())
   {
      serv_sockfd = socket(AF_INET, SOCK_STREAM, 0);

      bzero((void*) &serv_addr, sizeof(serv_addr));

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

      while(getline(conf_file, line))
      {
         /* Set the usr_info map */
         port = get_port(line);
         serv_addr.sin_port = htons(port);
      }
      conf_file.close();

      bind(serv_sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

      /* Get the servers bound info */
      sock_len = sizeof(serv_addr);
      getsockname(serv_sockfd, (struct sockaddr*) &serv_addr, &sock_len);

      std::cout << "ip: " << getenv("HOSTNAME") << " port: " 
                << ntohs(serv_addr.sin_port) << std::endl;

      listen(serv_sockfd, 5);

      /* Save the servers socket so it can be closed on SIGINT */
      sock_fds.push_back(serv_sockfd);

      for(;;)
      {
         sock_len = sizeof(cli_addr);
         cli_sockfd = accept(serv_sockfd, (struct sockaddr *)&cli_addr, &sock_len);
     
         std::cout << "remote client IP == " << inet_ntoa(cli_addr.sin_addr);
         std::cout << ", port == " << ntohs(cli_addr.sin_port) << std::endl;

         sock_ptr = (int *)malloc(sizeof(int));
         *sock_ptr= cli_sockfd;

         sock_fds.push_back(cli_sockfd);

         pthread_create(&tid, NULL, &proc_connect, (void *)sock_ptr);
      }
   }
   else
      std::cout << "Unable to open file : " << argv[2] << std::endl;

   return 0;

}
