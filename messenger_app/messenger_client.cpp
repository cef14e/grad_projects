/**
 * @name : Camill Folsom
 * @class: Concurrent, Parallel, and Distributed Programming
 * @brief: This program is the messenger client.
 */

#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fstream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <utility>
#include <unordered_map>
#include <vector>
#include <signal.h>

int sockfd, serv_sockfd;
int login = 0;

/**
 * @name : printlogin
 * @brief: Prints the login commands the user may use 
 */
void printlogin()
{
   std::cout << "---- Allowed Commands ---\n";
   std::cout << " r    register\n";
   std::cout << " l    login\n";
   std::cout << " exit quit\n";
   std::cout << "-------------------------\n";
}

/**
 * @name : menu
 * @brief: Prints the command the user my enter after logging in
 */
void menu()
{
   std::cout << "-------------------- Allowed Commands ------------------------------------\n";
   std::cout << " m user message: send a user a message\n";
   std::cout << " i user [message]: send a friend request to another user";
   std::cout << " with a message\n";
   std::cout << " ia user [meassage]: accecpt invite from user and send that";
   std::cout << " user a message\n";
   std::cout << " logout: logout from server and return to the login menu\n";
   std::cout << "--------------------------------------------------------------------------\n";
}

/**
 * @name : find_invite
 * @brief: Loops through all invite from users. Returns true if invite found
 */
bool find_invite(std::vector<std::string> &invites, std::string usr)
{
   for(unsigned i = 0; i < invites.size(); ++i)
   {
      /* If the usr is in invites then an invite has be sent to this user */
      if(usr == invites[i])
      {
         invites.erase(invites.begin()+i);
         return true;
      }
   }
   return false;
}

/**
 * @name : parse_message
 * @breif: Assumes line contains user message. Sets usr and message
 */
void parse_message(std::string line, std::string &usr, std::string &message, 
                   std::string del)
{
   std::string parsed;
   size_t pos = 0;

   /* Parse on the delimeter passed in */
   pos = line.find(del);
   usr = line.substr(0, pos);
   line.erase(0, pos + del.length());

   message = line;
}

/**
 * @name : parse_a_space
 * @bruef: Given a line parse on a single space to grab first element 
 */
void pasrse_a_space(std::string &line, std::string &set)
{
   size_t pos = 0;
   pos = line.find(" ");
   set = line.substr(0, pos);
}

/**
 * @name : parse_commas
 * @brief: This is a helper function for pasre_friends. It parses on commas.
 */
void parse_commas(std::string line, std::string &usr, std::pair<std::string, 
                  std::string> &ip_port)
{
   std::string ip, port;
   std::string del = ",";
   int count = 0;
   size_t pos = 0;

   while((pos = line.find(del)) != std::string::npos)
   {
      if(count == 0)
         usr = line.substr(0, pos);
      else if(count == 1)
         ip = line.substr(0, pos);
      else
      {
         std::cout << "error reading in friend location info: " << line << "\n";
         break;
      }
      line.erase(0, pos + del.length());
      ++count;
   }
   port = line;
   ip_port = std::make_pair(ip, port);
}

/**
 * @name : parse_friend
 * @brief: Sets the location info for the user's friends who are online. It 
 *         parses the string received from server to update its location info.
 */
void parse_friends(
     std::unordered_map<std::string, std::pair<std::string, std::string>> &on, 
     std::string line)
{
   std::pair<std::string, std::string> ip_port;
   std::string tok, tmp, usr, del1, del2, ip, port;
   size_t pos = 0;
   del1 = ";";
   del2 = ",";

   if(line.empty() || line[0] == ' ')
      return;

   /* Parse the line on ; to get a line that has contains "usr,ip,port" */
   if(((pos = line.find(del1)) == std::string::npos) && line[0] != ' ')
   {
      parse_commas(line, usr, ip_port);
      on.emplace(usr, ip_port);
   }

   /* if there are remaining friends parse them as well */
   while((pos = line.find(del1)) != std::string::npos)
   {
      tmp = line.substr(0, pos);
      line.erase(0, pos + del1.length());

      parse_commas(tmp, usr, ip_port);
      on.emplace(usr, ip_port);
   }
   parse_commas(line, usr, ip_port);

   /* Updates the online users location info */
   on.emplace(usr, ip_port);
}

/**
 * @name : connect_to_server
 * @brief: Connects a client to a server
 */
bool connect_to_server(int &sock_fd, const char *ip, const char* port)
{
   struct addrinfo hints, *res, *ressave;
   int flag, rv;

   bzero(&hints, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;

   if((rv = getaddrinfo(ip, port, &hints, &res)) != 0)
   {
      std::cout << "getaddrinfo wrong: " << gai_strerror(rv) << std::endl;
      return false;
   }

   ressave = res;

   /* Sets flag to 0 so we can test for success at the end */
   flag = 0;

   /* Attempt to connect to the hostname or ip address */
   do
   {
      sock_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
      if(sock_fd < 0)
         continue;
      if(connect(sock_fd, res->ai_addr, res->ai_addrlen) == 0)
      {
         flag = 1;
         break;
      }
      close(sock_fd);
   } while((res = res->ai_next) != NULL);

   freeaddrinfo(ressave);

   /* If flag was not set in the loop above then we failed to connect */
   if(flag == 0)
   {
      fprintf(stderr, "cannot connect\n");
      return false;
   }

   return true;
}

/**
 * @name : exit_sig
 * @brief: Handle the SIGINT signal. This function informs the server it has
 *         logged out. Next it closes any open sockets.
 */
void exit_sig(int signo)
{
   std::string line = "logout";

   std::cout << std::endl;

   if(login > 1)
   {
      write(sockfd, line.c_str(), line.length());
      close(serv_sockfd);
   }

   close(sockfd);

   exit(signo);
}

int main(int argc, char **argv)
{
   const unsigned MAXBUFLEN = 512;
   char buf[MAXBUFLEN], buf_cpy[MAXBUFLEN];
   int n, maxf, cli_sockfd;
   std::string key, val, ip, conf_port, line, usr, message, this_usr;
   fd_set rset, orig_set;
   struct sockaddr_in serv_addr, cli_ip, cli_addr;
   const unsigned port = 7100;
   socklen_t sock_len; 
   std::unordered_map<std::string, std::pair<std::string, std::string>> online;
   std::vector<std::string> invites;
   size_t pos = 0;
   bool get_user = false;
   std::string del = " ";
   int r_count = 0;

   /* Catch SIGINT */
   signal(SIGINT, exit_sig);

   login = 0;

   if(argc != 2)
   {
      std::cout << "messenger_client conf_file\n";
      return 1;
   }

   /* Open config file */
   std::ifstream file(argv[1]);

   if(!file.is_open())
   {
      std::cout << "Failed to open file: " << argv[1] << std::endl;
      return 1;
   }

   /* Parse the file on spaces */
   while(file >> key >> val)
   {
      if(key == "servhost:")
         ip = val;
      if(key == "servport:")
      {
         conf_port = val;
         break;
      }
   }

   /* Connect to the server that was specified in the config file */
   if(!connect_to_server(sockfd, ip.c_str(), conf_port.c_str()))
      return 1;

   /* Get the ip address of this client */
   sock_len = sizeof(cli_ip); 
   getsockname(sockfd, (sockaddr*) &cli_ip, &sock_len);

   std::cout <<  "\nWelcome to the Messenger Client\n";
   printlogin();

   FD_ZERO(&orig_set);
   FD_SET(STDIN_FILENO, &orig_set);
   FD_SET(sockfd, &orig_set);

   if(sockfd > STDIN_FILENO)
      maxf = sockfd + 1;
   else
      maxf = STDIN_FILENO + 1;

   while(1)
   {
      rset = orig_set;

      select(maxf, &rset, NULL, NULL, NULL);

      if(FD_ISSET(sockfd, &rset))
      {
         /* Read from the server into buf */
         if((n = read(sockfd, buf, MAXBUFLEN)) == 0)
         {
            close(sockfd);

            /* Close the server socket if the user has logged in */
            if(login == 1)
               close(serv_sockfd);

            std::cout << "server crashed\n";
            return 0;
         }
         buf[n] = '\0';

         /* Deals with invalid username or password entry by user */
         if(strcmp(buf, "500") == 0)
         {
            std::cout << "Invalid entry re-enter username and password.\n";
            std::cout << "Username:\n";

            /* Reset r_count so the user can restart registration */
            r_count = 1;

            /* Lets server know we want to restart logging in or registering */
            if(login == 1)
               line = "l";
            else
               line = "r";

            get_user = true;

            /* Write to the server and eat up the Username response */
            write(sockfd, line.c_str(), line.length());
            read(sockfd, buf, MAXBUFLEN);
         }
 
         /* Deals with valid registration username */
         else if(strcmp(buf, "200") == 0 && login < 1)
            std::cout << "Password:\n";
     
         /* Deals with valid username and password for logging in */       
         else if(strcmp(buf, "200") == 0 && login == 1)
         {
            std::cout << "login success\n";
            menu();

            /* Sets login to 2 so we know the login process has finished */
            login = 2;

            /* Send ip address to server */
            line = inet_ntoa(cli_ip.sin_addr);
            write(sockfd, line.c_str(), line.length());
            read(sockfd, buf, MAXBUFLEN);
                      
            /* Send port number to server */
            line = std::to_string(port);
            write(sockfd, line.c_str(), line.length());
            n = read(sockfd, buf, MAXBUFLEN);

            if(n == 0)
            {
               std::cout << "server crashed\n";
               close(sockfd);
               return 0;
            }

            buf[n] = '\0';

            /* Sets the location info of the users friends that are online */
            parse_friends(online, buf);

            /* Get the number of friend that are online from server */
            n = read(sockfd, buf, MAXBUFLEN);
            buf[n] = '\0';
            std::cout << "number of friends online: " << buf << std::endl;

            std::cout << "friends online: ";
            for(auto &x: online)
               std::cout << x.first << " ";

            std::cout << std::endl;

            /* Set up this client to accept connection request */
            serv_sockfd = socket(AF_INET, SOCK_STREAM, 0);

            bzero((void*) &serv_addr, sizeof(serv_addr));

            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
            serv_addr.sin_port = htons(port);

            bind(serv_sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
            listen(serv_sockfd, 5);
         }

         /* Used to update the online users when a user goes offline */
         else if(buf[0] == 'u' && buf[1] == 'd')
         {
            line = buf;
            line.erase(0,3);
            
            /* Remove the supplied user from the online structure */
            if(!online.empty())
               online.erase(line);

            std::cout << line << " has gone offline you now have " << online.size()
                      << " friends online\n";
         }

         /* Used to update the online users when a user goes online */
         else if(buf[0] == 'u')
         {
            for(unsigned i = 0; i < MAXBUFLEN; ++i)
            {
               if(buf[i] == '\0')
               {
                  buf_cpy[i-2] = '\0';
                  break;
               }
               else if(i > 1)
                  buf_cpy[i-2] = buf[i];
            }     
                  
            /* Save the location info for all online friends */
            parse_friends(online, buf_cpy);

            for(auto &x: online)
            {
               std::cout << x.first << " has come online.\n";
               break;
            }
         }
         else if(buf[0] == 'm')
         {
            /* Accept the connection request from another client */
            sock_len = sizeof(cli_addr);
            cli_sockfd = accept(serv_sockfd, (struct sockaddr*) NULL, NULL);

            /* Read the message the other client has sent  */
            if((n = read(cli_sockfd, buf, MAXBUFLEN)) == 0)
            {
               std::cout << "Failed to read message.\n";
               continue;
            }       

            buf[n] = '\0';

            std::cout << buf << std::endl;
            close(cli_sockfd);
         }
         else if(buf[0] == 'i' && buf[1] == 'a')
         {
            line = buf;

            /* Check to see if this user is the user who sent the invite */
            if(line[2] == 'm')
            {
               line.erase(0,4);

               /* Get the user who accepted invite */
               pos = line.find(del);
               usr = line.substr(0, pos);
               line.erase(0, pos+del.length());
           
               /* Get the message the user sent */
               del = ",";
               pos = line.find(del);
               message = line.substr(0, pos);

               /* Makes line the info needed to add location info */
               line.erase(0, pos+del.length());
               del = " ";

               std::cout << "You invite was accepted by:\n" << usr << " << "
                         << message << "\n";
            }
            else
               line.erase(0,3);

            /* Add the new friend location info */
            parse_friends(online, line);
         }
         else if(buf[0] == 'i')
         {
            line = buf;
            line.erase(0, 2);
           
            /* Get the user who sent the invite and add them to the invites */
            pos = line.find(del);
            invites.push_back(line.substr(0, pos));
 
            std::cout << "You have received a friend request from:\n" << line
                      << std::endl;
         }
         else
         {
            /* Prints whatever the server echoed. */
            std::cout << buf << std::endl;
 
            /* Used to see if the username is being entered by the user */
            if(strcmp(buf, "Username: ") == 0)
               get_user = true;
         }
      }

      if(FD_ISSET(STDIN_FILENO, &rset))
      {
         /* Get the users input */
         if(getline(std::cin, line) == NULL)
         {
            if(login > 1)
            {
               /* Make sure the user logout from server */
               line = "logout";
               write(sockfd, line.c_str(), line.length());
               close(serv_sockfd);
            }

            close(sockfd);

            return 0;
         }
         else
         {
            if(get_user)
            {
               this_usr = line;
               get_user = false;
            }

            /* If the user has logged in then these commands no longer work */
            if((line == "l" || line == "r" || line == "exit") && login > 1)
            {
               std::cout << "Invalid Input\n";
               continue;
            }

            /* Handles closing the client */
            else if(line == "exit")
            {
               close(sockfd);
               if(login > 1)
                  close(serv_sockfd);
               return 0;
            }
           
            /* Starts the registration process */
            else if(line == "r")
               r_count = 1;

            /**
             * Make sure the proper number of user entries are sent to the 
             * server when the user is registering
             */
            else if(r_count > 0 && r_count < 3)
               ++r_count; 

            /* Starts the login processes */
            else if(line == "l" && login != 2)
               login = 1;
            else if(line[0] == 'm' && login > 1)
            {
               line.erase(0, 2);
 
               /* Parse the line the user entered into usr and message */
               parse_message(line, usr, message, " ");


               /* Check to see we have this user location info */
               if(online.find(usr) == online.end())
               {
                  std::cout << usr << " is not online or is not one of your"; 
                  std::cout << " friends\n";
                   continue;
               }

               /* Has server tell user to accept connection from this client */
               line = "m " + usr;
               write(sockfd, line.c_str(), line.length()); 
 
               /* Connect this client to the other client */
               if(!connect_to_server(cli_sockfd,  online[usr].first.c_str(),
                                     online[usr].second.c_str()))
               {
                  std::cout << "Failed to connect to " << usr << "\n";
                  continue;
               } 
            
               /* Makes the message and writes it to the other client */
               message = this_usr + " >> " + message;
               write(cli_sockfd, message.c_str(), message.length());

               close(cli_sockfd);
               continue;
            }
            else if(line[0] == 'i' && line[1] == 'a' && login > 1)
            {
               line.erase(0,3);

               /* Parse the user input into usr and message */
               parse_message(line, usr, message, " ");

               /* Requires user to put brackets around their message */
               if(message[0] != '[' || message[message.length()-1] != ']')
               {
                  std::cout << "Message must be in brackets, [message] try"
                            <<  " again.\n";
                  continue;
               }

               /* Erase the brackets from the message */
               message.erase(0,1);
               message.erase(message.length()-1, 1);

               /* Check to see if the user has sent this user an invite */
               if(!find_invite(invites, usr))
               {
                  std::cout << "You do not have an invite from: " << usr
                            << std::endl;
                  continue;
               }
               else
                  std::cout << "You and " << usr << " are now friends.\n";

               /* send the invite acceptance to the server */
               line = "ia " + usr + " " + message;
            }
            else if(line[0] == 'i' && login > 1)
            {
               line.erase(0, 2);

               /* Parse the user input into usr and message */
               parse_message(line, usr, message, " ");

               /* Make sure the user is not already this users friend */
               if(online.find(usr) != online.end())
               {
                  std::cout << "You and " << usr << " are already friends.\n";
                  continue;
               }

               /* Requires user to put brackets around their message */
               if(message[0] != '[' || message[message.length()-1] != ']')
               {
                  std::cout << "Message must be in brackets, [message] try"
                            <<  " again.\n";
                  continue;
               }

               /* Erase the brackets from the message */
               message.erase(0,1);
               message.erase(message.length()-1, 1);

               /* send the invite to the server */
               line = "i " + usr + " >> " + message;
            }
            else if(line == "logout")
            {
               /* Close the socket this client listens on */
               close(serv_sockfd);
               login = 0;
               
               /* Delete the online friends location info */
               if(!online.empty())
                  online.clear();

               std::cout << "You have logout from the server.\n";

               printlogin();
               
            }
 
            /* Write to the server */
            write(sockfd, line.c_str(), line.length());
         }
      }
   }

   return 0;
}
