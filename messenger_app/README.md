Messenger Application
=====================
This project contains two programs a messenger client and a messenger server.
The client is used to register an account with the server. After successful
registration the user my login to there account. The user is not automatically
logged into the server upon successful registration. If the user has not logged in
to their account they may exit the client.

After logging into the server the user is allowed to message another user, that
is one of their friends. They may invite other users to be the friend and send
that user a message with the invite. They can also accept the invite from a user
and send a message to confirm acceptance. Finally, the user may logout and return
to the login menu.

The _Project2.html_ file contains the instructions for the assignment.

Compilation
-----------
The makefile makes the executables server and client. The server is created using 
the following command:
```
g++ -pthread -Wall -ansi -pedantic -std=c++1y messenger_server.cpp -o server
```

The client is created using the following command:
```
g++ -Wall -ansi -pedantic -std=c++1y messenger_client.cpp -o client
```

Usage
-----
The server reads in two file user_info and a configuration file. The user_info file
contains the each user's name, password, and friends. The configuration file 
contains the port number we wish to bind the server to.
```
./server user_info configuration  
```

The client reads in one file client_configuration. The client configuration file
contains the server's host name or IP address and the port number the server is bound to.
```
./client client_configuration
```

Allowed Commands
----------------

**r**    : Prompt the user to enter a username, if that user exist report error and
           re-prompt the user. Given a good username prompt the user to enter a
           password. Once the user enters a password registration is complete and
           the user my now log into the server.

**l**    : Prompt the user to enter a username if no user by that name re-prompt the
           user to enter a username. After entering the username the user will be
           prompted to enter a password. Given the correct password the user is 
           logged into the server.

**exit** : Allows the user to exit the program.

After a user successfully logs in they may no longer use the commands above.
Now that they are logged into the server they may issue the following commands:

**m user message**    : Send the specified user a message.

**i user [message]**  : Send an invite to the specified user. The brackets must be
                        included around the message they wish to send. If they wish
                        to send a blank message the can use empty brackets [].

**ia user [message]** : Accept an invite from the specified user and send them a
                        message. The brackets are required for a message.

**logout**            : Logs the user out of there account and returns them to the
                        login menu.

Limitations
-----------

**Invites** : If the user receive an invite they may logout of there account and then
              they can log back in and accept the invite. However, since the server
              does not track the invites user's have sent to one another once the
              client program ends then the user restarts the client and logs in they
              will not longer know that an invite was sent to them.

**Server**  : Sometimes when the server starts it binds to port 0. To fix this the
              port the server binds to can be change or the server can be restarted
              until it binds to the correct port.

**Messages**: Given many clients logged in sometime a client accepting a connection
              from another client will block forever, Ctrl-C will exit the client 
              when this occurs. I assume this happens since all clients bind to the
              same port number.
              Sometimes sending a message will cause the client to accepting the
              connection to block forever. This can be fixed by changing the port
              number the client binds to.

Writing user_info file can put ";" before the first friend. However, this does
not affect how the program runs.

