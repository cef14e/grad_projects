Optimizations
=============
Optimizations written for the graduate class Advanced Topics in Compilation 
taken in Summer 2018.  

Each program uses classes that are not included because they were created by the 
instructor. These programs optimize the compiler by producing optimized assembly
code for programs written in the SPARC assemble language. The programs used to test 
the programs are the following: banner.s, cal.s, echo.s, find.s, half.s, queens.s,
square.s, and wc.s. The optimizations performed are the following: local copy and 
constant propagation, local common sub-expression elimination, dead assignment 
elimination, filling delay slots, peephole optimization, and register allocation. 
Two programs performs analysis and detection on the code instead of optimizing it. 
These are live and dead variable analysis and loop detection.

Contents
--------

+-----------------+------------------------------------------------------------+
| Entry           | Description                                                |
+=================+============================================================+
| src             | DIR - Source code for each optimization.                   |
+-----------------+------------------------------------------------------------+
| instructions    | DIR - Assignment instructions.                             |
+-----------------+------------------------------------------------------------+
| tests           | DIR - Programs used for testings each projects code.       |
+-----------------+------------------------------------------------------------+
| impact_analysis | Tables that show the impact on each of the test files for  |
|                 | each optimization.                                         |
+-----------------+------------------------------------------------------------+
| README          | This file.                                                 |
+-----------------+------------------------------------------------------------+

Test Programs
-------------
+----------+-------------------------------------------------------------------+
| Program  | Description                                                       |
+==========+===================================================================+
| banner.s | Displays the user entered string as a banner.                     |
+----------+-------------------------------------------------------------------+
| cal.s    | Displays a calender for a given year.                             |
+----------+-------------------------------------------------------------------+
| echo.s   | Prints the user entered string.                                   |
+----------+-------------------------------------------------------------------+
| find.s   | Searches for files in a directory hierarchy.                      |
+----------+-------------------------------------------------------------------+
| half.s   | Prints the number divided by 2.                                   |
+----------+-------------------------------------------------------------------+
| queens.s | Simulates chess moves.                                            |
+----------+-------------------------------------------------------------------+
| square.s | Prints the squared value of a number.                             |
+----------+-------------------------------------------------------------------+
| wc.s     | Prints newline, word, and byte counts for each file.              |
+----------+-------------------------------------------------------------------+

Optimizations
-------------
Local Copy Propagation : copyprop.c
    This transformation will occur when the source register of a 
    register-to-register move instruction can replace later uses of
    the destination register within the same basic block.

Local Constant Propagation : constprop.c
    This transformation will occur when the constant source operand of
    a constant-to-register move instruction can replace later uses of 
    the destination register within the same basic block.

Local Common Subexpression Elimination : cse.c
    Detects when an arithmetic instruction assigns a value that has already been
    calculated in the same block of code. When this occurs, the redundant
    arithmetic instruction is changed to a register-to-register move.

Dead Assignment Elimination : deadasgs.c
    Determines when assignments to variables or registers are dead. A dead
    assignment means that the variable or register is set, but it is not used 
    and thus can be deleted.

Fill Delay Slots : fillslots.c
    Fills the delay slots for calls, jumps, and branches (not returns) with an 
    instruction that proceeds the transfer of control in the same basic code 
    block.

Register Allocation : regalloc.c
    Allocates local variables and arguments in a function to registers.

Branch Chaining : chains.c
    Blocks that contain only a branch instruction are eliminated and the targets
    of any block that branches to that block are updated to branch to the 
    eliminated blocks target.

Reversing Branches : reverse.c
    Eliminates unconditional jumps by reversing the branch condition.

Peephole Optimization : peephole.c
    Reads in a set of peephole optimization rules from a file named rules.txt in
    the current directory. The optimization rules are then applied to the code.

Analysis and Detection
----------------------
Live and Dead Variable Analysis : livedeadvars.c
    Calculates when variables and registers are live or dead in the control flow.
    We define a dead variable as the last use of that value in that variable.

Loop Detection : loops.c
    Calculates the dominator information for each block. This information is used 
    to calculate the natural loops in each block. The loop information is then
    sorted in a linked listed with the most deeply nested loop listed first.
    In case of ties (two or more loops at the same nesting level), the loops
    are ordered by the order that their header blocks appear in the assembly 
    (lower header block number first).
    