/**
 * @name : Camill Folsom
 * @proj : Loop Detection
 */

#include <stdio.h>
#include "opt.h"
#include "misc.h"
#include "analysis.h"
#include "vect.h"

#include "flow.h"

struct loopnode *loops;    /* head of linked list of loops */

/*
 * addtoloop - adds the blocks in between the head and tail to the loop
 */
void addtoloop(struct bblk *cblk, struct bblk *head, struct blist *templist)
{
   struct blist *bptr, *bptr2;

   /* loop throught the blocks */
   for(bptr = cblk->preds; bptr; bptr = bptr->next) 
   { 
      /* End of recursion when the predecessors list contains the head */
      if(inblist(bptr, head))
      {
         addtoblist(&head->loop->blocks, cblk);
 
         /* Add the blocks that were in the path from the tail to this block */
         if(templist)
            for(bptr2 = templist; bptr2; bptr2 = bptr2->next)
               addtoblist(&head->loop->blocks, bptr2->ptr);

         if(!(templist))
            freeblist(templist);
      }
      else
      {
          /* Keep track of the blocks to be added in the path from the tail */
          addtoblist(&templist, cblk);

         /* Make sure this block is not a header */
         if(!(cblk->loop))
            addtoloop(bptr->ptr, head, templist);

         /** 
          * Deals with a nested header's predecessor block that is not part of
          * its loop
          */
         else if(!inblist(cblk->loop->blocks, bptr->ptr))
            addtoloop(bptr->ptr, head, templist);

         /* Adds the nested header's loop to the current loop */
         else
         {
            for(bptr2 = cblk->loop->blocks; bptr2; bptr2 = bptr2->next)
            {
               addtoblist(&head->loop->blocks, bptr2->ptr);

               /* Increment loopnest for ecvery block in the nested loop */
               ++bptr2->ptr->loopnest;
            }
         }
            
      }
   }
}


/*
 * findloops - locate loops in the program and build the loop data structure
 */
void findloops()
{
   struct bblk *cblk, *tblk;
   extern struct bblk *top;
   struct blist *bptr, *templist;
   bvect oldvect;
   struct loopnode *lptr, *lptr2;
   int change = 0;
   templist = (struct blist *) NULL;
   
   /* Setting the doms for each block */

   /* Set the first dom itself */
   binsert(&top->dom, top->num);
   oldvect = ball();

   /* Set all the other blocks doms equal to all blocks except the top */
   for(cblk = top->down; cblk; cblk = cblk->down)
      bcpy(&cblk->dom, ball());

   do {
      change = 0;

      for(cblk = top->down; cblk; cblk = cblk->down)
      {
       if(!bequal(oldvect, ball()))
          oldvect = ball();

        /* Set oldvect equal to the current blocks dominators */
        binter(&oldvect, cblk->dom);

         /* Intersection of the predecessors of the current block */
         for(bptr = cblk->preds; bptr; bptr = bptr->next)
         {
            binter(&cblk->dom, bptr->ptr->dom);
         }

         /* Make sure cblk dominates itself */
         binsert(&cblk->dom, cblk->num);

         if(!bequal(cblk->dom, oldvect))
            change = 1;
      }

   } while (change == 1); 

   bfree(oldvect);

   /* Finding natural loops */

   for(cblk = top; cblk; cblk = cblk->down)
   {
      for(bptr = cblk->succs; bptr; bptr = bptr->next)
      {
         /* Find the tail of the loop */
         if(bin(cblk->dom, bptr->ptr->num))
         {
            /* *
             * If the loop does not exist for this header block then create
             * it and add the header and tail to its loopnode.
             */
            if(!(bptr->ptr->loop)) 
            {
               bptr->ptr->loop = newloop();
               bptr->ptr->loop->header = bptr->ptr;

               /* Add the header */
               addtoblist(&bptr->ptr->loop->blocks, bptr->ptr);
            
               /* Add the tail */
               addtoblist(&bptr->ptr->loop->blocks, cblk);
             
               if(!(templist))
                  freeblist(templist);

               /* Check that the predecessor and successor are differnet */
               if((cblk->preds) && cblk->preds->ptr->num != bptr->ptr->num)
                  addtoloop(cblk, bptr->ptr, templist);
            }

            /** 
             * If the loop was already created for this block then dont
             * make it again 
             */
            else
            {
               if(!(templist))
                  freeblist(templist);

               addtoloop(cblk, bptr->ptr, templist);
            }
         }
      }
   }

   /* Sort the blocks by how the appear in the assemble code */
   for(lptr = loops; lptr; lptr = lptr->next)
   {
      for(lptr2 = lptr->next; lptr2; lptr2 = lptr2->next)
      {
         /* Order by lowest block position in the code */
         if(lptr->header > lptr2->header)
         {
            /* Swap the headers */
            tblk = lptr2->header;
            lptr2->header = lptr->header;
            lptr->header = tblk;

            if(!(templist))
               freeblist(templist);

           /* Swap the blists */
           templist = lptr2->blocks;
           lptr2->blocks = lptr->blocks;
           lptr->blocks = templist;         
         }
             
      }
   }

   /* Sort the blocks by the loopnest level */
   for(lptr = loops; lptr; lptr = lptr->next)
   {
      for(lptr2 = lptr->next; lptr2; lptr2 = lptr2->next)
      {
         /* Order by greatest loopnest number */
         if(lptr2->header->loopnest > lptr->header->loopnest)
         {
            /* Swap the headers */
            tblk = lptr2->header;
            lptr2->header = lptr->header;
            lptr->header = tblk;

            if(!(templist))
               freeblist(templist);

           /* Swap the blists */
           templist = lptr2->blocks; 
           lptr2->blocks = lptr->blocks;
           lptr->blocks = templist;
         }
      }
   }

   /** 
    * Sort the loops with the same loop level that are out of order because of
    * the loopnest ordering above 
    */
   for(lptr = loops; lptr; lptr = lptr->next)
   {
      for(lptr2 = lptr->next; lptr2; lptr2 = lptr2->next)
      {
         /** 
          * Order by lower position in code only where the to entry have the
          * same loopnest level 
          */
         if(lptr2->header < lptr->header && lptr2->header->loopnest == lptr->header->loopnest)
         {
            /* Swap the headers */
            tblk = lptr2->header;
            lptr2->header = lptr->header;
            lptr->header = tblk;

            if(!(templist))
               freeblist(templist);

           /* Swap the blists */
           templist = lptr2->blocks;
           lptr2->blocks = lptr->blocks;
           lptr->blocks = templist;
         }
      }
   }
}
