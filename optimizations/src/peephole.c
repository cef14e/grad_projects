/**
 * @name : Camill Folsom
 * @proj : Peephole Optimization
 */

#include <stdio.h>
#include "opt.h"
#include "misc.h"
#include "vars.h"
#include "opts.h"

#include <stdlib.h>
#include <string.h>
#include <io.h>
#include "flow.h"

/* max size of a isntruction */
#define MAXINST 8

/* Max size of the deads */
#define MAXLIST 30

/* Max number of lines allowed in rules.txt, counting "=" lines */
#define NUMLINES 400

extern int numpeeprules;
extern int numrulesapplied[MAXRULES];

/**
 * Holds a int that indicates the position of the first line of a rule in the
 * lines structure and it also holds the last line of all the rules.
 */
int rules[MAXRULES];

/* Holds all the parsed rules. */
struct lines
{
   char inst[MAXINST];
   int arg1, arg2, arg3;
   int regs[NUMVARWORDS];
   int deads[MAXLIST];
};

/* Used to hold all the lines from rules.txt */
struct lines line[NUMLINES];

/*
 * readinrules - read in the peephole rules from the rules file
 */
void readinrules()
{
   char buffer[MAXLINE], temp[ARGSIZE];

   /* Used to hold the string so the can be parsed into integers */
   char args[ARGSIZE], deads[ARGSIZE], regs[ARGSIZE];
   char *ptr;

   /* Used as indexes for rule and deads */
   int linenum = 0;
   int deadcount;

   /* Used to determine the number of tabs in the read in string */
   int twotab = FALSE;
   int notabs = FALSE;
   int noregs = FALSE;
   int i, j;

   FILE *fp = fopen("rules.txt", "r");

   if(fp)
   {
      /**
       * Initializes all int elements of line struct to -1 so we can tell if
       * one of the ints was set
       */
      for(i = 0; i < NUMLINES; ++i)
      {
         line[i].arg1 = -1;
         line[i].arg2 = -1;
         line[i].arg3 = -1;
 
         for(j = 0; j < NUMVARWORDS; ++j)
             line[i].regs[j] = -1;
 
         for(j = 0; j < MAXLIST; ++j)
             line[i].deads[j] = -1;
      }

      /** 
       * Handles the first line since the first line will alway be position 0,
       * numpeeprules is an index for rules and counts the number of peep rules
       * it is set to 1 so that the when we set the next rule start position
       * it will be in position 1.
       */
      numpeeprules = 1;
      rules[0] = 0;

      while(fgets(buffer, MAXLINE, fp))
      {
         /* Resets the variables used to track if we have regs or deads */
         notabs = FALSE;
         twotab = FALSE;
         noregs = FALSE;

         /* Makes sure we ignore line ends */
         if(buffer[0] != '\n')
         {
            /* Check to see if the rule uses regs list but not the deads */
            if(strstr(buffer, "\t\t"))
               twotab = TRUE;

            /* Gets ride of the newline charater from the end of each line */
            ptr = strstr(buffer, "\n");
            if(ptr)
               *ptr = ' ';

            /*Begin parsing the instruction on each tab encountered, get inst*/
            ptr = strtok(buffer, "\t");
            if(ptr)
               sprintf(line[linenum].inst, "%s", ptr);

            /* Get the args */
            ptr = strtok(NULL, "\t");
            if(ptr)
              sprintf(args, "%s", ptr);

            /* Makes sure we do not have only regs before we get the deads */
            if(!twotab)
            {
               /* Get the deads */
               ptr = strtok(NULL, "\t");
               if(ptr)
                  sprintf(deads, "%s", ptr);

                /* Make sure we dont parse deads and regs if we have neither */
                else
                   notabs = TRUE;

               /* Get the regs when we also have deads */
               ptr = strtok(NULL, "\t");
               if(ptr)
                  sprintf(regs, "%s", ptr);
               else
                  /* Make sure we dont parse the regs */
                  noregs = TRUE;
            }
            else
            {
               /* Get the regs when we do not have deads */
               ptr = strtok(NULL, "\t");
               if(ptr)
                  sprintf(regs, "%s", ptr);
            }

            /***** Parse args *****/

            /* Start parsing the args on commas */
            ptr = strtok(args, ",");
            if(ptr)
               sprintf(temp, "%s", ptr);

            /* Gets rid of the $ so we can convert to int */
            if(temp[0] == '$')
               temp[0] = ' ';

            /* Sets arg1 */
            line[linenum].arg1 = atoi(temp);

            /* Get the second arg */
            ptr = strtok(NULL, ",");
            if(ptr)
            {
              sprintf(temp, "%s", ptr);

               /* Gets rid of the $ so we can convert to int */
               if(temp[0] == '$')
                  temp[0] = ' ';

               /* Sets arg2 */
               line[linenum].arg2 = atoi(temp);
            }

            /* Get the third arg */
            ptr = strtok(NULL, ",");
            if(ptr)
            {
               sprintf(temp, "%s", ptr);

               /* Gets rid of the $ so we can convert to int */
               if(temp[0] == '$')
                  temp[0] = ' ';

               /* Sets arg3 */
               line[linenum].arg3 = atoi(temp);
            }

            /***** Parse deads *****/

            /* Makes sure the are deads to parse */
            if(strlen(deads) > 0 && !notabs && !twotab) 
            {
               /* Gets rid of the $ so we can convert to int */
               deads[0] = ' ';

               /* Grabs the int if we only have one dead */
               if(strlen(deads) < 3)
                  line[linenum].deads[0] = atoi(deads);

               /* Gets the ints when there are multiple deads */
               else
               {
                  /* Start parsing the deads on the $ char */
                  ptr = strtok(deads, "$");
 
                  /* Makes sure the deads index is 0 when we start */
                  deadcount = 0;

                  /* Loop through the dead list parsing on $ */
                  while(ptr)
                  {
                     sprintf(temp, "%s", ptr);

                     /* Get each dead value */
                     line[linenum].deads[deadcount] = atoi(temp);
 
                     /* Get the next dead */
                     ptr = strtok(NULL, "$");

                     ++deadcount;
                  }
               }
            }

            /* Parse regs */
            if(strlen(regs) > 0 && !notabs && !noregs)
            {
               /* Gets rid of the $ so we can convert to int */
               regs[0] = ' ';

               /* Grabs the int if we only have one dead */
               if(strlen(regs) < 3)
                  line[linenum].regs[0] = atoi(regs);

               /* Gets the ints when there are multiple regs */
               else
               {
                  ptr = strtok(regs, "$");
                  deadcount = 0;

                  /* Loop through the dead list parsing on $ */
                  while(ptr)
                  {
                     sprintf(temp, "%s", ptr);
                     line[linenum].regs[deadcount] = atoi(temp);
                     ptr = strtok(NULL, "$");
                     ++deadcount;
                  }
               }
            }  

            /* Keeps track of the number of lines */
            ++linenum;
         }
         else
         {
            /*Sets each element of rules to the index for the start of a rule*/
            rules[numpeeprules] = linenum;
            ++numpeeprules;
         }
      }

      /* Puts the last lines index into rules */
      rules[numpeeprules] = linenum;
   }
   else
   {
      fprintf(stderr, "Failed to open file rules.txt.\n");
      quit(1);
   }
}

/*
 * applypeeprules - apply peephole rules to the function
 */
void applypeeprules(int *changes)
{
   int MAXMATCH = 200;
   struct bblk *cblk;
   extern struct bblk *top;
   struct assemline *cline, *tline, *todel[MAXMATCH];

   /* Assumes there can only ne $0-$9 in a match or replce line */
   int numvars = 10;
   char *ptr;

   /* Holds the variable from the assemline items */
   char *variable[numvars];

   char buffer[MAXLINE];
   int i, j, k;
   int count = 0;
   int broke = FALSE;

   /* Initializes all elements of the todel array */
   for(i = 0; i < MAXMATCH; i++)
       todel[i] = (struct assemline *) NULL;

   /* Loops through each block */
   for(cblk = top; cblk; cblk = cblk->down)
   {
      for(cline = cblk->lines; cline; cline = cline->next)
      {
         if(cline->type != COMMENT_LINE && cline->type != DEFINE_LINE)
         {
            /* loop through each rule */
            for(i = 0; i < numpeeprules; ++i)
            {
               /* Resest broke, tline, and count for next rule */
               broke = FALSE;
               tline = cline;
               count = 0; 

               /* Resets variable for the next rule */
               for(j = 0; j < numvars; ++j)
                  variable[j] = NULL;

               /** 
                * Loop through the lines in each rule, this loop looks for rule
                * violations and breaks if one is found. If none are found the
                * rule is applied
                */
               for(j = rules[i]; j < rules[i+1]; ++j)
               {
                  /* Make sure this is not the first iteration of the loop */
                  if(count > 0 && tline->next) 
                  {
                     tline = tline->next;

                     /* Make sure we skip define and comment lines */
                     if(tline->type == DEFINE_LINE || 
                        tline->type == COMMENT_LINE)
                     {
                        /* Skip a define or comment line */
                        if(tline->next)
                           tline = tline->next;

                        /* Make sure the next line exists */
                        else
                           break; 
                     }
                   }
           
                  /** 
                   * Make sure the next line exist and we do not break 
                   * for a lineend 
                   */
                  else if(!tline->next && tline != cblk->lineend)
                     break;

                  /* Check if the current rules line inst is equal to tlines */
                  if(strcmp(tline->items[0], line[j].inst) == 0) 
                  {
                     /**
                      * Check to see if arg1 was set in readinrules,
                      * also make sure items[1] exits
                      */
                     if(line[j].arg1 >= 0 && tline->items[1])
                     {
                        /* If variable is not set set it with items[1] */
                        if(!variable[line[j].arg1])
                           variable[line[j].arg1] = tline->items[1];

                        /** 
                         * If we have a load but the first arg is already in
                         * variable then we have to add brackets to the
                         * variable so we can check it agian tlines items[1]
                         */ 
                        else if((strcmp(line[j].inst, "ld") == 0 || 
                                 strcmp(line[j].inst, "ldsb") == 0) && 
                                 variable[line[j].arg1][0] != '[')
                        {
                           sprintf(buffer, "[%s]", variable[line[j].arg1]);

                           /** 
                            * Break if the string in variable doesnt match
                            * tline items[1] that coresponds to the rules
                            * items[1] 
                            */
                           if(strcmp(tline->items[1], buffer) != 0)
                              break;
                        }

                        /** 
                         * Break if tlines item[1] doesnt match the value 
                         * loaded into variable for a previous line
                         */
                        else if(strcmp(tline->items[1], 
                                variable[line[j].arg1]) != 0)
                        {
                           break;
                        }
                     }

                     /* Check if arg2 was set in readinrules */
                     if(line[j].arg2 >= 0 && tline->items[2])
                     {
                         /** 
                          * Makes sure if tlines items[1] and items[2] are the
                          * same but this rules line item[1] and item[2] are 
                          * not equal we break
                          */
                         if(line[j].arg2 != line[j].arg1 &&
                            strcmp(tline->items[1], tline->items[2]) == 0 )
                         {
                            break;
                         }

                        /** 
                         * If the variable for arg2 has not been set then set
                         * it with tlines items[2] 
                         */
                        else if(!variable[line[j].arg2])
                           variable[line[j].arg2] = tline->items[2];

                        /**
                         * If we have a store but the first arg is already in
                         * variable then we have to add brackets to the
                         * variable so we can check it agian tlines items[2]
                         */
                        else if((strcmp(line[j].inst, "st") == 0 ||
                                 strcmp(line[j].inst, "stb") == 0) &&
                                 variable[line[j].arg2][0] != '[')
                        {
                           sprintf(buffer, "[%s]", variable[line[j].arg2]);

                           /**
                            * Break if the string in variable doesnt match
                            * tline items[1] that coresponds to the rules
                            * items[1]
                            */
                           if(strcmp(tline->items[2], buffer) != 0)
                              break;
                        }


                         /** 
                          * Make sure items[2] matches the string loaded int
                          * variable for previous line
                          */
                         else if(strcmp(tline->items[2], 
                                        variable[line[j].arg2]) != 0)
                         {
                            break;
                         }
                     }

                     /* Check if arg3 was set */
                     if(line[j].arg3 >= 0 && tline->items[3])
                     {
                         /** 
                          * Make sure theres no equal arqgs where there 
                          * shoulnt be 
                          */
                         if((line[j].arg3 != line[j].arg1 && 
                            strcmp(tline->items[1], tline->items[3]) == 0) ||
                            (line[j].arg3 != line[j].arg2 &&
                            strcmp(tline->items[2], tline->items[3]) == 0))
                         {
                            break;
                         }

                        /* Sets variable for arg3 if its empty */
                        if(!variable[line[j].arg3])
                           variable[line[j].arg3] = tline->items[3];

                         /* Make sure items three matches value in variable */
                         else if(strcmp(tline->items[3], 
                                        variable[line[j].arg3]) != 0)
                         {
                            break;
                         } 
                     }

                     /* Check if any dead were set */
                     for(k = 0; k < NUMVARWORDS; ++k) 
                     {
                        /* Check if the reg was set */
                        if(line[j].regs[k] >= 0)
                        {
                           /* Check if arg1 was set */
                           if(line[j].arg1 == line[j].regs[k])
                           {
                              /* Break if arg1 is not a register */
                              if(!isreg(tline->items[1]))
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }

                           /* Check if arg2 was set */
                           if(line[j].arg2 == line[j].regs[k])
                           {
                              if(!isreg(tline->items[2]))
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }

                           /* Check if arg3 was set */
                           if(line[j].arg3 == line[j].regs[k])
                           {
                              if(!isreg(tline->items[3]))
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }
                        }
                     }

                     /* Break if we broke out of the loop above */
                     if(broke)
                        break;

                     /* Check if any deads were set */
                     for(k = 0; k < MAXFIELD; ++k)
                     {
                         /* Check if the dead was set */
                        if(line[j].deads[k] >= 0)
                        {
                           /** 
                            * If the deads are empty break since deads[k] was
                            * set 
                            */
                           if(varempty(tline->deads))
                           {
                              broke = TRUE;
                              break;
                           }
    
                           /* Check if arg1 was set */
                           if(line[j].arg1 == line[j].deads[k])
                           {
                               /** 
                                * If tline items[1] is not in the dead list
                                * break since arg1 should be dead 
                                */
                              if(!regexists(tline->items[1], tline->deads)) 
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }

                           /* Check if arg2 was set */
                           if(line[j].arg2 == line[j].deads[k])
                           {
                              if(!regexists(tline->items[2], tline->deads))
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }

                           /* Check if arg3 was set */
                           if(line[j].arg3 == line[j].deads[k])
                           {
                              if(!regexists(tline->items[3], tline->deads))
                              {
                                 broke = TRUE;
                                 break;
                              }
                           }
                        }
                     }

                     /* If we broke out of deads loop go to next rule */
                     if(broke)
                        break;

                     /* Keep track of the lines that need to be deleted */
                     todel[count] = tline;
                     ++count;
                  }
         
                  /* If we hit a equal line we can start deleting insts */
                  else if(strcmp(line[j].inst, "= ") == 0 && todel[0])
                  {
                     /* Make sure count is 0 since we use it in the if part */
                     count = 0;

                     /* Delete each line umtil none left to delete */
                     while(todel[count])
                     {
                        /* Replace the first inst with the replace line */
                        if(count == 0)
                        {
             
                           /* Create new inst if we use three variables */
                           if(line[j+1].arg1 >= 0 && line[j+1].arg2 >= 0 && 
                              line[j+1].arg3 >= 0)
                           {
                              /** 
                               * Make sure we dont add bracketed strings to
                               * three arg instructions 
                               */
                              if(variable[line[j+1].arg1][0] == '[') 
                              {
                                  /** 
                                   * Make sure the inst isnt adding to a
                                   * reg address 
                                   */
                                  if(strlen(variable[line[j+1].arg1]) > 6)
                                     break;

                                 /* Find and replace ']' with '\0' for arg1 */
                                 ptr = strstr(variable[line[j+1].arg1], "]");
                                 if(ptr)
                                    *ptr = '\0';
                    
                                 /* Used to loop through chars in variable */
                                 k = 0;

                                 /* Swap '[' with each char including '\0' */
                                 while(variable[line[j+1].arg1][k] != '\0')
                                 {
                                    variable[line[j+1].arg1][k] =
                                    variable[line[j+1].arg1][k+1];

                                    ++k;
                                 }
                              }

                              /* Deals with braketed arg2 strings */
                              if(variable[line[j+1].arg2][0] == '[')
                              {
                                 /* Find and replace ']' with '\0' for arg2 */
                                 ptr = strstr(variable[line[j+1].arg2], "]");
                                 if(ptr)
                                    *ptr = '\0';

                                 k = 0;

                                 /* Swap '[' with each char including '\0' */
                                 while(variable[line[j+1].arg2][k] != '\0')
                                 {
                                    variable[line[j+1].arg2][k] =
                                    variable[line[j+1].arg2][k+1];

                                    ++k;
                                 }
                              }

                              /* Deals with braketed arg3 strings */
                              if(variable[line[j+1].arg3][0] == '[')
                              {
                                 /* Find and replace ']' with '\0' for arg3 */
                                 ptr = strstr(variable[line[j+1].arg3], "]");
                                 if(ptr)
                                    *ptr = '\0';

                                 k = 0;

                                 /* Swap '[' with each char including '\0' */
                                 while(variable[line[j+1].arg3][k] != '\0')
                                 {
                                    variable[line[j+1].arg3][k] = 
                                    variable[line[j+1].arg3][k+1];

                                    ++k;
                                 }
                              }

                              /* Make the new instruction */
                              sprintf(buffer, "\t%s\t%s,%s,%s", line[j+1].inst, 
                                                     variable[line[j+1].arg1], 
                                                     variable[line[j+1].arg2], 
                                                     variable[line[j+1].arg3]);
    
                           }

                           /* Make replace instruction for two arg insts */
                           else if(line[j+1].arg1 >= 0 && line[j+1].arg2 >= 0)
                           {
                              /*Deal with printing nonbracked string to a ld */
                              if((strcmp(line[j+1].inst, "ld") == 0 || 
                                  strcmp(line[j+1].inst, "ldsb") == 0) &&
                                  variable[line[j+1].arg1][0] != '[')
                              {
                                 /* Make new load instruction */
                                 sprintf(buffer, "\t%s\t[%s],%s", 
                                                     line[j+1].inst,
                                                     variable[line[j+1].arg1],
                                                     variable[line[j+1].arg2]);
                              }

                              /*Deal with printing nonbracked string to a ld */
                              if((strcmp(line[j+1].inst, "st") == 0 ||
                                  strcmp(line[j+1].inst, "stb") == 0) &&
                                  variable[line[j+1].arg1][0] != '[')
                              {
                                 /* Make new load instruction */
                                 sprintf(buffer, "\t%s\t%s,[%s]", 
                                                     line[j+1].inst,
                                                     variable[line[j+1].arg1],
                                                     variable[line[j+1].arg2]);
                              }

                              else
                              {
                                 /* Make other new two arg instructions */
                                 sprintf(buffer, "\t%s\t%s,%s", line[j+1].inst,
                                                     variable[line[j+1].arg1], 
                                                     variable[line[j+1].arg2]);
                              }
                           }
                           else if(line[j+1].arg1 >= 0)
                           {
                              /* Make new instructions for 1 arg */
                              sprintf(buffer, "\t%s\t%s", line[j+1].inst,
                                                variable[line[j+1].arg1]);
                           }
                           
                           /* Makes no arg new instructions */
                           else
                              sprintf(buffer, "\t%s", line[j+1].inst);

                           /* Change the assemline to the new replace line */
                           free(todel[count]->text);
                           todel[count]->text = allocstring(buffer);
                           setupinstinfo(todel[count]);
                           *changes = TRUE;
                        }

                        /* For all but the first iteration delete the lines */
                        else
                           delline(todel[count]);

                        ++count;
                     }

                     /* Increment counts if changes were applied */
                     if(*changes)
                     {
                        incropt(PEEPHOLE_OPT);
                    
                        ++numrulesapplied[i];

                        /* Make sure we restart looking through the rules */
                        i = 0;
 
                        *changes = FALSE;
                     }

                     /* Make sure we dont go to the replace instruction */
                     break;
                  }
 
                  /** 
                   * If the lines inst does not match any tline items[0] and
                   * it is not = 
                   */
                  else
                     break;
               }
            }
         }
      }
   }
   check_cf();
}
