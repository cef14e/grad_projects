Memory Hierarchy Simulator
==========================
This program simulates a memory hierarchy. It reads in a '.dat' file where each
line contains the character 'R' or 'W' to indicate whether the memory is being
read or written. Each line contains a colon ':' separating the character from
the hexadecimal address that is being written or read. Supports 32-bit addresses
up to _7fffffff_. Converts addresses greater than _7fffffff_ into _7fffffff_.

* R:1b23
* W:3bc

There must be a _trace.config_ that will specify the configuration of the memory
hierarchy. This file allows for the following parts of the hierarchy to be 
configured: translation lookaside buffer (TLB,) page table, data cache, and victims
cache. The file also specifies if the simulator should us the TLB, virtual addresses,
and the victims cache.

The simulator outputs the configuration statistics, a table showing the simulation results,
and the simulation statistics. The tables shows the following: 

* each virtual or physical address read in
* the virtual page number
* the page's offset
* the TLB's tag, index, and result (hit/miss)
* the page table's result
* the physical page number
* the data cache's tag, index, and result
* the victims cache's tag and result

The simulation statics section contains the total number of hits and misses for the TLB, page 
table, data cache, victims cache, and L1. The statistics also include the hit ratio 
for each of these. The total number of reads, writes and their ratio is also included.
The final piece of information output is the total number of main memory, page table,
and disk references.

Compilation
-----------
This program can be created using the supplied makefile. The makefile creates an
executable named _mem.exe_ using the command:
```
g++ -Wall -std=c++1y mem_sim.cpp -o mem.exe
```

Usage
-----
To use this program a _trace.config_ file must be present in the same directory 
as the simulator. The program can be run using the command:
```
./mem.exe < ../tests/memhier1.dat
```

trace.config
------------
This file allows for the configuration of the memory hierarchy. The TLB's number
of sets and the size of each set can be specified:
```text
Data TLB configuration
Number of sets: 2
Set size: 8
```

The page table can have its number of both virtual and physical pages specified.
The size of each page is also specified:
```text
Page Table configuration
Number of virtual pages: 64
Number of physical pages: 4
Page size: 256
```

The data cache is configured by specifying the number of sets, the size of each set,
and the size of each line:
```text
Data Cache configuration
Number of sets: 4
Set size: 8
Line size: 16
```

The victim cache configurations specifies the size of each set allowed in the cache:
```text
Victim Cache configuration
Set size: 8
```

The final part of the _trace.config_ file specifies if the simulator should use 
the TLB, victims cache, or virtual addresses. If a 'y' is given the corresponding
element of the simulator is used. If a 'n' is supplied then that element is not
used:
```text
Virtual addresses: y
TLB: y
Victim cache: y
```

Example Output
--------------
This output was created using the file _memhier5.dat_. The first part of the output
is the configuration read in from the trace file. The second part contains a table
with the simulation results for each address read in. The legend for the table is
below:

* **Virtual/Physical Address** : the virtual or physical address read in
* **Virt. Page #** : the virtual page number
* **Page Off** : the page's offset
* **TLB Tag, Ind, Res.** : the TLB's tag, index and result (hit/miss)
* **PT Res.** : the page table's result
* **Phys Pg #** : the physical page number 
* **DC Tag, Ind, Res.** : the data cache's tag, index, and result
* **VC Tag, Res.** : the victims cache's tag and result

The last part of the output is the simulations statistics.

```text
Data TLB contains 2 sets.
Each set contains 8 entries.
Number of bits used for the index is 1.

Number of virtual pages is 64.
Number of physical pages is 4.
Each page contains 256 bytes.
Number of bits used for the page table index is 6.
Number of bits used for the page offset is 8.

D-cache contains 4 sets.
Each set contains 8 entries.
Each line is 16 bytes.
Number of bits used for the index is 2.
Number of bits used for the offset is 4.

Each V-cache set contains 8 entries.

The addresses read in are virtual addresses.


Virtual  Virt.  Page TLB    TLB TLB  PT   Phys        DC  DC          VC
Address  Page # Off  Tag    Ind Res. Res. Pg # DC Tag Ind Res. VC Tag Res.
-------- ------ ---- ------ --- ---- ---- ---- ------ --- ---- ------ ----
75f6d123     11   23      8   1 miss miss    0      0   2 miss      2 miss
00003123     31   23     18   1 miss miss    1      4   2 miss     12 miss
00000123      1   23      0   1 miss miss    2      8   2 miss     22 miss
00000111      1   11      0   1 hit          2      8   1 miss     21 miss
00000999      9   99      4   1 miss miss    3      e   1 miss     39 miss
00000764      7   64      3   1 miss miss    0      1   2 miss      6 miss
000001cc      1   cc      0   1 hit          2      b   0 miss     2c miss
000001cc      1   cc      0   1 hit          2      b   0 hit      2c miss
00000444      4   44      2   0 miss miss    1      5   0 miss     14 miss
00002321     23   21     11   1 miss miss    3      c   2 miss     32 miss
000001bb      1   bb      0   1 hit          2      a   3 miss     2b miss
00000bbb      b   bb      5   1 miss miss    0      2   3 miss      b miss
00000abc      a   bc      5   0 miss miss    1      6   3 miss     1b miss
000003bc      3   bc      1   1 miss miss    3      e   3 miss     3b miss
0000057f      5   7f      2   1 miss miss    2      9   3 miss     27 miss
00000fff      f   ff      7   1 miss miss    0      3   3 miss      f miss
00000917      9   17      4   1 miss miss    1      4   1 miss     11 miss
00001b23     1b   23      d   1 miss miss    3      c   2 miss     32 miss
00000111      1   11      0   1 miss miss    2      8   1 miss     21 miss
00000111      1   11      0   1 hit          2      8   1 hit      21 miss
000009b1      9   b1      4   1 hit          1      6   3 miss     1b miss
0000019b      1   9b      0   1 hit          2      a   1 miss     29 miss


Simulation statistics

dtlb hits        : 7
dtlb misses      : 15
dtlb hit ratio   : 0.318182

pt hits          : 0
pt faults        : 15
pt hit ratio     : 0.000000

dc hits          : 2
dc misses        : 20
dc hit ratio     : 0.090909

vc hits          : 0
vc misses        : 22
vc hit ratio     : 0.000000

L1 hits          : 2
L1 misses        : 20
L1 hit ratio     : 0.090909

Total reads      : 11
Total writes     : 11
Ratio of reads   : 0.500000

main memory refs : 28
page table refs  : 15
disk refs        : 22
```