#include<iostream>
#include<stdio.h>
#include<string>
#include<sstream>
#include<fstream>
#include<math.h>
#include<iomanip>
#include<vector>

/**
 * @name    : Camill Folsom
 * @project : Memory Hierarchy Simulator
 * @compile : g++ -Wall -std=c++1y mem_sim.cpp -o mem.exe
 */

/**
 * @name : TLB (Translation Lookaside Buffer)
 */
struct TLB
{
	int numSets, tag, index, hits, misses;
	int numEntries, columns;
	bool isUsed, isHit;
	std::vector<std::vector<int>> theTLB;

	/* Returns number of bits needed for the index */
	int indexBits(){return log2(numSets);} 
};

/**
 * @name : DC (Data Cache)
 */
struct DC
{
	int numSets, numEntries, lineSize, index, tag, virtMemrefs;
	int memrefs, tagBits, columns, hits, misses;
	std::vector<std::vector<int>> theDC;

	/* These functions return the number of bits needed for the index and offset. */
	int indexBits(){return log2(numSets);}
	int offsetBits(){return log2(lineSize);}
};

/**
 * @name : PT (Page Table)
 */
struct PT
{
	/* number of virtual and physical pages */
	int numVirtual, numPhys, pageSize, pfaults, hits;
	int diskrefs, columns, LRUnum, physnumber;
	bool isVirtual, isHit, allpUsed;
	std::vector<std::vector<int>> thePT;

	/* Used to return the number of bits for the index, offset, and physical number */
	int indexBits(){return log2(numVirtual);}
	int offsetBits(){return log2(pageSize);}
	int numphysBits(){return log2(numPhys);}
};

/**
 * @name : VC (Victim's Cache)
 */
struct VC
{
	int numEntries, hits, misses, columns, tag;
	int memrefs, LRUnum;
	bool isUsed, isHit;

	std::vector<int> theVC;
};

/**
 * @name  : isUsed
 * @brief : This funtion is used by the config function and it determines whether
 * 		    or not the virtual address, TLB, and VC are used.
 */
bool isUsed (char c)
{
	if(c == 'y' || c == 'Y')
		return true;

	if(c =='n' || c =='N')
		return false;

	return false;
}

/**
 * @name  : config
 * @brief : Reads in the trace.config file, parses the data, and sets the data
 * 			needed to initialize the structures.
 */
void config(TLB &mytlb, PT &mypt, DC &mydc, VC &myvc)
{
	char check;
	std::string line;
	int count = 0;
	std::ifstream file("trace.config");

	if(file.is_open())
	{
		/* Sets data from trace.config file */
		while(getline(file,line))
		{
		/***** Sets TLB Values *********/
			/* Sets TLB Number of sets */
			if(count == 1)
				/* Makes line a string that contains everything after the colon and convert it to an int. */ 
				mytlb.numSets = std::stoi(line.substr(line.find(":") + 1));		
		
			/* Sets TLB number of entries */
			if(count == 2)
				mytlb.numEntries = std::stoi(line.substr(line.find(":") + 1));	
	
		/***** Sets PT Values ******************/
			/* Sets PT number of virtual pages */
			if(count == 5)
				mypt.numVirtual = std::stoi(line.substr(line.find(":") + 1));	

			/* Sets PT number of physcial pages */
			if(count == 6)
				mypt.numPhys = std::stoi(line.substr(line.find(":") + 1));	
			/* Sets PT page size */
			if(count == 7)
				mypt.pageSize = std::stoi(line.substr(line.find(":") + 1));	

		/***** Sets DC Values ************/
			/* Sets DC number of sets */
			if(count == 10)
				mydc.numSets = std::stoi(line.substr(line.find(":") + 1));	
		
			/* Sets DC number of entries */
			if(count == 11)
				mydc.numEntries = std::stoi(line.substr(line.find(":") + 1));	

			/* Sets DC line size */
			if(count == 12)
				mydc.lineSize = std::stoi(line.substr(line.find(":") + 1));	
			
			/* Sets VC number of entries */
			if(count == 15)
				myvc.numEntries = std::stoi(line.substr(line.find(":") + 1));	

			/* Check if virtual addresses are used */
			if(count == 17)
			{
				check = line[line.size()-1];	
				mypt.isVirtual = isUsed(check);
			}

			/* Check if the TLB is used */
			if(count == 18)
			{
				check = line[line.size()-1];	
				mytlb.isUsed = isUsed(check);
			}

			/* Check if the VC is used */
			if(count == 19)
			{
				check = line[line.size()-1];	
				myvc.isUsed = isUsed(check);
			}
			count++;
		}
		file.close();
	}
	else
	{
		std::cout << "Failed to open file trace.config\n";
		exit(1);
    }
}

/**
 * @name        : initDC (initalize data cache)
 * @brief       : Initializes the data cache data.
 *
 * @Description : Allocates space for theDC vector. If associativity is greater
 * 				  than 1 then we allocate more space for theDC. So that each entry
 * 				  can contain the valid, dirty, LRU, tag data, and physical page
 * 				  number. We also include one entry at the end of the row to 
 * 				  keep track of the LRU number for that row. The valid and dirty 
 * 				  entries are zero if not valid  or dirty and 1 if valid or dirty. 
 * 				  If direct-map then it works the same way except we do not allocate 
 * 				  space for LRU data. An example entry is given below.
 *
 * 				  Entry      : [valid][dirty][LRU][tag][physical number]
 * 				  Direct-Map : [valid][dirty][tag][physical number]
 *
 * 				  The LRU entries are used as follows. The LRU number starts at 0
 * 				  then when a tag is place in the cache its LRU entry equals the LRU
 * 				  number at the end of the row. Then the LRU number at the end of the 
 * 				  row is increased by 1. Then the LRU entry is the one who's LRU number
 * 				  is the smallest. In affect this is tracking when the page was added
 * 				  since the first page inserted will have an LRU of 0 the 2nd will have
 * 				  an LRU of 1 if the first page is used next then its LRU would become 2, etc.
 */
void initDC(DC &mydc, int physAddrBits)
{
	/* Calculate the size of the columns based of the associativity */
	if(mydc.numEntries == 1)
		mydc.columns = (4 * mydc.numEntries);
	else
		mydc.columns = (5 * mydc.numEntries) + 1;

	/* Allocate the rows for the DC */	
	mydc.theDC.resize(mydc.numSets);
	/* Allocate the columns for the DC */
	for(int i = 0; i < mydc.numSets; ++i)
		mydc.theDC[i].resize(mydc.columns);

	/* Sets all values of theDC to zero */
	for(int i = 0; i < mydc.numSets; ++i)
		for(int j = 0; j < mydc.columns; ++j)
			mydc.theDC[i][j] = 0;
		

	/* Sets values to zero and gets the number of tagBits */
	mydc.memrefs = 0;
	mydc.tagBits = (physAddrBits - (mydc.indexBits() + mydc.offsetBits()));
	mydc.misses = 0;
	mydc.hits = 0;
	mydc.virtMemrefs = 0;
}

/**
 * @name        : initVC (Initialize Victim's Cache)
 * @brief       : Used to initialize the VC. 
 * @description : Its entries are laid out the same way as
 * 				  for DC. However since we only have one set the LRU number is tracked 
 * 				  by a single variable so one is not needed at the end of the row.
 * 				  An entrie example is given below.
 * 			
 * 				  Entry      : [valid][dirty][LRU][tag][physical number]
 * 				  Direct Map : [valid][dirty][tag][physical number]
 */
void initVC(VC &myvc)
{
	/* Calculate the size of the columns based of the associativity */	
	if(myvc.numEntries == 1)
		myvc.columns = (4 * myvc.numEntries);
	else
		myvc.columns = (5 * myvc.numEntries);

	/* Allocates theVC */
	myvc.theVC.resize(myvc.columns);

	/* Zeros out the VC */
	for(int i = 0; i < myvc.columns; ++i)
		myvc.theVC[i] = 0;

	myvc.hits = 0;
	myvc.misses = 0;
	myvc.memrefs = 0;
	myvc.isHit = false;
	myvc.LRUnum = 0;
}

/**
 * @name        : initPT (Initalize Page Table)
 * @brief       : Initalizes the PT. 
 * @description : Allocates like the DC exept since it only has one entry in the
 * 				  set we can keep one LRU number so one is not need at the end of
 * 				  the row and we do not need to keep track of a tag. Example below.
 *
 * 				  Entry : [valid][dirty][LRU][physical number]
 */
void initPT(PT &mypt)
{
	/* Sets the number of columns */
	mypt.columns = 4;
	
	/* Allocates the PT */
	mypt.thePT.resize(mypt.numVirtual);
	for(int i = 0; i < mypt.numVirtual; ++i)
		mypt.thePT[i].resize(mypt.columns);

	/* Sets all values of thePT to zero */
	for(int i = 0; i < mypt.numVirtual; ++i)
		for(int j = 0; j < mypt.columns; ++j)
			mypt.thePT[i][j] = 0;

	mypt.LRUnum = 0;
	mypt.hits = 0;
	mypt.pfaults = 0;
	mypt.diskrefs = 0;
	mypt.physnumber = 0;
	mypt.isHit = false;
	mypt.allpUsed = false;
}

/**
 * @name  : initTLB (Initalize TLB)
 * @brief : Initalizes the TLB. The TLB is allocated the exact same way the DC is.
 */
void initTLB(TLB &mytlb)
{
	/* Calculate the size of the columns based of the associativity */
	if(mytlb.numEntries == 1)
		mytlb.columns = (4 * mytlb.numEntries);
	else
		mytlb.columns = (5 * mytlb.numEntries) + 1;

	/* Allocates the TLB */
	mytlb.theTLB.resize(mytlb.numSets);
	for(int i = 0; i < mytlb.numSets; ++i)
		mytlb.theTLB[i].resize(mytlb.columns);
	
	/* Zeros out the TLB */
	for(int i = 0; i < mytlb.numSets; ++i)
		for(int j = 0; j < mytlb.columns; ++j)
			mytlb.theTLB[i][j] = 0;

	mytlb.isHit = false;
	mytlb.hits = 0;
	mytlb.misses = 0;
}

/**
 * @name: printout (Print Output)
 * @brief: Prints the initial output up to the titles of the columns.
 */
void printout(TLB &mytlb, PT &mypt, DC &mydc, VC &myvc)
{
	std::cout << "Data TLB contains " << mytlb.numSets << " sets.\n";
	std::cout << "Each set contains " << mytlb.numEntries << " entries.\n";
	std::cout << "Number of bits used for the index is " << mytlb.indexBits() << ".\n";

	std::cout << std::endl;

	std::cout << "Number of virtual pages is " << mypt.numVirtual << ".\n";
	std::cout << "Number of physical pages is " << mypt.numPhys << ".\n";
	std::cout << "Each page contains " << 	mypt.pageSize << " bytes.\n";
	std::cout << "Number of bits used for the page table index is " << mypt.indexBits() << ".\n";
	std::cout << "Number of bits used for the page offset is " << mypt.offsetBits() << ".\n";

	std::cout << std::endl;

	std::cout << "D-cache contains " << mydc.numSets << " sets.\n";
	std::cout << "Each set contains " << mydc.numEntries << " entries.\n";
	std::cout << "Each line is " << mydc.lineSize << " bytes.\n";
	std::cout << "Number of bits used for the index is " << mydc.indexBits() << ".\n";
	std::cout << "Number of bits used for the offset is " << mydc.offsetBits() << ".\n";

	std::cout << std::endl;

	std::cout << "Each V-cache set contains " << myvc.numEntries << " entries.\n";

	std::cout << std::endl;
	
	if(mypt.isVirtual)
		std::cout << "The addresses read in are virtual addresses.\n";
	else
		std::cout << "The addresses read in are physical addresses.\n";

	if(!mytlb.isUsed)
		std::cout << "TLB is disabled in this configuration.\n";

	if(!myvc.isUsed)
		std::cout << "VC is disabled in this configuration.\n";
	
	std::cout << std::endl;
	std::cout << std::endl;

	if(mypt.isVirtual)
		printf("Virtual  Virt.  Page TLB    TLB TLB  PT   Phys        DC  DC          VC\n");
	else
		printf("Physical Virt.  Page TLB    TLB TLB  PT   Phys        DC  DC          VC\n");

	printf("Address  Page # Off  Tag    Ind Res. Res. Pg # DC Tag Ind Res. VC Tag Res.\n");
	printf("-------- ------ ---- ------ --- ---- ---- ---- ------ --- ---- ------ ----\n");
}

/**
 * @name  : isinVC
 * @brief : Checks is myvc.tag is in the VC. It also increments the number of
 * 			hits and misses for the VC. It also set whether or not we had a hit
 * 			by setting myvc.isHit to true or false.
 */
void isinVC(VC &myvc)
{
	int count;
	/* Used to indicate the tag was not found */
	bool broke = false;

	/* Used to loop through all the valid bit entries, sets it based on the associativity */	
	if(myvc.numEntries == 1)
		count = 4;
	else
		count = 5;

	/* Make sures myvc.isHit is false when we start looking */
	myvc.isHit = false;

	/* Loops through the valid bits */
	for(int i = 0; i < myvc.columns; i += count)
	{
		/* Check for a hit */
		if(myvc.theVC[i] != 0 && myvc.tag == myvc.theVC[i+count-2])
		{
			/* Sets the valid bit to invalid */
			myvc.theVC[i] = 0;
			++myvc.hits;
			myvc.isHit = true;
			broke = true;
			break;
		}
	}

	/* Increments misses if the loop did not find the tag */
	if(!broke)
		++myvc.misses;
}

/**
 * @name  : addtoVC ( Add to Victim's Cache)
 * @brief : Add a tag to the VC and physical number. This functions code is similar 
 * 	 	    to the code in the wBack function. It loops through the valid bits if 
 * 	 	    it finds an invalid one it updates that entry. If no invalid entries then
 * 		    it uses LRU to select a victim. When the VC is use myvc.memrefs
 * 		    is used to track the memory refernces instead of the DC's.
 * 		    The values are those of the evicted DC entry.
 */

void addtoVC(VC &myvc, int dirty, int physnum)
{
	int count, index, min;
	/* Used to determine if we need to use LRU to replace a entry */
	bool full = false;
	
	/* Used to increment the for loop counter */
	if(myvc.numEntries == 1)
		count = 4;
	else
		count = 5;

	/* Loops through the valid bits */
	for(int i = 0; i < myvc.columns; i += count)
	{
		/* Check for an invalid bit */
		if(myvc.theVC[i] == 0)
		{
			/* Sets the physical number */
			myvc.theVC[i+count-1] = physnum;
			/* Sets the tag */
			myvc.theVC[i+count-2] = myvc.tag;
			/* Sets the valid bit */
			myvc.theVC[i] = 1;
			full = false;

			/* Increment memory references on a miss */
			if(!myvc.isHit)
				++myvc.memrefs;

			/* Sets the dirty bit */
			myvc.theVC[i+1] = dirty;

			if(myvc.numEntries > 1)
			{
				/* Sets the LRU number for this entry, then increase the LRU number by one. */
				myvc.theVC[i+2] = myvc.LRUnum;
				++myvc.LRUnum;
			}

			break;			
		}
		else
			full = true;
	}

	/* Perform LRU replacement */
	if(full)
	{
		if(myvc.numEntries == 1)
		{
			/* Sets the physcial number and tag */
			myvc.theVC[3] = physnum;
			myvc.theVC[2] = myvc.tag;
			
			/* Increments memory references based on the dirty bit */
			if(myvc.theVC[1] == 0)
				++myvc.memrefs;
			else
				myvc.memrefs += 2;

			/* Updates the dirty bit */
			myvc.theVC[1] = dirty;
		}

		else
		{
			/* Loop throught the LRU numbers */
			for(int i = 2; i < myvc.columns; i += count)
			{
				/* Sets min and index to the first entry so min has a value */
				if(i == 2)
				{
					min = myvc.theVC[i];
					index = i + 1;
				}
				/* Compares the LRU entries and find the smallest */
				else if(min > myvc.theVC[i])
				{
					min = myvc.theVC[i];
					index = i + 1;
				}
			}
			/* Updates the physical number and tag based of the index found */
			myvc.theVC[index+1] = physnum;
			myvc.theVC[index] = myvc.tag;

			/* Increments memory references based on the dirty bit */
			if(myvc.theVC[index-2] == 0)
				++myvc.memrefs;
			else
				myvc.memrefs += 2;

			/* Updates the dirty bit */
			myvc.theVC[index-2] = dirty;

			if(myvc.numEntries > 1)
			{
				/* Sets the LRU number for this entry, then increase the LRU number by one. */
				myvc.theVC[index-1] = myvc.LRUnum;
				++myvc.LRUnum;
			}
		}
	}
}

/**
 * @name        : wBack (Write Back)
 * @brief       : Adds a tag and physical number to the DC.
 * @description : It first loops through the valid bits and adds the tag and 
 * 				  physical number to the DC if it finds an invalid tag. If it
 * 				  finds a valid tag it check to see it we have a hit. If there
 * 				  no invalid bits and no matching tags then this function
 * 				  performs LRU replacement on the DC. It uses write back write
 * 				  allocate. So a bit is set to dirty on write hits and misses
 * 				  and it is set to clean when we have a read miss.
 */
void wback(DC &mydc, char RW, bool &isHit, VC &myvc, int physnum, bool isVirtual)
{
	int count, LRUphys;
	int min, index, dirty;
	/* Used to loop through all the columns */
	int entries = mydc.columns;
	bool full = false;

	/* Count is used to increment through the loop */
	if(mydc.numEntries == 1)
		count = 4;
	else
	{
		count = 5;
		/* Makes sure the extra entry for the LRU is not looped through */
		entries -= 1;	
	}
	
	/* Loop throuh the valid bits */
	for(int i = 0; i < entries; i += count)
	{
		if(mydc.theDC[mydc.index][i] == 0)
		{
			/* Updates the DC with the physical number */
			mydc.theDC[mydc.index][i+count-1] = physnum;

			/* Updates the DC with the tag */
			mydc.theDC[mydc.index][i+count-2] = mydc.tag;
			/* Update the valid bit */
			mydc.theDC[mydc.index][i] = 1;
			++mydc.misses;
			full = false;
	
			/* Makes sure if we have virtual addresses but no VC then the dirty bit can be ignored */
			if(!myvc.isUsed && isVirtual)
				++mydc.memrefs;
			else
			{
				/* Check if the entry is dirty, reference the memory once if it is not dirty */
				if(mydc.theDC[mydc.index][i+1] == 0)
					++mydc.memrefs;
				else
					mydc.memrefs += 2;
			}

			/* Set dirty bit if write miss, resets it for a read miss */
			if(RW == 'W')
				mydc.theDC[mydc.index][i+1] = 1;
			else
				mydc.theDC[mydc.index][i+1] = 0;

			/* Makes sure we increment the VC memory reference tracker when it is used */
			if(myvc.isUsed && !myvc.isHit)
				++myvc.memrefs;

			if(mydc.numEntries > 1)
			{
				/* Sets the LRU number for this entry, then increase the LRU number by one. */
				mydc.theDC[mydc.index][i+2] = mydc.theDC[mydc.index][mydc.columns-1];
				++mydc.theDC[mydc.index][mydc.columns-1];
			}
		
			break;
		}
		/* Check for a hit */
		else if(mydc.theDC[mydc.index][i+count-2] == mydc.tag)
		{
			++mydc.hits;
			isHit = true;
			full = false;
			
			/* Sets dirt bit for write hit */
			if(RW == 'W')
				mydc.theDC[mydc.index][i+1] = 1;

			if(mydc.numEntries > 1)
			{
				/* Sets the LRU number for this entry, then increase the LRU number by one. */
				mydc.theDC[mydc.index][i+2] = mydc.theDC[mydc.index][mydc.columns-1];
				++mydc.theDC[mydc.index][mydc.columns-1];
			}			

			break;
		}
		else
			full = true;
	}

	/* Perform the LRU operation */
	if(full)
	{
		if(mydc.numEntries == 1)
		{
			/* Adds the evicted entry to the VC */
			if(myvc.isUsed)
			{
				/* Get the VC tag that corresponds to this entry in the DC */
				myvc.tag = (mydc.theDC[mydc.index][2] << mydc.indexBits()) | mydc.index;
				/* Get the dirty bit from this entry */
				dirty = mydc.theDC[mydc.index][1];
				/* Get the physical number that for this entry */
				LRUphys = mydc.theDC[mydc.index][3];
				/* Add the evicted page to the VC */
				addtoVC(myvc, dirty, LRUphys);		
			}

			/* Updates the physical number */
			mydc.theDC[mydc.index][3] = physnum;

			/* Uodates the tag */
			mydc.theDC[mydc.index][2] = mydc.tag;
			++mydc.misses;
		
			/* Check if the entry is dirty, reference the memory once if it is not dirty */
			if(mydc.theDC[mydc.index][1] == 0)
				++mydc.memrefs;
			else
				mydc.memrefs += 2;

			/* Set dirty bit if write miss, resets it for a read miss */
			if(RW == 'W')
				mydc.theDC[mydc.index][1] = 1;
			else
				mydc.theDC[mydc.index][1] = 0;

		}
		else
		{
			/* loop through the LRU numbers for each entry */
			for(int i = 2; i < entries; i += count)
			{
				/* Makes sure min has information to compore entries with */
				if(i == 2)
				{
					min = mydc.theDC[mydc.index][i];
					index = i + 1;
				}
				else if(mydc.theDC[mydc.index][i] < min)
				{
					min = mydc.theDC[mydc.index][i];
					index = i + 1;
				}
			}

			/* Add the evicted page to the VC */
			if(myvc.isUsed)
			{
				/* Get the VC tag that corresponds to this entry in the DC */
				myvc.tag = (mydc.theDC[mydc.index][index] << mydc.indexBits()) | mydc.index;

				/* Get the dirty bit for this entry */
				dirty = mydc.theDC[mydc.index][index-2];
				/* Get the physical number for this entry */
				LRUphys = mydc.theDC[mydc.index][index+1];
				addtoVC(myvc, dirty, LRUphys);		
			}

			/* Updates the DC with the physical number */
			mydc.theDC[mydc.index][index+1] = physnum;

			/* Updates the DC's tag */
			mydc.theDC[mydc.index][index] = mydc.tag;
		
			if(mydc.numEntries > 1)
			{
				/* Update the LRU number for the entry and at the end of the set */
				mydc.theDC[mydc.index][index-1] = mydc.theDC[mydc.index][mydc.columns-1];
				++mydc.theDC[mydc.index][mydc.columns-1];
			}

			++mydc.misses;
		
			/* Check if the entry is dirty, reference the memory once if it is not dirty */
			if(mydc.theDC[mydc.index][index-2] == 0)
				++mydc.memrefs;
			else
				mydc.memrefs += 2;

			/* Set dirty bit if write miss, resets it for a read miss */
			if(RW == 'W')
				mydc.theDC[mydc.index][index-2] = 1;
			else
				mydc.theDC[mydc.index][index-2] = 0;

		}
	}
}

/**
 * @name  : invalidPages
 * @brief : Used to invalidate pages in the TLB, DC, and VC.
 */
void invalidPages(DC &mydc, VC &myvc, TLB &mytlb, int physnum)
{
	int count;
	int entries = mydc.columns;

	/* Sets count and entry to loop through the valid entries */
	if(mydc.numEntries == 1)
		count  = 4;
	else
	{
		count = 5;
		entries -= 1;
	}

	/* Loops through the DC's sets */
	for(int i = 0; i < mydc.numSets; ++i)
	{
		/* Loops through each sets valid bits */
		for(int j = 0; j < entries; j += count)
		{
			/* Check ig the page should be invalidated */
			if(mydc.theDC[i][j] != 0 && mydc.theDC[i][j+count-1] == physnum)
			{
				/* Invalidate entry */
				mydc.theDC[i][j] = 0;
				/* Increment the memory reference depending on whether we use the VC or not */
				if(mydc.theDC[i][j+1] == 1)
				{
					++mydc.memrefs;
					if(myvc.isUsed)
						++myvc.memrefs;
				}
			}
		}
	}

	/* Invalidate the entries in the VC if it is used */
	if(myvc.isUsed)
	{
		if(myvc.numEntries == 1)
			count = 4;
		else
			count = 5;
	
		/* Loops through the VC's valid bits */
		for(int i = 0; i < myvc.columns; i += count)
		{
			/* Checks if the entry need to be invalidated */
			if(myvc.theVC[i] != 0 && myvc.theVC[i+count-1] == physnum)
			{
				/* Invalidate the page and increment memory references */
				myvc.theVC[i] = 0;
				if(myvc.theVC[i+1] == 1)
					++myvc.memrefs;
			}
		}
	}	

	/* Invalidate the entries in the TLB if it is used */
	if(mytlb.isUsed)
	{
		entries = mytlb.columns;
		if(mytlb.numEntries == 1)
			count = 4;
		else
		{
			count = 5;
			entries -= 1;
		}

		for(int i = 0; i < mytlb.numSets; ++i)
		{
			/* Loops through the valid entries */
			for(int j = 0; j < entries; j +=count)
			{
				/* Checks is the entry needs to be invalidated */
				if(mytlb.theTLB[i][j] != 0 && mytlb.theTLB[i][j+count-1] == physnum)
				{
					mytlb.theTLB[i][j] = 0;
				}
			}
		}
	}

}

/**
 * @name  : LRUpt
 * @brief : Gets the index to the LRU entry in the PT.
 */
void LRUpt(PT &mypt, int &index)
{
	int min;
	bool first = true;	

	/* Loop through the virtual pages */
	for(int i = 0; i < mypt.numVirtual; ++i)
	{
		/* Look through all the valid entries */
		if(mypt.thePT[i][0] == 1)
		{
			/* Finds the LRU index */
			if(first)
			{
				min = mypt.thePT[i][2];
				index = i;
				first = false;
			}
			else if(min > mypt.thePT[i][2])
			{
				min = mypt.thePT[i][2];
				index = i;
			}	
		}
	}

}

/**
 * @name  : simPT (Simulate PT)
 * @brief : Adds entries to the PT. It calls invalidPages if it has given out all the pages.
 * 			It modifies physnum so that physnum is the one the DC will process.
 */
void simPT(PT &mypt, int &physnum, int virtnum, char RW, DC &mydc, VC &myvc, TLB &mytlb)
{
	int LRUindex;
		
	/* Check if the entry is valid */
	if(mypt.thePT[virtnum][0] == 0)
	{
		/* Check if all the pages have been assigned */
		if(!mypt.allpUsed)
		{
			/* Update the PT's physical number */
			mypt.thePT[virtnum][3] = mypt.physnumber;
			/* Update the physical number with the one in the PT */
			physnum = mypt.thePT[virtnum][3];
	
			++mypt.physnumber;
	
			/* Sets allpUsed to true if we have assigned all pages */
			if(mypt.physnumber == (mypt.numPhys))
				mypt.allpUsed = true;
		}
		/* Perform LRU on the PT */
		else
		{
			/* Returns an index to the LRU entry */
			LRUpt(mypt, LRUindex);
			/* Update the PT with the LRU physical numebr */
			mypt.thePT[virtnum][3] = mypt.thePT[LRUindex][3];
			/* Update the PT dirty bit with the LRU's dirty bit */
			mypt.thePT[virtnum][1] = mypt.thePT[LRUindex][1];
			/* Invalidate the LRU entry */
			mypt.thePT[LRUindex][0] = 0;
		
			/* Update physical number with the one from the PT */
			physnum = mypt.thePT[virtnum][3];
		
			/* Invalidate pages in the DC, VC, and TLB */
			invalidPages(mydc, myvc, mytlb, physnum);
		}

		/* Update the LRU number for this entry */
		mypt.thePT[virtnum][2] = mypt.LRUnum;
		++mypt.LRUnum;
		/* Update this entry as valid */
		mypt.thePT[virtnum][0] = 1;
		++mypt.pfaults;

		/* Increment disk references based off whether the page is dirty or not */
		if(mypt.thePT[virtnum][1] == 0)
			++mypt.diskrefs;
		else
			mypt.diskrefs += 2;

		/* Make the page ditry are write misses and clean on read misses */
		if(RW == 'W')
			mypt.thePT[virtnum][1] = 1;
		else
			mypt.thePT[virtnum][1] = 0;
		
		mypt.isHit = false;
	}
	/* Deals with indexing into a valid entry */
	else
	{
		++mypt.hits;
		mypt.isHit = true;

		/* Update this entry's LRU number */
		mypt.thePT[virtnum][2] = mypt.LRUnum;
		++mypt.LRUnum;

		/* Set the dirty bit for write hits */
		if(RW == 'W')
			mypt.thePT[virtnum][1] = 1;	

		/* Updates the physical number with the one from thePT */
		physnum = mypt.thePT[virtnum][3];
	}
}

/**
 * @name  : checkTLB
 * @brief : Check if an entry is in the TLB. It also updates validInex with
 * 			the index for where we want to add a entry to the TLB.
 */
void checkTLB(TLB &mytlb, int &physnum, int &validIndex, char RW)
{
	int count, min, tempIndex;
	int entries = mytlb.columns;
	bool broke = false;	
	bool invalid = false;

	/* Sets the looping values */
	if(mytlb.numEntries == 1)
		count = 4;
	else
	{
		count = 5;
		entries -= 1;
	}

	/* Loops through the valid bits */
	for(int i = 0; i < entries; i += count)
	{
		/* Grabs the index for the first invalid entry */
		if(mytlb.theTLB[mytlb.index][i] == 0 && !invalid)
		{
			invalid = true;
			tempIndex = i;
		}
		/* Checks for a hit */
		else if(mytlb.theTLB[mytlb.index][i] != 0 && mytlb.theTLB[mytlb.index][i+count-2] == mytlb.tag)
		{
			/* Updates the physical number */
			physnum = mytlb.theTLB[mytlb.index][i+count-1];
			mytlb.isHit = true;
			++mytlb.hits;

			/* Updates the dirty bit on write hits */
			if(RW == 'W')
				mytlb.theTLB[mytlb.index][i+1] = 1;

			/* Update the LRU number for the entry and for the set */
			if(mytlb.numEntries > 1)
			{
				mytlb.theTLB[mytlb.index][i+2] = mytlb.theTLB[mytlb.index][mytlb.columns-1];
				++mytlb.theTLB[mytlb.index][mytlb.columns-1];
			}

			broke = true;
			break;
		}
	}

	/* If we found a invalid bit and we did not get a hit then we updat the valid Index, isHit, and misses */
	if(invalid && !broke)
	{
		validIndex = tempIndex;
		mytlb.isHit = false;
		++mytlb.misses;
	}
	/* Gets the validIndex using LRU if no hit and no invalid bits */
	else if(!broke && !invalid)
	{
		++mytlb.misses;
		mytlb.isHit = false;

		if(mytlb.numEntries == 1)
			validIndex = 0;
		else
		{
			/* Loop through the LRU numbers */
			for(int i = 2; i < entries; i += count)
			{
				/* Get the LRU validIndex */
				if(i == 2)
				{
					min = mytlb.theTLB[mytlb.index][i];
					validIndex = i - 2;
				}
				else if(min > mytlb.theTLB[mytlb.index][i])
				{
					min = mytlb.theTLB[mytlb.index][i];
					validIndex = i - 2;
				}
			}
		}
	}
}

/**
 * @name  : addtoTLB (Add to TLB)
 * @brief : Adds an entry to the TLB. Uses validIndex to know what to update
 */
void addtoTLB(TLB &mytlb, int validIndex, char RW, int physnum)
{
	int count;

	/* Sets the loop counter */
	if(mytlb.numEntries == 1)
		count = 4;
	else
		count = 5;

	/* Updates the valid bit */
	mytlb.theTLB[mytlb.index][validIndex] = 1;

	/* Make the bit dirty on write misses and clean on read misses */
	if(RW == 'W')
		mytlb.theTLB[mytlb.index][validIndex+1] = 1;
	else
		mytlb.theTLB[mytlb.index][validIndex+1] = 0;

	/* Update the LRU number and increments the one at the end of the set */
	if(mytlb.numEntries > 1)
	{
		mytlb.theTLB[mytlb.index][validIndex+2] = mytlb.theTLB[mytlb.index][mytlb.columns-1];
		++mytlb.theTLB[mytlb.index][mytlb.columns-1];
	}	

	/* Update the tag and physical number */
	mytlb.theTLB[mytlb.index][validIndex+count-2] = mytlb.tag;
	mytlb.theTLB[mytlb.index][validIndex+count-1] = physnum;

}

/**
 * @name  : sim
 * @brief : Calls all the functions that simualate the DC, VC, TLB, and PT.
 * 			It also calculates values these functions need and it prints
 * 			the data for each entry.
 */
void sim(int addr, char RW, TLB &mytlb, PT &mypt, DC &mydc, VC &myvc)
{
	int pageoff, physnum, VCtagbits, virtnum, validIndex;
	int virtAddr = addr;
	bool isHit = false;
	std::string res, VCres, PTres, TLBres;

	/* Calculate the page offset */
	pageoff = addr & (( 1 << mypt.offsetBits()) - 1);

	/* Calculate the physciacl number if no PT used */
	if(!mypt.isVirtual)
		physnum = (addr >> mypt.offsetBits()) & ((1 << mypt.numphysBits()) - 1);
	else
	{
		/* Calculate the virtual number */
		virtnum = (addr >> mypt.offsetBits()) & ((1 << mypt.indexBits()) - 1);

		if(mytlb.isUsed)
		{
			/* Calculates the TLB's index and tag */
			mytlb.index = virtnum & ((1 << mytlb.indexBits()) - 1);
			mytlb.tag = virtnum >> mytlb.indexBits();

			/* Checks for a hit in the TLB */
			checkTLB(mytlb, physnum, validIndex, RW);
		}
		/* Makes sure simPT runs if no TLB but we have a PT */
		else
			mytlb.isHit = false;

		if(!mytlb.isHit)
		{
			/**
 			 * This function calls a function that invalidates entries.
 			 * It also sets the physnum value.
 			 */
			simPT(mypt, physnum, virtnum, RW, mydc, myvc, mytlb);

			if(mypt.isHit)
				PTres = "hit ";
			else
				PTres = "miss";

			TLBres = "miss";

			if(mytlb.isUsed)
				/* Add to tlb after simPT because simPT invalidates entries in the tlb */
				addtoTLB(mytlb, validIndex, RW, physnum);
		}
		else
		{
			TLBres = "hit ";
			PTres = "    ";
	
			/* Update the LRUnum in PT on TLB hit */
			mypt.thePT[virtnum][2] = mypt.LRUnum;
			++mypt.LRUnum;
			
			/* Sets dirty bit in PT for TLB hit */
			if(RW == 'W')
				mypt.thePT[virtnum][1] = 1;	
		}
		/* Calculate the address used for the DC using the offset and physical number */
		addr = addr & ((1 << mypt.offsetBits()) - 1);
		addr = (physnum << mypt.offsetBits()) | addr;
	}

	/* Calculate the DC's index and tag */
	mydc.tag = (addr >> (mydc.offsetBits() + mydc.indexBits())) & ((1 << mydc.tagBits) - 1);
	mydc.index = (addr >> mydc.offsetBits()) & ((1 << mydc.indexBits()) - 1);

	if(myvc.isUsed)
	{
		/* Get the VC number of bits for its tag */
		VCtagbits = mydc.tagBits + mydc.indexBits();
		/* Calculates the VC's tag */
		myvc.tag = (addr >> mydc.offsetBits()) & ((1 << VCtagbits) - 1);
		
		/* Check if myvc.tag is in the VC */
		isinVC(myvc);

		if(myvc.isHit)
		{
			VCres = "hit ";
			--mydc.memrefs;
		}
		else
			VCres = "miss";
	}

	/* Adds to the DC and VC */
	wback(mydc, RW, isHit, myvc, physnum, mypt.isVirtual);

	/* Re-caculate the VC tag for being printed out */
	if(myvc.isUsed)
		myvc.tag = (addr >> mydc.offsetBits()) & ((1 << VCtagbits) - 1);

	/* Sets if DC had a hit */
	if(isHit)
		res = "hit ";
	else
		res = "miss";

	/* Print Data */

	if(!myvc.isUsed && !mytlb.isUsed && !mypt.isVirtual)
		printf("%08x%12x%26x%7x%4x%5s\n", addr, pageoff, physnum, mydc.tag, mydc.index, res.c_str());
	else if(myvc.isUsed && !mytlb.isUsed && !mypt.isVirtual)
		printf("%08x%12x%26x%7x%4x%5s%7x%5s\n", addr, pageoff, physnum, 
				mydc.tag, mydc.index, res.c_str(), myvc.tag, VCres.c_str());
	else if(myvc.isUsed && mypt.isVirtual && !mytlb.isUsed)
		printf("%08x%7x%5x%21s%5x%7x%4x%5s%7x%5s\n", virtAddr, virtnum, 
				pageoff, PTres.c_str(), physnum, mydc.tag, mydc.index, res.c_str(), myvc.tag, VCres.c_str());
	else if(mypt.isVirtual && !myvc.isUsed && !mytlb.isUsed)
		printf("%08x%7x%5x%21s%5x%7x%4x%5s\n", virtAddr, virtnum, pageoff, 
				PTres.c_str(), physnum, mydc.tag, mydc.index, res.c_str());
	else if(mypt.isVirtual && myvc.isUsed && mytlb.isUsed)
		printf("%08x %6x %4x %6x %3x %4s %4s %4x %6x %3x %4s %6x %4s\n", virtAddr, 
				virtnum, pageoff, mytlb.tag, mytlb.index, TLBres.c_str(), PTres.c_str(), 
				physnum, mydc.tag, mydc.index, res.c_str(), myvc.tag, VCres.c_str());

	else if(mypt.isVirtual && mytlb.isUsed && !myvc.isUsed)
		printf("%08x %6x %4x %6x %3x %4s %4s %4x %6x %3x %4s\n", virtAddr, 
				virtnum, pageoff, mytlb.tag, mytlb.index, TLBres.c_str(), PTres.c_str(), 
				physnum, mydc.tag, mydc.index, res.c_str());

}

/**
 * @name  : printStats (Print Statistics)
 * @brief : Prints the simulation statistics.
 */
void printStats(TLB &mytlb, PT &mypt, DC &mydc, VC &myvc, int reads, int writes)
{
	int l1hits, l1misses;
	float accesses = reads + writes;
	float ptaccess = mypt.hits + mypt.pfaults;

	printf("\n\n");
	std::cout << "Simulation statistics\n";

	printf("\n");

	if(!mytlb.isUsed)
	{
		std::cout << "dtlb hits        : 0\n";
		std::cout << "dtlb misses      : 0\n";
		std::cout << "dtlb hit ratio   : N/A\n";
	}
	else
	{
		std::cout << "dtlb hits        : " << mytlb.hits << "\n";
		std::cout << "dtlb misses      : " << mytlb.misses << "\n";
		std::cout << "dtlb hit ratio   : ";
		std::cout << std::fixed << std::setprecision(6) << mytlb.hits/accesses << "\n";
	}

	printf("\n");
	if(!mypt.isVirtual)
	{
		std::cout << "pt hits          : 0\n";
		std::cout << "pt faults        : 0\n";
		std::cout << "pt hit ratio     : N/A\n";
	}
	else
	{
		std::cout << "pt hits          : " << mypt.hits << "\n";
		std::cout << "pt faults        : " << mypt.pfaults << "\n";
		std::cout << "pt hit ratio     : ";
		std::cout << std::fixed << std::setprecision(6) << mypt.hits/ptaccess << "\n";
	}

	printf("\n");

	std::cout << "dc hits          : " << mydc.hits << "\n";
	std::cout << "dc misses        : " << mydc.misses << "\n";
	std::cout << "dc hit ratio     : ";
	std::cout << std::fixed << std::setprecision(6) << mydc.hits/accesses << "\n";

	printf("\n");

	if(!myvc.isUsed)
	{
		std::cout << "vc hits          : 0\n";
		std::cout << "vc misses        : 0\n";
		std::cout << "vc hit ratio     : N/A\n";
	}
	else
	{
		std::cout << "vc hits          : " << myvc.hits << "\n";
		std::cout << "vc misses        : " << myvc.misses << "\n";
		std::cout << "vc hit ratio     : ";
		std::cout << std::fixed << std::setprecision(6) << myvc.hits/accesses << "\n";
	}

	printf("\n");

	if(!myvc.isUsed)
	{
		l1hits = mydc.hits;
		l1misses = mydc.misses;
	}
	else
	{
		l1hits = mydc.hits + myvc.hits;
		l1misses = mydc.misses - myvc.hits;
	}

	std::cout << "L1 hits          : " << l1hits << "\n";
	std::cout << "L1 misses        : " << l1misses << "\n";
	std::cout << "L1 hit ratio     : ";
	std::cout << std::fixed << std::setprecision(6) << l1hits/accesses << "\n";

	printf("\n");

	std::cout << "Total reads      : " << reads << "\n";
	std::cout << "Total writes     : " << writes << "\n";
	std::cout << "Ratio of reads   : ";
	std::cout << std::fixed << std::setprecision(6) << reads/accesses << "\n";

	printf("\n");

	if(!myvc.isUsed)
		std::cout << "main memory refs : " << mydc.memrefs << "\n";
	else
		std::cout << "main memory refs : " << myvc.memrefs << "\n";

	if(!mypt.isVirtual)
	{
		std::cout << "page table refs  : 0\n";
		std::cout << "disk refs        : 0\n";
	}
	else
	{
		std::cout << "page table refs  : " << (mypt.hits + mypt.pfaults) << "\n";
		std::cout << "disk refs        : " << mypt.diskrefs << "\n";
	}
}

int main()
{
	TLB mytlb;
	PT mypt;
	DC mydc;
	VC myvc;
	char RW;
	int addr, physAddrBits;
	int reads = 0;
	int writes = 0;
	std::string line;
	std::stringstream streamline;

	/* Read in the config file and get ready to initialize structures */
	config(mytlb, mypt, mydc, myvc);

	/* Calculate the number of bits in the physical address */
	physAddrBits = mypt.numphysBits() + mypt.offsetBits();

	/* Initalize the DC */
	initDC(mydc, physAddrBits);
	
	/* Initalize the VC, PT, and TLB if they are used */
	if(myvc.isUsed)
		initVC(myvc);
	
	if(mypt.isVirtual)
		initPT(mypt);

	if(mytlb.isUsed)
		initTLB(mytlb);
	
	/* Print the initial config stats */
	printout(mytlb, mypt, mydc, myvc);

	/* Read in the .dat file */
	while(std::cin >> line)
	{
		/* Get the R or W character */
		RW = line[0];

		/* Increment the number of read and writes depending on RW */
		if(RW == 'R')
			++reads;
		else if(RW == 'W')
			++writes;

		//Erase starts at line[0] and erases two characters.
		line.erase(0,2);
		streamline << std::hex << line;
		streamline >> addr;
		
		/* Resets line */
		streamline.clear();
		streamline.str(std::string());

		/* Simulate the memory hierachy */
		sim(addr, RW, mytlb, mypt, mydc, myvc);
	}
	/* Print the simulation stats */
	printStats(mytlb, mypt, mydc, myvc, reads, writes);
}
