#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include<vector>
#include<dirent.h>
#include<fcntl.h>

/* External environment variables */
extern char **environ;

/**
 * @name : parse_line
 * @brief: Takes in an input string and parses it, on spaces, into a vector.
 *         The vecotr of parsed tokens is returned.
 */
std::vector<char*> parse_line(const char input[], char *del)
{
   char cpy[5000];
   char* token;
   char *ptr, *ptr2;
   std::vector<char*> tokens;

   strcpy(cpy, input);

   ptr = cpy; 

   /* Parse the string on spaces and push back each char* into a vector */
   while((token = strtok_r(ptr, del, &ptr)))
   {
      ptr2 = strstr(token, "\"");
      if(ptr2)
         *ptr2 =' ';
 
      tokens.push_back(token);
   }

   return tokens;
}

/**
 * @name : cd (Change Directory)
 * @breif: Change to the working directory to the string passed in
 */
void cd(char* dir)
{
   /* If the user does cd ~ change to the users home directory */
   if(strcmp(dir, "~") == 0)
      dir = getenv("HOME");

   /* Change to the desired directory */
   if(chdir(dir) != 0)
      perror(dir);
}

/**
 * @name : execute
 * @brief: This function execute external commands passed in through cmds.
 *         Since cmds has a size of 10 this function will except one command
 *         and 9 aurguments.
 */
bool execute(const std::vector<char*> parsed)
{
   DIR *d;
   struct dirent *w;
   char path[PATH_MAX];
   char *ptr; 
   int stat;
   int exec_size = 30;
   char *cmds[30];
   std::vector<char*> element;

   element.push_back(NULL);

   for(int i = 0; i < exec_size; ++i)
      cmds[i] = NULL;

   element = parse_line(parsed[0], (char*)" ");
   for(unsigned int i = 0; i < element.size(); ++i)
   {
      if(int(i) < exec_size)
         cmds[i] = element[i];
   }

   /* Make a copy of path so it can be tokenized without changing the path */
   strcpy(path, getenv("PATH"));

   /* Tokenize the path on the ':' character */
   ptr = strtok(path, ":");
   while(ptr != NULL)
   {
      /* If failed to open path move to next path */
      if((d = opendir(ptr)) == NULL)
      {
         ptr = strtok(NULL, ":");
         continue;
      }
      
      /* Loop through all the files in the directory that was opened */
      while((w = readdir(d)) != NULL)
      {
         /** 
          * Check to see if the command the user entered matches any of the
          * file in this path.
          */ 
         if(strcmp(cmds[0], w->d_name) == 0)
         {
            /* Create the path to the file we want to execute */
            sprintf(path, "%s/%s", ptr, cmds[0]);
            
            /* Create a child to execute the external command */
            if(fork() == 0)
            {
               /* Execute the command with its aurguments */
               if(execv(path, cmds) == -1)
               {
                  printf("Command failed.\n");
                  return EXIT_FAILURE;
               }
            }
            /* Have the parent wait for the child to finish */
            else
            {
               wait(&stat);
               if(stat >> 8 == 0xff)
                  printf("Command Failed\n");
            }            
            return true;
         }   
      }
      /* Grab the next path */
      ptr = strtok(NULL, ":");
   }
   return false;
}

/**
 * @name : exec_pipe (Execute Pipe)
 * @brief: Emplements the ability to pipe commands. This function assumes a max
 *         of two pipes.
 * @usage: command | command | command
 */
bool exec_pipe(std::vector<char*> parsed)
{
   int fds[4], child;
   int exec_size = 30;
   char *cmds[30];
   std::vector<char*> element;
   unsigned int num_pipes = parsed.size()-1;

   /* Initialize cmds */
   for(int j = 0; j < exec_size; ++j)
      cmds[j] = NULL;

   
   /* Tokenize the first command on spaces */
   element = parse_line(parsed[0], (char*)" ");
   for(unsigned int j = 0; j < element.size(); ++j)
   {
      if(int(j) < exec_size)
         cmds[j] = element[j];
   }
   
   /* Create the pipes */
   pipe(fds);
 
   /* If there is two pipes set up the second group */
   if(num_pipes == 2)
      pipe(fds+2);

   /* Fork a child to replace the first commands stdout */
   if(fork() == 0)
   {
      /* Replace stdout with the write end of a pipe */
      dup2(fds[1], 1);

      /* Close all the file descriptors */
      close(fds[0]); 
      close(fds[1]);

      /* Close the extra two pipes if they are used */
      if(num_pipes == 2)
      {
         close(fds[2]);
         close(fds[3]);
      }

      execvp(cmds[0], cmds);
      exit(0);
   }

   /* Fork a child to do the second command */
   if(fork() == 0)
   {

      /* Tokenize the next command */
      element = parse_line(parsed[1], (char*)" ");
      for(unsigned int j = 0; j < element.size(); ++j)
      {
         if(int(j) < exec_size)
            cmds[j] = element[j];
      }
             
      /* Replace stdin with read end of pipe */
      dup2(fds[0], 0);

      if(num_pipes == 2)
      {
         /* If theres a second pipe repalce stdout with the write end of pipe */
         dup2(fds[3], 1);
          
         /* Close the extra pipes */
         close(fds[2]);
         close(fds[3]);
      }
     
      /* CLose all the pipes */
      close(fds[0]);
      close(fds[1]);

      execvp(cmds[0], cmds);
      exit(0);
   }

   /* If there are two pipes spawn a 3rd child */
   if(num_pipes == 2)
   {
      if(fork() == 0)
      {
         /* Tokenize the last command */
         element = parse_line(parsed[2], (char*)" ");
         for(unsigned int j = 0; j < element.size(); ++j)
         {
            if(int(j) < exec_size)
               cmds[j] = element[j];
         }

         /* Replace stdin with the read end of the pipe */
         dup2(fds[2], 0);
 
         /* Close all pipes */
         for(unsigned int k = 0; k < num_pipes*2; ++k)
            close(fds[k]);
  
         execvp(cmds[0], cmds);
         exit(0);
      }
   }

  /* Close all the pipes in the parent */
   close(fds[0]);
   close(fds[1]);
   if(num_pipes == 2)
   {
      close(fds[2]);
      close(fds[3]);
   }

   /* The parent waits for all the children to finish */
   wait(&child);
   wait(&child);
   if(num_pipes == 2)
   {
      wait(&child);
      wait(&child);
   }

   return true;
}

/**
 * @name : set_cmd (Set Command)
 * @brief: Used to initialize the cmds char * array.
 */
void set_cmd(char *to_parse, char **cmds, int size)
{
   std::vector<char*> element;

   for(int i = 0; i < size; ++i)
      cmds[i] = NULL;

   element = parse_line(to_parse, (char*)" ");

   for(unsigned int j = 0; j < element.size(); ++j)
   {
      if(int(j) < size)
         cmds[j] = element[j];
   }
}

/**
 * @name : exec_io (Execute Input/Output)
 * @brief: Implements I/O redirection for command > file, command < file, and
 *         command < file1 > file2
 * @param: parsed contians the string parsed on '>', '<', or '<>' this
 *         corresponds to states 0, 1, and 2 respectively.
 */
bool exec_io(std::vector<char*> parsed, int state)
{
   int input, output, stat;
   int num_cmds = 30;
   
   /* Used to store one command and 29 aurguments */
   char *cmds[30];

   /* Used to delete the blank space from infront of the file names */
   char buf[1024], buf2[1024];

   /* Used to parse the string with the command on aurguments */
   std::vector<char*> element;

   /* Initialize the commands array to NULL */
   for(int i = 0; i < num_cmds; ++i)
      cmds[i] = NULL;

   /* Use parse_line to delete the blankspace in parsed[1][0] */
   strcpy(buf, parse_line(parsed[1], (char*)" ")[0]);

   /* When there is more than one file delete blank space from parsed[2][0] */
   if(state == 2)
      strcpy(buf2, parse_line(parsed[2], (char*)" ")[0]);

   /* Get the command and its aurguments */
   element = parse_line(parsed[0], (char*)" ");

   /* Copy each vector element into cmds */
   for(unsigned int j = 0; j < element.size(); ++j)
   {
      if(int(j) < num_cmds)
         cmds[j] = element[j];
   }

   /* Output to file */
   if(state == 0)
   {
      if(fork() == 0)
      {
         /* Get the file descriptor and open the specified file */
         output = open(buf , O_RDWR | O_TRUNC | O_CREAT, 0777);
         
         /* Replace stdout with our file descriptor */
         dup2(output, 1);
         close(output);
      
         execvp(cmds[0], cmds);
      }
      /* Parent waits for child to finish */
      else
      {
         wait(&stat);
         if(stat >> 8 == 0xff)
            printf("command failed\n");
      }
   }

   /* input to file */
   if(state == 1)
   {
      if(fork() == 0)
      {
         /* Open and get the fds for the specified file */
         input = open(buf , O_RDONLY);

         /* Replace stdin with the fds */
         dup2(input, 0);
         close(input);

         execvp(cmds[0], cmds);
      }
      else
      {
         wait(&stat);
         if(stat >> 8 == 0xff)
            printf("command failed\n");
      }
   }

   /* intput and output ex command < file > file */
   if(state == 2)
   {

      if(fork() == 0)
      {
         /* Open and get the fds for both of the specified files */
         output = open(buf2 , O_WRONLY | O_TRUNC | O_CREAT, 0777);
         input = open(buf , O_RDONLY);

         /* Replace stdout with output and stdin with input */
         dup2(output, 1);
         dup2(input, 0);

         close(input);
         close(output);

         execvp(cmds[0], cmds);
      }
      else
      {
         wait(&stat);
         if(stat >> 8 == 0xff)
            printf("command failed\n");
      }
   }

   return true;
}

/**
 * @name : exec_background (Execute Background Processes)
 * @brief: Executes every command in parsed. Where only the last command
 *         in the vector is executed as a foreground process the rest are
 *         background processes.
 */
bool exec_background(std::vector<char*> parsed)
{
    int stat;
    int size = 30;
    char* cmds[30];
    pid_t pid;

   for(unsigned int i = 0; i < parsed.size(); ++i)
   {
      set_cmd(parsed[i], cmds, size);
      /* Create a child to execute the external command */
      pid = fork();
      if((pid) < 0)
      {
         perror("fork: ");
         return EXIT_FAILURE;
      }
      if((pid) == 0)
      {
         /* Execute the command with its aurguments */
         if(execvp(cmds[0], cmds) < 0)
         {
            printf("Command failed.\n");
            return EXIT_FAILURE;
         }
         exit(0);
       }
   }

   /* Wait for each spawned child to finish */
   for(unsigned int i = 0; i < parsed.size(); ++i)
      wait(&stat);

   return false;
}

int main(int argc, char* argv[])
{  
   int stat;
   char input[82];
   int args = 0;
   char* cmd;
   char* ptr, *ptr2;
   std::vector<char*> parsed, p2;
   int i = 0;

   /* Set the MYPATH variable to contian the path to myls */
   setenv("MYPATH", realpath("myls", NULL), 1);

   /* Prompt the user to enter a command */
   printf("$ ");

   /* Read in user input */
   while(std::cin.getline(input,82))
   {
      /* Make sure i is set to zero at the start of each loop */
      i = 0;

      /* Parse the initial input on spaces */
      parsed = parse_line(input, (char*)" "); 
     
      /* The size of ther parsed vector is the number of args entered */
      args = parsed.size();

      /* User hit enter and did not enter any commands */
      if(args <= 0)
      {
         printf("$ ");
         continue;
      }
      /* Grab the user entered command */
      cmd = parsed[0];

      /* Deal with the exit command */
      if(strcmp(cmd, "exit") == 0)
         return EXIT_SUCCESS;

      /* Deal with the cd command with to many arguments */
      else if(strcmp(cmd, "cd") == 0 && args > 2)
         printf("cd: Too many arguments.\n");

      /* Deal with the cd command */
      else if(strcmp(cmd, "cd") == 0 && args <= 2)
         cd(parsed[1]);

      /* Deal with the pwd command */
      else if(strcmp(cmd, "pwd") == 0)
         std::cout << realpath("./", NULL) << std::endl; 
 
      /* Deal with the set command */
      else if(strcmp(cmd, "set") == 0 && args == 1)
      {
         while(environ[i])
            std::cout << environ[i++] << std::endl;
      }
      /* Deal with the set command with multiple args */
      else if(strcmp(cmd, "set") == 0 && args > 1)
      {
         ptr = strtok(parsed[1], "=");
         ptr2 = strtok(NULL, "=");
         if(ptr && ptr2)
            setenv(ptr, ptr2, 1);
      }

      /* Run the myls external command */
      else if(strcmp(cmd, "myls") == 0)
      {
         /* Spawn child to execute command */
         if(fork() == 0)
         {
            if(args > 1)
            {
               if(execl(getenv("MYPATH"), "myls", parsed[1], (char*)0) == -1)
               {
                  printf("execl: failed to execute.\n");
                  return EXIT_FAILURE;
               }
            }
            else
            {
               if(execl(getenv("MYPATH"), "myls", (char*)0) == -1)
               {
                  printf("execl: failed to execute.\n");
                  return EXIT_FAILURE;
               }
            }
         }

         /* Have the parent wait for the child to finish */
         else
         {
            wait(&stat);
            if(stat >> 8 == 0xff)
               printf("command failed\n");
         }
      }

      /** 
       * Deals with pipes, I/O redirection, background processes, and invalid
       * commands
       */
      else
      {
          /* Check input to see if the user enter a pipe */
          if(strstr(input, "|"))
          {
             /* Tokenize the string on the | character */
             parsed = parse_line(input, (char*)"|");
             if(!exec_pipe(parsed))
                printf("Failed to execute %s\n", input);
          }

          /* Check input to see if the user enter I/O redirection */
          else if(strstr(input, "<") && strstr(input, ">"))
          {
             /* Tokenize the string on the < and > characters */
             parsed = parse_line(input, (char*)"<>");
             if(!exec_io(parsed, 2))
                printf("Failed to execute %s\n", input);
          }
          else if(strstr(input, ">"))
          {
             /* Tokenize the string on the > character */
             parsed = parse_line(input, (char*)">");
             if(!exec_io(parsed, 0))
                printf("Failed to execute %s\n", input);
          }
          else if(strstr(input, "<"))
          {
             /* Tokenize the string on the < character */
             parsed = parse_line(input, (char*)"<");
             if(!exec_io(parsed, 1))
                printf("Failed to execute %s\n", input);
          }

          /* Check input to see if the user entered an & */
          else if(strstr(input, "&"))
          {
             /* Tokenize the string on the | character */
             parsed = parse_line(input, (char*)"&");
             exec_background(parsed);
          }
          else
          {
             /** 
              * Tokenize the string on the | character so that execute gets
              * one string.
              */
             parsed = parse_line(input, (char*)"|");
             if(!execute(parsed))
                printf("No command for %s try agian.\n", input);
          }
      }
      printf("$ ");
   }

   printf("\n");

   return EXIT_SUCCESS;
}
