/**
 * @name : Camill Folsom
 * @class: Concurent, Parellel, and Distributed Programming
 * @date : October 24, 2018
 * @descr: This program is a ls program for unix systems. Its output closely
 *         matches that of ls -l. This program does not support any flagsr.
 *         The suspected usage is ls dir or ls. 
 */
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<dirent.h>
#include<time.h>
#include<string.h>
#include<pwd.h>
#include<grp.h>

void ls(char *dir)
{
   DIR *d;
   struct dirent *w;
   struct stat buf;
   struct passwd *pwd;
   struct group *grp;
   struct tm *p;
   char path[256];
   const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", 
                           "Aug", "Sept", "Oct", "Nov", "Dec"};

   /* Used to restor flags and fill */
   std::ios_base::fmtflags fstate = std::cout.flags();
   char oldfill = std::cout.fill();


   /* Open the given directory */
   if((d = opendir(dir)) == NULL)
   {
      printf("dir: The directory %s could not be found.\n", dir);
      exit(0);
   }

   /* Loop through all files and folders in directory */
   while((w = readdir(d)) != NULL)
   {
      /* Do not display . or .. directories */
      if(w->d_name[0] != '.')
      {
         /* Make sure path is a copy of dir at the start */
         strcpy(path, dir);
 
         /* If the user gave a path then stat need the name cated with dir */
         if(strcmp(dir, "./") != 0)
            strcat(path, w->d_name);  
         
         /* If user didnt give a directory or they gave ./ then ls this dir */
         else
            strcpy(path, w->d_name);

         /* Get directory info */
         if(stat(path, &buf) < 0)
         {
            std::cout << "stat " << w->d_name << " failed\n";
            continue;
         }
         
         /** 
          * The next four blocks of if statements print the permissions.
          * The first block prints a letter based on the file type.
          */
         if(S_ISREG(buf.st_mode))
            printf("-");
         else if(S_ISDIR(buf.st_mode))
            printf("d");
         else if(S_ISCHR(buf.st_mode))
            printf("c");
         else if(S_ISBLK(buf.st_mode))
            printf("b");
         else if(S_ISFIFO(buf.st_mode))
            printf("f");
         else if(S_ISLNK(buf.st_mode))
            printf("l");
         else if(S_ISSOCK(buf.st_mode))
            printf("s");
         else
            printf("u");

         /* This block prints the users permissions */
         if(S_IRUSR & buf.st_mode)
            printf("r");
         else
            printf("-");

         if(S_IWUSR & buf.st_mode)
            printf("w");
         else
            printf("-");

         if(S_IXUSR & buf.st_mode)
            printf("x");
         else
            printf("-");

         /* This block prints the groups permissions */
         if(S_IRGRP & buf.st_mode)
            printf("r");
         else
            printf("-");

         if(S_IWGRP & buf.st_mode)
            printf("w");
         else
            printf("-");

         if(S_IXGRP & buf.st_mode)
            printf("x");
         else
            printf("-");

         /* This block prints the other permissions */
         if(S_IROTH & buf.st_mode)
            printf("r");
         else
            printf("-");

         if(S_IWOTH & buf.st_mode)
            printf("w");
         else
            printf("-");

         if(S_IXOTH & buf.st_mode)
            printf("x");
         else
            printf("-");
         
         printf(" %d", (int)buf.st_nlink);

         /* Get the user and group */
         grp = getgrgid(buf.st_gid);
         pwd = getpwuid(buf.st_uid);
          
         /* Print the files user and group */
         printf(" %s %s ", pwd->pw_name, grp->gr_name);

         /* Print the files size */
         std::cout.width(5);
         std::cout << std::internal << buf.st_size;

         /* Restore the flags and fill */
         std::cout.flags(fstate);
         std::cout.fill(oldfill);

         /* Get the file last modification time */
         p = localtime(&buf.st_mtime);

         /* Print the month that coresaponds to the month number */
         for(int i = 0; i < 12; ++i)
            if(p->tm_mon == i)
               std::cout << " " << months[i];

         /* Print the month day */
         std::cout << " " << p->tm_mday << " ";

         /* Print hour and add zero in front if below 10 */
         if(p->tm_hour >= 10)
            std::cout << p->tm_hour << ":"; 
        else
            std::cout << "0" << p->tm_hour << ":";

        /* Print minutes as above */
        if(p->tm_min >=10)
           std::cout << p->tm_min;
        else
           std::cout << "0" << p->tm_min;

         /* Print the name of the file */
         printf(" %s\n", w->d_name);
      }
   }
}

int main(int args, char* argv[])
{
   /* No path given ls current directory */
   if(args == 1)
   {
      ls((char*)"./");
   }
   if(args > 1)
   {
      /* Make it so the users can enter a path not ended by / */
      if(argv[1][strlen(argv[1])-1] != '/')
         strcat(argv[1], "/");

      ls(argv[1]);
   }

   /* Make sure the user provides acceptible input */
   if(args > 1 && argv[1][0] == '-')
      printf("ls error: usage ls or ls dir\n");

   return 0;
}
