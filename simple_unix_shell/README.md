A Simplified Unix Shell Program
===============================
This is a simple Unix shell program. Running this program will give the user
"$ " to prompt them to enter commands. To exit this program the use can use
CRTL-D or they may use the exit command. With this shell program there is also
a program called myls that prints all the data that ls -l prints, except for
the total. This shell program supports the commands myls, exit, cd, pwd, set,
up to two pipes, I/O redirection, background process, and all external 
commands that can be found in the environment variable "PATH". In general this
shell attempts to reproduce the Unix commands.

Compilation
-----------
The makefile will create an executable for both the myls and myshell programs. 
The command _make myls_ creates the myls program using the command:
```
g++ -Wall -ansi -pedantic myls.cpp -o myls
```
The command _make myshel_ creates the myshell program using the command:
```
g++ -Wall -ansi -pedantic myshell.cpp -o myshell
```

Usage
-----
The shell can be run using the command:
```
./myshell
```

Command Usages
--------------

* **myls** : "myls" prints output similar to ls -l.

* **pwd** : "pwd" prints the current absolute working directory

* **cd** : "cd dir" change to the specified directory. "cd .." goes to previous directory.

* **set** : "set PATHNAME=path1:path2..." will set a given PATHNAME to the given path.

* **exit** : "exit" or "CTRL-D" will exit the shell.

* **pipes** : "command1 | command2" pipes the output from command1 into command2 
              and command2 outputs its results. "command1 | command2 | command3" 
              pipes the output of command1 into command2 then command2's output is 
              read into command3 and command3 prints to output. Maximun pipes 
              supported is two.

* **I/O Re-direction** : "command < file" reads the file into the command as the 
                          commands input. 
                         "command > file" writes the commands output to a file. 
                         "command < file1 > file2" reads the contenst of into command 
                         and writes commands output to file2. The shell does not support 
                         other I/O redirection usages.

* **back-ground processes** : "command1 [args] &" executes the the specified command.
                              "command1 [args] & command2 [args]" executes the first 
                              command in the list in the background. 
                              "command1 [args] & command2 [args] & ... & commandn [args] "
                              executes each command up to commandn in the background.

* **external commands** : all commands in the enviroments variable "PATH" can be used
                      the same way they are used in other shells.

Shell Functions Declarations
----------------------------
```
std::vector<char*> parse_line(const char input[], char* del)
void cd(char* dir)
bool execute(const std::vector<char*> parsed)
bool exec_pipe(std::vector<char*> parsed)
void set_cmd(char *to_parse, char **cmds, int size)
bool exec_io(std::vector<char*> parsed, int state)
bool exec_background(std::vector<char*> parsed)
```
Shell Function Descriptions
---------------------------
* **parse_line** : Takes in an input string and parses it, on spaces, into a vector.
                   The vector of parsed tokens is returned.

* **cd** : Changes to the working directory to the string passed in.

* **execute** : This function execute external commands passed in through cmds.
                Since cmds has a size of 30 this function will except one command
                and 29 aurguments.

* **exec_pipe** : Executes the up to three pipe commands.

* **set_cmd** : Parses a string stored in a verctor on spaces and then sets a 
                char * array equal to each element.

* **exec_io** : Implements I/O redirection for command > file, command < file, and
                command < file1 > file2
                parsed contians the string parsed on '>', '<', or '<>' this
                corresponds to states 0, 1, and 2 respectively

* **exec_background** : Executes every command in parsed. Where only the last command
                        in the vector is executed as a foreground process the rest are
                        background processes. 

Limitations
-----------
**Pipe** : This shell supports a maximum of two pipes.

**I/O Reriection** : Only supports one input "<" and one output ">" operator.
                     command > file1 > file2 will only write to file1 one and
                     it will ignore file 2.

**Background Processes** : Given "command [args] &" will execute command as a 
                           forground processes.
