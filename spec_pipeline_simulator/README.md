Speculative Dynamically Scheduled Pipeline Simulator
====================================================
This program simulates a speculative dynamically scheduled pipeline. It reads in 
a '.dat' file where each line contains an assembly instruction.
```
S.S   F6,4(R2):0
L.S   F2,48(R2):0
MUL.S F0,F2,F4
SUB.S F8,F6,F2
```
For loads and stores the number after the colon is the address to load from or store
to.  
The file _config.txt_ is required to be in the same directory as the simulator.
It provides the configuration information for the buffers and the latencies needed
for the floating point instructions add, subtract, multiply, and divide.

The simulator outputs the configuration information read in from _config.txt_.
The simulations results are output as a table thats shows the instruction and the
cycle for which it executes, reads from memory, writes its results, and when the results
are committed. The final part of the output is the delay statistics. It show the number
of delays caused by the re-order buffer, reservation station, data memory, and 
true dependence.


Compilation
-----------
This program can be created using the supplied makefile. The makefile creates an
executable named _dynamsched.exe_ using the command:
```
g++ -Wall dynamsched.cpp -o dynamsched.exe
```

Usage
-----
To use this program a _config.txt_ file must be present in the same directory 
as the simulator. The program can be run using the command:
```
./dynamsched.exe < ../tests/trace1.dat
```

config.txt
----------
This file allows for the configuration of the buffers and the floating point latencies.
The buffer configuration allows for the number of stations for the following to be specified: effective addresses, floating point add and multiply addresses, and integers. The
number of open spots in the re-orders buffer is also specified. A legend is
given below:

* **eff addr** : Number of open effective address stations
* **fp adds**  : Number of open add stations
* **fp muls**  : Number of open multiply stations
* **ints**     : Number of open integer stations
* **reorder**  : Number of open spots in reorder buffer

An example buffer configuration is given below:
```text
buffers:
   eff addr: 1
    fp adds: 3
    fp muls: 3
       ints: 2
    reorder: 5

```
The latencies portion of the configuration file gives the number of cycles needed 
for the floating point instructions add, subtract, multiply, and divide. An example
of the latencies portion of the configuration file is given below.
```
latencies:
   fp add: 2
   fp sub: 2
   fp mul: 5
   fp div: 10
```
Example Output
--------------
The example output was created using _trace12.dat_. The first part of the output
is the configuration read in from the _config.txt_ file. The second part is the
results of the simulation. This is a table that list the instruction and the 
cycle for which the instruction is issued, executes, reads from memory, writes its
results, and commits its results. The final part of the output displays the total 
number of delays cause by the re-order buffer, reservation station, data memory, 
and true dependence. The example output is given below:
```text
Configuration
-------------
buffers:
   eff addr: 1
    fp adds: 3
    fp muls: 3
       ints: 2
    reorder: 5

latencies:
   fp add: 2
   fp sub: 2
   fp mul: 5
   fp div: 10


                    Pipeline Simulation
-----------------------------------------------------------
                                      Memory Writes
     Instruction      Issues Executes  Read  Result Commits
--------------------- ------ -------- ------ ------ -------
L.S   F0,0(R1):0           1   2 -  2      3      4       5
MUL.S F4,F0,F2             2   5 -  9            10      11
S.S   F4,0(R1):0           4   5 -  5                    12
S.S   F5,4(R2):0           6   7 -  7                    13
DADD  R1,R1,R3             7   8 -  8             9      14
BNE   R1,R2,L1             8  10 - 10                    15
L.S   F0,0(R1):0          11  12 - 12     14     15      16
MUL.S F4,F0,F2            12  16 - 20            21      22
S.S   F4,0(R1):0          15  16 - 16                    23
DADD  R1,R1,R2            16  17 - 17            18      24
BEQ   R1,R2,L1            17  19 - 19                    25
L.S   F6,34(R2):0         18  19 - 19     24     25      26
L.S   F2,45(R3):4         25  26 - 26     27     28      29
MUL.S F0,F2,F4            26  29 - 33            34      35
SUB.S F8,F6,F2            27  29 - 30            31      36
DIV.S F10,F0,F6           28  35 - 44            45      46
ADD.S F6,F8,F2            29  32 - 33            35      47
LW   R1,32(R4):0          30  31 - 31     32     33      48
DADD R3,R1,R2             35  36 - 36            37      49
DSUB R2,R3,R1             36  38 - 38            39      50
DADD R7,R1,R1             46  47 - 47            48      51
LW    R1,4(R6):0          47  48 - 48     49     50      52
L.S   F6,8(R1):16         50  51 - 51     52     53      54
L.S   F3,0(R1):8          53  54 - 54     55     56      57
L.S   F4,4(R1):12         56  57 - 57     58     59      60
S.S   F6,0(R2):0          59  60 - 60                    61
DADD  R1,R2,R2            60  61 - 61            62      63
SW    R1,0(R2):0          61  62 - 62                    64
SW    R1,0(R2):0          63  64 - 64                    65
LW    R3,32(R2):0         65  66 - 66     67     68      69
S.S   F6,4(R2):4          68  69 - 69                    70
L.S   F4,48(R1):4         70  71 - 71     72     73      74
L.S   F3,16(R2):0         73  74 - 74     75     76      77
L.S   F1,32(R2):12        76  77 - 77     78     79      80
SW    R3,32(R2):0         79  80 - 80                    81
ADD.S  F1,F2,F2           80  81 - 82            83      84
SUB.S  F3,F4,F5           81  82 - 83            84      85
MUL.S  F6,F7,F7           82  83 - 87            88      89
DIV.S  F8,F9,F10          83  84 - 93            94      95
LW   R1,0(R2):0           84  85 - 85     86     87      96
DADD R2,R1,R1             85  88 - 88            89      97
BNE  R2,R1,L1             86  90 - 90                    98


Delays
------
reorder buffer delays: 18
reservation station delays: 26
data memory conflict delays: 2
true dependence delays: 27
```