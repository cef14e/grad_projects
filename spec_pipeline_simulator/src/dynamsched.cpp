/**
 * @name   : Camill Folsom
 * @project: Simulating a Speculative Dynamically Scheduled Pipeline
 * @compile: g++ -Wall dynamsched.cpp -o dynamsched.exe
 */
#include<iostream>
#include<fstream>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<queue>

/**
 * @name : buffers
 * @brief: This struct tracks all the we are interested in data for the program
 */
struct buffers
{
   int effaddrs;                 /* Number of open effaddr stations */
   int adds;                     /* Same as above for add stations */
   int muls;                     /* Same as above for multiply stations */
   int ints;                     /* Same as above for integer stations */
   int reorder;                  /* Number of open spots in reorder buffer */

   int comcycle;                 /* Cycle when the previous instruction 
                                    commits */

   int storecycle;               /* Cycle when a store instruction commits */

   int datmemdelays;             /* Number of data memory delays */
   int reordersize;              /* Size of the reorder buffer */
   int reg1, reg2, reg3;         /* Numbers for registers from instructions */
   int dsp, addr;                /* Numbers for the displacements and addresses
                                    from the instructions */

   int columns;                  /* Number of columns in the buffer */
   int entrynum;                 /* Number when instruction entered buffer */
   int depends;                  /* Number of true dependencies */
   int afterwrite;               /* Takes the write cycle and adds one to it */
   int r_delays, s_delays;       /* Tracks reorder buffer and station delays */
                                 
   std::queue<int> s_free_addr;  /* Tracks the cycles when effaddrs are freed */
   std::queue<int> s_free_ints;  /* Same as above for int stations */
   std::queue<int> s_free_adds;  /* Same as above for add stations */
   std::queue<int> s_free_muls;  /* Same as above for multiply stations */
                                              
   std::queue<int> r_free;       /* Cycles when buffer is freed */
   std::vector<std::vector<int> > buffer; /* The reorder buffer */
   std::vector<int> memcycles;   /* Memory cycles for each instruction */
   std::vector<int> writecycles; /* Write cycles for each instruction */

   int busycol;                  /* Column for whether a entry is busy or not,
                                    it is 0 if entry is not busy 1 otherwise */

   int instrcol;                 /* Column with the number for intstruction */
   int reg1col;                  /* Column with the 1st registers value */
   int dspcol;                   /* Column with the dissplacements value */
   int reg2col;                  /* Column with the 2nd registers value */
   int addrcol;                  /* Column with addres from load or store */
   int reg3col;                  /* Column with the 3rd registers value */
   int comcol;                   /* Column with the cycle the entry commits */
   int afterwritecol;            /* Column with write cycle+1 for the entry */
   int entrycol;                 /* Column with entry number for the entry */

   int typesize;                 /* The number of instruction recognized */

   std::string types[12];        /* Holds the intructions recognized */
};

/**
 * @name : latencies
 * @brief: The lantencies for each flaoting point operation
 */
struct latencies
{
   int add, sub, mul, div;       /* Latencies for each fp operation */
};

/**
 * @name : config (configure)
 * @brief: This struct sets up all the data in the buffer struct and parses
 *         the config.txt file
 */
void config(buffers &buffs, latencies &latens)
{
   /* Each line from the file */
   std::string line;
   std::ifstream file("config.txt");
   int count = 0;
   /* Used to contruct the string array for buffs */
   std::string temptypes[12] = {"DADD", "DSUB", "LW", "SW", "BNE", "BEQ", 
                                "L.S", "S.S", "ADD.S", "SUB.S", "MUL.S",
                                "DIV.S"};
   /* The number of columns in the buffer */
   buffs.columns = 10;

   /* Sets the columns where each type of data will be found */
   buffs.busycol = 0;
   buffs.instrcol = 1;
   buffs.reg1col = 2;
   buffs.dspcol = 3;
   buffs.reg2col = 4;
   buffs.addrcol = 5;
   buffs.reg3col = 6;
   buffs.comcol = 7;
   buffs.afterwritecol = 8;
   buffs.entrycol = 9;

   /* Sets the number of instructions recognized */
   buffs.typesize = 12;

   /* Initializes variables */
   buffs.r_delays = 0;
   buffs.s_delays = 0;
   buffs.comcycle = 0;
   buffs.storecycle = 0;
   buffs.datmemdelays = 0;
   buffs.entrynum = 1;
   buffs.depends = 0;

   for(int i = 0; i < buffs.typesize; ++i)
      buffs.types[i] = temptypes[i];

   if(file.is_open())
   {
      while(getline(file, line))
      {
         ++count;

        /* Reads in the buffers data from the config file */
        if(count == 3)
           buffs.effaddrs = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 4)
           buffs.adds = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 5)
           buffs.muls = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 6)
           buffs.ints = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 7)
           buffs.reorder = atoi(line.substr(line.find(":") + 1).c_str());

        /* Reads in the latencies from the config file */
        if(count == 12)
           latens.add = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 13)
           latens.sub = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 14)
           latens.mul = atoi(line.substr(line.find(":") + 1).c_str());
        if(count == 15)
           latens.div = atoi(line.substr(line.find(":") + 1).c_str());
      }

      /* Sets the size of the reoder buffer */
      buffs.reordersize = buffs.reorder;
      /* Creates the rows for the buffer */
      buffs.buffer.resize(buffs.reorder);

      /* Creates the columns for the buffer */
      for(int i = 0; i < buffs.reorder; ++i)
         buffs.buffer[i].resize(buffs.columns);
   }
   else
      std::cout << "Failed to open file config.txt\n";
}

/**
 * @name : num2type (Number to Type)
 * @brief: Reads in a number and the buffer and returns the instruction for
 *         that number
 */
std::string num2type(buffers &buffs, int num)
{
   return buffs.types[num];
}

/**
 * @name : type2num (Type to Number)
 * @brief: Reads in the buffer and the instruction and returns the number for 
 *         it
 */
int type2num(buffers &buffs, std::string &type)
{
   for(int i = 0; i < buffs.typesize; ++i) 
   {
      if(type == buffs.types[i])
         return i;
   }
   return -1;
}

/**
 * @name : commit
 * @brief: Simulates the commit cycle for the type, type repesents the 
 *         instruction
 */
void commit(buffers &buffs, int &cycle, std::string &type)
{
   /* The intruction as a number */
   int numtype;
   int comcycle = cycle + 1;

   /* Ensures in-order commits */
   if(buffs.comcycle < comcycle)
      buffs.comcycle = comcycle;
   else
      ++buffs.comcycle;

   if(type == "S.S" || type == "SW" || type == "BNE" || type == "BEQ")
      printf("%22d", buffs.comcycle);
   else
      printf("%8d", buffs.comcycle);

   /** 
    * Tracks the commit cycle for the store instruction, 
    * used to check for hazards
    */
   if(type == "S.S" || type == "SW")
      buffs.storecycle = buffs.comcycle;

   /* Since these types do not have a write cycle we need to set afterwrite */
   if(type == "S.S" || type == "SW" || type == "BNE" || type == "BEQ")
      buffs.afterwrite = buffs.comcycle;

   /* The cycle a reorder buffer spot needs to be freed */
   buffs.r_free.push(buffs.comcycle);

   numtype = type2num(buffs, type);

   /* Adds the instruction to the buffer */
   for(int i = 0; i < buffs.reordersize; ++i)
   {
      /* Check for a non-busy entry */
      if(buffs.buffer[i][buffs.busycol] == 0)
      {
         /* Mark the entry as busy */
         buffs.buffer[i][buffs.busycol] = 1;
         buffs.buffer[i][buffs.instrcol] = numtype;
         buffs.buffer[i][buffs.reg1col] = buffs.reg1;
         buffs.buffer[i][buffs.reg2col] = buffs.reg2;
         buffs.buffer[i][buffs.comcol] = buffs.comcycle;
         buffs.buffer[i][buffs.afterwritecol] = buffs.afterwrite;
         buffs.buffer[i][buffs.entrycol] = buffs.entrynum;
         ++buffs.entrynum;

         /** 
          * In this if statement -1 is used when the instruction doe not use 
          * that column 
          */
         if(type == "L.S" || type == "LW" || type == "S.S" || type == "SW")
         {
            buffs.buffer[i][buffs.dspcol] = buffs.dsp;
            buffs.buffer[i][buffs.addrcol] = buffs.addr;
            buffs.buffer[i][buffs.reg3col] = -1;
         }
         else if(type == "BNE" || type == "BEQ")
         {
            buffs.buffer[i][buffs.reg3col] = -1;
            buffs.buffer[i][buffs.addrcol] = -1;
            buffs.buffer[i][buffs.dspcol] = -1;
         }
         else 
         {
            buffs.buffer[i][buffs.reg3col] = buffs.reg3;
            buffs.buffer[i][buffs.addrcol] = -1;
            buffs.buffer[i][buffs.dspcol] = -1;
         }

         break;
      }
   }
}

/**
 * @name : write
 * @brieg: Simulates the write cycle for the type
 */
void write(buffers &buffs, int &cycle, std::string &type)
{
   int write_cycle = cycle + 1;

   /* Makes sure no two write cycles are the same */
   for(unsigned int i = 0; i < buffs.writecycles.size(); ++i)
   {
      if(buffs.writecycles[i] == write_cycle)
         ++write_cycle;
   }

   /* Keep track of when stations need to be freed */
   if(type == "L.S" || type == "LW")
   {
      printf("%7d", write_cycle);
      buffs.s_free_addr.push(write_cycle);
   }
   else
      printf("%14d", write_cycle);

   if(type == "DADD" || type == "DSUB")
      buffs.s_free_ints.push(write_cycle);

   if(type == "ADD.S" || type == "SUB.S")
      buffs.s_free_adds.push(write_cycle);

   if(type == "MUL.S" || type == "DIV.S")
      buffs.s_free_muls.push(write_cycle);

   buffs.writecycles.push_back(write_cycle);
   buffs.afterwrite = write_cycle + 1;
   commit(buffs, write_cycle, type);
}

/**
 * @name : memstall (Memory Stall)
 * @brieg: Check to see if there is a hazard in the memory cycle
 */
void memstall(buffers &buffs, int &cycle, std::string &type)
{
   std::string temptype;
   int maxcom = 0;

   for(int i = 0; i < buffs.reordersize; ++i)
   {
      /** 
       * Look for non-busy entry that has entered the buffer before this 
       * instruction 
       * */
      if(buffs.buffer[i][buffs.busycol] == 1 &&
        (buffs.buffer[i][buffs.entrycol] < buffs.entrynum))
      {
         /* Get the found entries type */
         temptype = num2type(buffs, buffs.buffer[i][buffs.instrcol]);

         if((type == "LW" || type == "L.S") &&
            (temptype == "SW" || temptype == "S.S"))
         {
            if(buffs.buffer[i][buffs.addrcol] == buffs.addr && 
               cycle < buffs.buffer[i][buffs.comcol])
            {
               maxcom = buffs.buffer[i][buffs.comcol];
               /* Calculate the number of true dependencies */
               buffs.depends += (maxcom - cycle);
               /* Updates the current cycle */
               cycle = maxcom;
            }
         }
      }
   }
}

/**
 * @name : checkmemcycles (Check Memory Cycles)
 * @brief: Checks to see if the current cycle is equal to a cycle a
 *         instruction is accessing the memory
 */
void checkmemcycles(buffers &buffs, int &cycle)
{
   for(unsigned int i = 0; i < buffs.memcycles.size(); ++i)
   {
      if(buffs.memcycles[i] == cycle)
      {
         ++buffs.datmemdelays;
         ++cycle;
      }
   }
}

void mem(buffers &buffs, int &cycle, std::string &type)
{
   int mem_cycle = cycle + 1;

   /* Check to see if the is a stall, if there is cycle is updated */
   memstall(buffs, mem_cycle, type);

   /* Check to see if the current cycle is equal to anther memory cycle */
   checkmemcycles(buffs, mem_cycle);

   if(type == "L.S" || type == "LW")
   {
      /* Check to see if a store is accessing memory in this cycle */
      if(buffs.storecycle == mem_cycle)
      {
         ++mem_cycle;
         ++buffs.datmemdelays;
      }

      /* Check cycles agian in case it changed above */
      checkmemcycles(buffs, mem_cycle);

      printf("%7d", mem_cycle);
      buffs.memcycles.push_back(mem_cycle);
      write(buffs, mem_cycle, type);
   }
}

/**
 * @name : exstall (Execution Stall)
 * @brief: Check to see is there is a hazard in the execution cycle 
 */
void exstall(buffers &buffs, int &cycle, std::string &type)
{
   std::string temptype;
   int maxwrite = cycle;
   /* Used to check for a register 1 or 3 hazard */
   bool reg3haz = false;
   bool reg1haz = false;

   for(int i = 0; i < buffs.reordersize; ++i)
   {
      /* Check for non-busy instruction that is at most two above this one */
      if(buffs.buffer[i][buffs.busycol] == 1 && 
        (buffs.buffer[i][buffs.entrycol] == buffs.entrynum-1 || 
         buffs.buffer[i][buffs.entrycol] == buffs.entrynum-2) )
      {
         /* Get the type for the found instruction */
         temptype = num2type(buffs, buffs.buffer[i][buffs.instrcol]);
         
         if((type == "LW" || type == "L.S" || type == "DSUB" || 
             type == "DADD" || type == "BEQ" || type == "BNE" ) && 
            (temptype == "LW" || temptype == "DADD" || temptype == "DSUB"))
         {
            /* Check for a hazard with register 3 */
            if((type == "DADD" || type == "DSUB") && buffs.buffer[i][buffs.reg1col] == buffs.reg3)
               reg3haz = true;

            if(type == "BNE" || type == "BEQ")
            {
               /* Makes sure we do not look for a hazard with register 3 */
               reg3haz = false;
               /* Check for a hazard with register 1 */
               if(buffs.buffer[i][buffs.reg1col] == buffs.reg1)
                  reg1haz = true;     
            }
            /* Detect the hazard */
            if(buffs.buffer[i][buffs.reg1col] == buffs.reg2 || reg3haz || reg1haz)
            {
               if(cycle < buffs.buffer[i][buffs.afterwritecol])
               {
                  buffs.depends += (buffs.buffer[i][buffs.afterwritecol] - cycle);
                  cycle = buffs.buffer[i][buffs.afterwritecol];
               }
            }
         }
      }
      reg3haz = false;
      reg1haz = false;
 
      /* Deals with hazards for floating point operations */      
      if(buffs.buffer[i][buffs.busycol] == 1 && 
         buffs.entrynum > buffs.buffer[i][buffs.entrycol])
      {
         temptype = num2type(buffs, buffs.buffer[i][buffs.instrcol]);

         if((type == "ADD.S" || type == "SUB.S" || type == "MUL.S" || 
            type == "DIV.S") && (temptype == "L.S" || temptype == "ADD.S" ||
            temptype == "SUB.S" || temptype == "MUL.S" || temptype == "DIV.S"))
         {
            /* Find a hazard */
            if((buffs.buffer[i][buffs.reg1col] == buffs.reg2 || 
               buffs.buffer[i][buffs.reg1col] == buffs.reg3) &&
               maxwrite < buffs.buffer[i][buffs.afterwritecol])
            {
               maxwrite = buffs.buffer[i][buffs.afterwritecol];

               buffs.depends += (maxwrite - cycle);
               cycle = maxwrite;
            }
         }
      }
   }


}

/**
 * @name : execute
 * @brief: Simulates the execution cycle 
 */
void execute(buffers &buffs, int &cycle, std::string &type, latencies &latens)
{
   int ex_cycle = cycle + 1;
   /* Used by floating point operations to find the end of the cycle */
   int end_ex;

   /* Check for a hazard */
   exstall(buffs, ex_cycle, type);

   /* Makes sure the none floating point operations can us end_ex */
   end_ex = ex_cycle;

    /* Calculates the end of execution for the floating point operations */
   if(type == "ADD.S")
      end_ex = (ex_cycle + latens.add) - 1;
   else if(type == "SUB.S")
      end_ex = (ex_cycle + latens.sub) - 1;
   else if(type == "MUL.S")
      end_ex = (ex_cycle + latens.mul) - 1;
   else if(type == "DIV.S")
      end_ex = (ex_cycle + latens.div) - 1; 

   printf("%4d -%3d", ex_cycle, end_ex);

   if(type =="S.S" || type == "SW")
   {
      /* The cycle the next LW or SW can be issued if the station are full */
      buffs.s_free_addr.push(ex_cycle + 1);
      commit(buffs, ex_cycle, type);
   }

   if(type == "L.S" || type == "LW")
      mem(buffs, ex_cycle, type);

   if(type == "DADD" || type == "DSUB")
      write(buffs, ex_cycle, type);

   if(type == "BEQ" || type == "BNE")
      commit(buffs, ex_cycle, type);
  
   if(type == "ADD.S" || type == "SUB.S" || type == "MUL.S" || type == "DIV.S")
      write(buffs, end_ex, type); 
}

/**
 * @name : station_free
 * @brief: Used to free up a reservation station
 */
void station_free(buffers &buffs, std::string &type, int &cycle)
{
   /* Checks the cycle to see if it is a cycle when all stations are full */
   if(cycle == buffs.s_free_addr.front() && !buffs.s_free_addr.empty())
   {
      buffs.s_free_addr.pop();
      ++buffs.effaddrs;
   }

   if(cycle == buffs.s_free_ints.front() && !buffs.s_free_ints.empty())
   {
      buffs.s_free_ints.pop();
      ++buffs.ints;
   }

   if(cycle == buffs.s_free_adds.front() && !buffs.s_free_adds.empty())
   {
      buffs.s_free_adds.pop();
      ++buffs.adds;
   }
   if(cycle == buffs.s_free_muls.front() && !buffs.s_free_muls.empty())
   {
      buffs.s_free_muls.pop();
      ++buffs.muls;
   }

   /**
    * Since s_free is popped before this function is called then if this is
    * true then that means the front and the element after it where equal so
    * another station needs to be freed in this cycle.
    */ 
   if(cycle == buffs.s_free_addr.front())
      station_free(buffs, type, cycle);
}

/**
 * @name : printIssue (Print Issue)
 * @brief: Used to make sure the spacing between the printed instruction and
 *         the issue cycle remains a fixed length
 */
void printIssue(int cycle, int width)
{
   int dist = 15;
   if(width > 14)
   {
      width -= 14;
      dist -= width;
      printf("%*d", dist, cycle);
   }
   else
      printf("%15d", cycle);
}

/**
 * @name : freebuff
 * @brief: Checks to see if we need to free a entry in the buffer
 */
void freebuff(buffers &buffs)
{
   for(int i = 0; i < buffs.reordersize; ++i)
   {
      /* Check if we need to free the entry */
      if(buffs.buffer[i][buffs.comcol] == buffs.r_free.front() &&
         !buffs.r_free.empty())
      {
         /* Marks the entry as non-busy */
         buffs.buffer[i][buffs.busycol] = 0;
      }
   }
   buffs.r_free.pop();
   ++buffs.reorder;
}

/**
 * @name : station_delay (Station Delay)
 * @brief: Check to see if there is a delay caused by the reservation stations,
 *         if there is upade the current cycle
 */
void station_delay(buffers &buffs, int &cycle, std::string &type)
{
   /* Delay for eff addrs */
   if(type == "L.S" || type == "LW" || type == "SW" || type == "S.S")
   {
      /* Check to see if we have a delay */
      if(buffs.effaddrs <= 0 && cycle != buffs.s_free_addr.front() &&
         cycle < buffs.s_free_addr.front())
      {
         buffs.s_delays += (buffs.s_free_addr.front() - cycle);
         /* Updates the current cycle to the cycle when there is a free station */
         cycle = buffs.s_free_addr.front();
      }

   }
   /* Delay for ints */
   if(type == "DADD" || type == "DSUB" || type == "BNE" || type == "BEQ")
   {
      if(buffs.ints <= 0 && cycle != buffs.s_free_ints.front() &&
         cycle < buffs.s_free_ints.front())
      {
         buffs.s_delays += (buffs.s_free_ints.front() - cycle);
         cycle = buffs.s_free_ints.front();
      }
   }

   /* Delay for floating pint adds */
   if(type == "ADD.S" || type == "SUB.S")
   {
      if(buffs.adds <= 0 && cycle != buffs.s_free_adds.front() &&
         cycle < buffs.s_free_adds.front())
      {
         buffs.s_delays += (buffs.s_free_adds.front() - cycle);
         cycle = buffs.s_free_adds.front();
      }
   }

   //printf("\ncycle = %d BUFFS.MULS = %d,  s_free_muls = %d\n", cycle, buffs.muls, buffs.s_free_muls.front());
   /* Delay for muls */
   if(type == "MUL.S" || type == "DIV.S")
   {
      if(buffs.muls <= 0 && cycle != buffs.s_free_muls.front() &&
         cycle < buffs.s_free_muls.front())
      {
         buffs.s_delays += (buffs.s_free_muls.front() - cycle);
         cycle = buffs.s_free_muls.front();
      }
   }
}

/**
 * @name : issue
 * @brief: This function checks for reservation station and buffer delays.
 *         If there is availble stations then it the instruction is issued.
 */
void issue(buffers &buffs, int &cycle, std::string &type, int &width, latencies &latens)
{
   int tempcycle = cycle;

   /* Check to see if the reorder buffer is full */
   if(buffs.reorder <= 0 && cycle != buffs.r_free.front() &&
      cycle < buffs.r_free.front())
   {
      buffs.r_delays += (buffs.r_free.front() - cycle);
      /* Update the current cycle */
      cycle = buffs.r_free.front();
   }

   /* Free up a entry in the buffer */
   if(cycle == buffs.r_free.front())
      freebuff(buffs);

   /* Check for a delay */
   station_delay(buffs, cycle, type);

   if(cycle == buffs.s_free_addr.front() ||
      cycle == buffs.s_free_ints.front() ||
      cycle == buffs.s_free_adds.front() ||
      cycle == buffs.s_free_muls.front())
   {
      /* Free up a station */
      station_free(buffs, type, cycle);
   }

   /** 
    * If cycle changed above look for a skiped cycle where a station or 
    * buffer entry is freed. The code for the if is the same as above except
    * we do not update the cycle and we do not call station delay agian 
    */
   if( (cycle - tempcycle) != 0)
   {
      ++tempcycle;
      for(int i = tempcycle; i != cycle+1; ++i)
      {
         if(buffs.reorder <= 0 && i != buffs.r_free.front() &&
            i < buffs.r_free.front())
         {
            buffs.r_delays += (buffs.r_free.front() - i);
            i = buffs.r_free.front();
         }

        if(i == buffs.r_free.front())
           freebuff(buffs);

        if(i == buffs.s_free_addr.front() ||
           i == buffs.s_free_ints.front() ||
           i == buffs.s_free_adds.front() ||
           i == buffs.s_free_muls.front())
        {
           station_free(buffs, type, i);
        }
     }
   }

   /** 
    * Once we get here the cycle is one that can issue so subtract one from 
    * the correct station 
    */
   if(type == "L.S" || type == "LW" || type == "SW" || type == "S.S")
       --buffs.effaddrs;

   if(type == "DADD" || type == "DSUB" || type == "BNE" || type == "BEQ")
      --buffs.ints;

   if(type == "ADD.S" || type == "SUB.S")
      --buffs.adds;

   if(type == "MUL.S" || type == "DIV.S")
      --buffs.muls;

   /* The cycle can issue so the buffer can hold one less item */
   --buffs.reorder;
   printIssue(cycle, width);
   execute(buffs, cycle, type, latens);
 
   /* Increments the current cycle for the next call to the issue function */
   ++cycle;
}

/**
 * @name : print
 * @brief: Used to print the info up to the first instruction printed 
 */
void print(buffers &buffs, latencies &latens)
{
   printf("Configuration\n");
   printf("-------------\n");
   printf("buffers:\n");
   printf("   eff addr: %d\n", buffs.effaddrs);
   printf("    fp adds: %d\n", buffs.adds);
   printf("    fp muls: %d\n", buffs.muls);
   printf("       ints: %d\n", buffs.ints);
   printf("    reorder: %d\n", buffs.reorder);

   printf("\n");

   printf("latencies:\n");
   printf("   fp add: %d\n", latens.add);
   printf("   fp sub: %d\n", latens.sub);
   printf("   fp mul: %d\n", latens.mul);
   printf("   fp div: %d\n", latens.div);

   printf("\n\n");

   printf("                    Pipeline Simulation\n");
   printf("-----------------------------------------------------------\n");
   printf("                                      Memory Writes\n");
   printf("     Instruction      Issues Executes  Read  Result Commits\n");
   printf("--------------------- ------ -------- ------ ------ -------\n");
}

/**
 * @name : printINST (Print Instruction)
 * @brief: Used to print a instruction
 */
void printINST(char line[], int width)
{
   line[width-1] = '\0';
   printf("%s", line);
}

int main()
{
   buffers buffs;
   latencies latens;
   std::string type;
   /* Used to repensent the length of the instruction */
   int size = 64;
   char line[size];
   int cycle = 1;
   int width = 0;

   /* Configure the buffer */
   config(buffs, latens);
   print(buffs, latens);

   while(fgets(line, size, stdin))
   {
      /* Used to get the length of the instuction */
      type = line;
      width = type.size();
      printINST(line, width);

      if(sscanf(line, "L.S F%d,%d(R%d):%d", &buffs.reg1, &buffs.dsp,
         &buffs.reg2, &buffs.addr) == 4)
      {
         type = "L.S";
      }

      if(sscanf(line, "S.S F%d,%d(R%d):%d", &buffs.reg1, &buffs.dsp, 
         &buffs.reg2, &buffs.addr) == 4)
      {
         type = "S.S";
      }
    
      if(sscanf(line, "LW R%d,%d(R%d):%d", &buffs.reg1, &buffs.dsp, 
         &buffs.reg2, &buffs.addr) == 4)
      {
         type = "LW";
      }

      if(sscanf(line, "SW R%d,%d(R%d):%d", &buffs.reg1, &buffs.dsp, 
         &buffs.reg2, &buffs.addr) == 4)
      {
         type = "SW";
      }

      if(sscanf(line, "ADD.S F%d,F%d,F%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "ADD.S";
      }

      if(sscanf(line, "DADD R%d,R%d,R%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "DADD";
      }

      if(sscanf(line, "SUB.S F%d,F%d,F%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "SUB.S";
      }

      if(sscanf(line, "DSUB R%d,R%d,R%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "DSUB";
      }

      if(sscanf(line, "MUL.S F%d,F%d,F%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "MUL.S";
      }

      if(sscanf(line, "DIV.S F%d,F%d,F%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "DIV.S";
      }

      if(sscanf(line, "BNE R%d,R%d,L%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "BNE";
      }

      if(sscanf(line, "BEQ R%d,R%d,L%d", &buffs.reg1, &buffs.reg2, 
         &buffs.reg3) == 3)
      {
         type = "BEQ";
      }

      issue(buffs, cycle, type, width, latens);
      printf("\n");
   }
 
   printf("\n\nDelays\n");
   printf("------\n");
   printf("reorder buffer delays: %d\n", buffs.r_delays);
   printf("reservation station delays: %d\n", buffs.s_delays);
   printf("data memory conflict delays: %d\n", buffs.datmemdelays);
   printf("true dependence delays: %d\n", buffs.depends);

   return 0;
}
